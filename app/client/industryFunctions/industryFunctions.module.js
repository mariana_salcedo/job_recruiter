(function() {
  'use strict';

  angular
      .module('avisare.industryFunctions', [
          'avisare.core', 'ui.grid', 'ui.grid.edit', 'ui.grid.expandable', 'ui.grid.pagination', 'ui.grid.resizeColumns'
      ]);
})();
