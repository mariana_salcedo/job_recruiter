(function () {
  'use strict';

  angular
      .module('avisare.industryFunctions')
      .factory('industryFunctionsService', industryFunctionsService);

  /* @ngInject */
  function industryFunctionsService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'industryFunctions/:id', {id: '@id'},
      {
        get:{
          method: 'GET'
        },
        post:{
          method: 'POST'
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        }
      });
    return service;
  }
})();
