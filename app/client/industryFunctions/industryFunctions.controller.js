/**
 * avisare
 * Created by Ricardo Colmenarez on 11/11/15.
 * email rcolmenarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular.module('avisare.industryFunctions')
      .controller('IndustryFunctionsController', IndustryFunctionsController)
      .controller('IndustryFunctionsModalController', IndustryFunctionsModalController)
      .controller('EditIndustryFunctionsModalController', EditIndustryFunctionsModalController);

  /* @ngInject */

  function IndustryFunctionsController(
    $uibModal,
    toaster,
    industryFunctionsService,
    _,
    $animate) {
    var vm = this;

    vm.getIndustryFunctions = getIndustryFunctions;
    vm.showForm = showForm;
    vm.editIndustryFunctions = editIndustryFunctions;
    vm.deleteIndustryFunctions = deleteIndustryFunctions;

    vm.tabs = [
      {
        title: 'Basic Info',
        content: '/public/templates/_industryFunctions_info.html'
      }
    ];
    
    vm.paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null
    };

    vm.gridOptions = {
      columnDefs: [
        {field: 'id', displayName: 'ID', cellEditableCondition:false},
        {field: 'functionName', displayName: 'Name'},
        {field: 'functionStatus', displayName: 'Status', cellTemplate:'<div class="ui-grid-cell-contents">{{COL_FIELD ? \'Enabled\' : \'Disabled\'}}</div>'},
        /*{field: 'functionDescription', displayName: 'Description'},*/
        {field: 'createdAt', displayName: 'Created At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        {field: 'updatedAt', displayName: 'Updated At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        {field: 'deletedAt', displayName: 'Deleted At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        {name: 'Action', cellEditableCondition:false, cellTemplate: '<div class="ui-grid-cell-contents"><button class="btn btn-danger btn-xs btn-block" ng-click="grid.appScope.industryFunctions.deleteIndustryFunctions(row.entity.id)"><span class="glyphicon glyphicon-trash"></span></button></div>'}
      ],
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi){
        vm.gridApi = gridApi;
        $animate.enabled(vm.gridApi.grid.element, false);
        vm.gridApi.edit.on.beginCellEdit(null, function(rowEntity, colDef, newValue, oldValue) {
          editIndustryFunctions(rowEntity);
        });
        
        vm.gridApi.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.paginationObj.offset = (newPage - 1) * pageSize;
          vm.paginationObj.limit = pageSize;
          vm.getIndustryFunctions();
        });
        
        vm.gridApi.core.on.sortChanged(null, function(grid, sortColumns) {
          if (sortColumns.length == 0) {
            vm.paginationObj.order = null;
            vm.paginationObj.orderBy = null;
          } else {
            vm.paginationObj.order = sortColumns[0].sort.direction;
            vm.paginationObj.orderBy = sortColumns[0].name;
          }
          vm.getIndustryFunctions();
        });
      }
    };

    getIndustryFunctions();

    function getIndustryFunctions(){
      industryFunctionsService.get(vm.paginationObj).$promise.then(function (res) {
        vm.industryFunction = res.rows;
        vm.gridOptions.totalItems = res.count;
        vm.gridOptions.data = vm.industryFunction;
      });
    }

    // Formulario Modal
    function showForm(size) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_industryFunctions_form.html',
        controller: 'IndustryFunctionsModalController',
        controllerAs: 'industryFunctions',
        animation: true,
      });
      modalInstance.result.then(function (res) {
        vm.industryFunction.push(res);
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function editIndustryFunctions(industryFunctionObject) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_industryFunctions_form.html',
        controller: 'EditIndustryFunctionsModalController',
        controllerAs: 'industryFunctions',
        animation: true,
        resolve: {
          industryFunction: function () {
            return industryFunctionObject;
          }
        }
      });
      modalInstance.result.then(function (res) {
        getIndustryFunctions();
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function deleteIndustryFunctions(id) {
      industryFunctionsService.delete({id:id}).$promise.then(function(res){
        getIndustryFunctions();
      });
    }
  }

  function IndustryFunctionsModalController($uibModalInstance, industryFunctionsService) {
    var vm = this;
    vm.addNewIndustryFunction = addNewIndustryFunction;
    vm.industryFunction = {};
    vm.industryFunction.functionStatus = false;
    function addNewIndustryFunction(industryFunction) {
      var newIndustryFunction = {
        functionName: industryFunction.functionName,
        functionDescription: industryFunction.functionDescription,
        functionStatus: industryFunction.functionStatus
      };
      industryFunctionsService.post(newIndustryFunction).$promise
        .then(function(res) {
          $uibModalInstance.close(res);
        });
    }
  }

  function EditIndustryFunctionsModalController($uibModalInstance, industryFunction, industryFunctionsService) {
    var vm = this;
    vm.addNewIndustryFunction = addNewIndustryFunction;
    vm.industryFunction = {};
    vm.industryFunction.id = industryFunction.id;
    vm.industryFunction.functionStatus = industryFunction.functionStatus;
    vm.industryFunction.functionName = industryFunction.functionName;
    vm.industryFunction.functionDescription = industryFunction.functionDescription;
    function addNewIndustryFunction(industryFunction) {
      var newIndustryFunction = {
        id: industryFunction.id,
        functionName: industryFunction.functionName,
        functionDescription: industryFunction.functionDescription,
        functionStatus: industryFunction.functionStatus
      };
      industryFunctionsService.update(newIndustryFunction).$promise.then(function() {
        $uibModalInstance.close(vm.industryFunction);
      });
    }
  }

})();