(function() {
  'use strict';

  angular
      .module('avisare.client')
      .controller('ClientController', ClientController);
      /*.controller('NotesPopOutController', NotesPopOutController)
      .controller('ConsultantPopUpController', ConsultantPopUpController)
      .controller('ReferencesPopUpController', ReferencesPopUpController);*/

  /* @ngInject */
  function ClientController($scope, applicationsServices, clientServices, $filter, $uibModal, toaster, $animate, $moment, $state, $timeout) {
    var vm = this;

    vm.favoriteItem = favoriteItem;
    vm.addEditNotes = addEditNotes;
    vm.getConsultants = getConsultants;
    vm.checkAvaliability = checkAvaliability;

    vm.tabs = [
      {
        title: 'Search',
        content: '/public/templates/_client_search.html'
      },
      {
        title: 'Records',
        content: '/public/templates/_client_consultant_records.html'/*,
        disabled: true*/
      },
      {
        title: 'Favorite',
        content: '/public/templates/_client_consultant_favorites.html'/*,
        disabled: true*/
      }
    ];

    vm.avStatus = [
      'Now',
      'This Month',
      'All'
    ];

    var appStatus = '<div class="ui-grid-cell-contents">' +
      '<span ng-if="grid.getCellValue(row, col) == \'Pending\'" class="label" style="display: block; background-color: #00C0EC">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Reviewed\'" class="label" style="display: block; background-color: #D14CBB" >{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'New\'" class="label" style="display: block; background-color: #FDA631">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Rejected\'" class="label" style="display: block; background-color: #F11E32">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Bench\'" class="label" style="display: block; background-color: #373737">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Accepted\'" class="label" style="display: block; background-color: #00C561">{{COL_FIELD}}</span>' +
      '</div>';

    var appAction = '<div class="ui-grid-cell-contents">' +
      '<div class="btn-group" dropdown dropdown-append-to-body>' +
        '<button type="button" class="btn btn-xs dropdown-toggle" dropdown-toggle>' +
          'Select an action <span class="caret"></span>' +
        '</button>' +
        '<ul class="dropdown-menu" role="menu">' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Pending\')">Pending</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Reviewed\')">Reviewed</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'New\')">New</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Rejected\')">Rejected</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Bench\')">Bench</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Accepted\')">Accepted</a></li>' +
        '</ul>' +
      '</div>' +
    '</div>';

    var addEditBtn = '<div class="ui-grid-cell-contents">' +
      '<button class="btn btn-primary btn-xs" ng-click="grid.appScope.consultant.addEditNotes(\'lg\', row.entity)">ADD & EDIT NOTE</button>' +
      '</div>';

    //var appResume = '<div ng-include="\'/public/templates/_application_expand_row.html\'" style="background-color: #FAFAFA"></div>';

    vm.app_paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null,
      query: ''
    };

    function updateAppStatus(appObj, status) {
      var newStatus = {
        id:appObj.id,
        applicationDate: $filter('date')(appObj.applicationDate, 'yyyy-MM-dd', 'UTC'),
        status:status
      };
      if (status === 'Rejected' || status === 'Accepted') {
        applicationsServices.respondApplication({'response': status, id:appObj.id})
          .$promise.then(function(res) {
            toaster.success({body: 'Application ' + status});
            getApplications();
            getConsultants();
          })
          .catch(function() {
            toaster.error({body: 'Unable to Approve application'});
          });
      } else {
        applicationsServices.update(newStatus).$promise.then(function(res) {
          getApplications();
          getConsultants();
        });
      }
    };

    function addEditNotes(size, appObj) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_new_app_note.html',
        controller: 'NotesPopOutController',
        controllerAs: 'note',
        animation: true,
        size: size,
        resolve: {
          application: function () {
            return appObj;
          },
          profileIsLoaded: function () {
            return profileIsLoaded;
          }
        }
      });
      modalInstance.result.then(function (res) {
        getApplications();
      });
    };

    var consultantStatus = '<div class="ui-grid-cell-contents">' +
      '<span ng-if="grid.getCellValue(row, col) == \'Pending\'" class="label" style="display: block; background-color: #00C0EC">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Deleted\'" class="label" style="display: block; background-color: #E11C2F" >{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Active\'" class="label" style="display: block; background-color: #00C561">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Accepted\'" class="label" style="display: block; background-color: #00C561">{{COL_FIELD}}</span>' +
      '</div>';

    var consultantAction = '<div class="ui-grid-cell-contents">' +
      '<div class="btn-group" dropdown dropdown-append-to-body>' +
        '<button type="button" class="btn btn-xs dropdown-toggle" dropdown-toggle>' +
          'Select an action <span class="caret"></span>' +
        '</button>' +
        '<ul class="dropdown-menu" role="menu">' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Pending\')">Pending</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Deleted\')">Deleted</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Active\')">Active</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Accepted\')">Accepted</a></li>' +
        '</ul>' +
      '</div>' +
    '</div>';

    var viewSiteBtn = '<div class="ui-grid-cell-contents">' +
      '<a class="btn btn-primary btn-xs btn-block" href="/consultant/{{row.entity.ProfileSetting.profileLink}}" target="_blank">VIEW SITE</a>' +
      '</div>';

    var ProfileProgress = '<div class="ui-grid-cell-contents">' +
    '      <div class="progress progress-md active"> ' +
    '         <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{COL_FIELD}}" aria-valuemin="0" aria-valuemax="100" style="width: {{COL_FIELD}}%">{{COL_FIELD}}%</div> ' +
    '      </div>' +
    '</div>';

    var nameLink = '<div class="ui-grid-cell-contents">' +
      '<a href="#" ng-click="grid.appScope.consultant.viewSite(row.entity)">{{row.entity.User.firstName}} {{row.entity.User.lastName}}</a>' +
      '</div>';

    var pictureTemplate = '<div class="ui-grid-cell-contents">' +
      '<span ng-if="grid.appScope.client.checkAvaliability(row.entity);" class="badge alert-success" style="padding-top: 0px; padding-right: 0px; font-size: 11px">Now &nbsp;</span>' +
      '<span ng-if="!grid.appScope.client.checkAvaliability(row.entity);" class="badge alert-danger" style="padding-top: 0px; padding-right: 0px; font-size: 11px">{{ row.entity.avaliability | date:"dd MM yyyy" }} &nbsp;</span>' +
      '<img  src="http://res.cloudinary.com/avisare/image/upload/w_50,h_50,c_fill/{{row.entity.File.cloudinaryUrl}}" class="pull-right img-responsive" lazy-src/>' +
      '</div>'

    var consultantResume = '<div ng-include="\'/public/templates/_client_consultant_expand_row.html\'" style="background-color: #FAFAFA"></div>'; 

    var optionsTemplate = '<div class="ui-grid-cell-contents">' + 
      '<a href="" ng-click=""><i class="fa fa-street-view" style="font-size: 20px"></i></a>    ' +
      '<a href="" ng-click="grid.appScope.client.favoriteItem(row.entity)"><i ng-class="{\'fa fa-star\': row.entity, \'yellow\': row.entity.favorite}" style="font-size: 20px"></i></a>   ' +
      '<a href="" ng-click=""><i class="fa fa-pencil-square-o" style="font-size: 20px"></i></a>   ' +
      //'<a class="btn btn-primary btn-xs btn-block" href="/consultant/{{row.entity.ProfileSetting.profileLink}}" target="_blank">HIRE/INQUIRE</a>' +
      '</div>';

    vm.consultant_paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null,
      query: ''
    };

    var columnDefs = [
      {field:'Picture', name:'Avaliability', enableSorting:false, enableColumnMenu:false, cellTemplate:pictureTemplate},
      {field:'User.firstName', name:'Name', enableHiding:false, cellTemplate:nameLink},
      {field:'IndustryFunctions[0].functionName', name:'Function', enableColumnMenu:false, enableSorting:false},
      {field:'companyName', name:'Company', enableColumnMenu:false, enableSorting:false},
      {field:'User.Addresses[0].City.cityName', name:'City', enableColumnMenu:false, enableSorting:false},
      {field:'User.Addresses[0].City.State.stateName', name:'State', enableColumnMenu:false, enableSorting:false}
    ];

    var headerSelection = true;
    var rowSelection = true;

    if ($state.$current.url.source !== '/client/new/rfp') {
      //columnDefs.push({field:'percentage', name:'Profile',  enableColumnMenu:false, enableSorting:false, cellTemplate: ProfileProgress});
      //columnDefs.push({field:'Site', name:'Site', enableSorting:false, enableColumnMenu:false, cellTemplate: viewSiteBtn});
      columnDefs.push({field:'Actions', name:'Actions', enableSorting:false, enableColumnMenu:false, cellTemplate: optionsTemplate});
      headerSelection = false;
      rowSelection = false;
    }

    vm.consultantsGrid = {
      data:                     [],
      columnDefs:               columnDefs,
      rowHeight:                70,
      expandableRowTemplate:    consultantResume,
      expandableRowHeight:      300,
      paginationPageSizes:      [25, 50, 75],
      paginationPageSize:       25,
      useExternalPagination:    true,
      useExternalSorting:       true,
      enableRowHeaderSelection: headerSelection,
      enableRowSelection:       rowSelection,
      onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;

        $animate.enabled(vm.gridApi.element, false);
        vm.gridApi.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.consultant_paginationObj.offset = (newPage - 1) * pageSize;
          vm.consultant_paginationObj.limit = pageSize;
          vm.getConsultants();
        });
        vm.gridApi.core.on.sortChanged(null, function(grid, sortColumns) {
          if (sortColumns.length == 0) {
            vm.consultant_paginationObj.order = null;
            vm.consultant_paginationObj.orderBy = null;
          } else {
            vm.consultant_paginationObj.order = sortColumns[0].sort.direction;
            vm.consultant_paginationObj.orderBy = sortColumns[0].field;
          }
          vm.getConsultants();
        });
        vm.gridApi.expandable.on.rowExpandedStateChanged(null ,function(row){
          // applicationsServices.getConsultantDetails({id:row.entity.id}).$promise.then(function (res) {
          if (row.entity.id == 1){
            row.entity= {"id":1,"status":"Active","UserId":2,"FileId":2,"avaliability":"2016-01-28T00:00:00.000Z","User":{"firstName":"Francisco","lastName":"Lopez","email":"flopez@teravisiontech.com","Addresses":[{"CityId":43116,"City":{"cityName":"Mountain View","State":{"stateName":"California"}},"UserAddress":{"UserId":2,"AddressId":1,"createdAt":"2015-12-11T15:21:28.000Z","updatedAt":"2015-12-11T15:21:28.000Z","deletedAt":null}}],"Languages":[{"id":40,"languageCode":"en","languageName":"English","languageStatus":true,"createdAt":"2015-12-10T14:37:20.000Z","updatedAt":"2015-12-10T14:37:20.000Z","deletedAt":null,"UserLanguage":{"UserId":2,"LanguageId":40,"createdAt":"2015-12-10T15:23:44.000Z","updatedAt":"2015-12-10T15:23:44.000Z"}},{"id":148,"languageCode":"es","languageName":"Spanish; Castilian","languageStatus":true,"createdAt":"2015-12-10T14:37:20.000Z","updatedAt":"2015-12-10T14:37:20.000Z","deletedAt":null,"UserLanguage":{"UserId":2,"LanguageId":148,"createdAt":"2015-12-10T15:23:44.000Z","updatedAt":"2015-12-10T15:23:44.000Z"}}]},"File":{"id":2,"s3Url":null,"cloudinaryUrl":"v1449758666/afvosgeiwpjohsucr122.jpg","type":"Image","extension":"jpg","size":null,"createdAt":"2015-12-10T14:44:46.000Z","updatedAt":"2015-12-10T14:44:46.000Z","deletedAt":null},"Services":[{"id":1,"serviceName":"Developer","serviceStatus":true,"serviceDescription":"","createdAt":"2015-12-14T13:09:44.000Z","updatedAt":"2015-12-14T13:09:44.000Z","ProfileServices":{"ProfileId":1,"ServiceId":1,"createdAt":"2015-12-14T13:09:44.000Z","updatedAt":"2015-12-14T13:09:44.000Z"}},{"id":2,"serviceName":"Analyst","serviceStatus":true,"serviceDescription":"","createdAt":"2015-12-14T13:09:44.000Z","updatedAt":"2015-12-14T13:09:44.000Z","ProfileServices":{"ProfileId":1,"ServiceId":2,"createdAt":"2015-12-14T13:09:44.000Z","updatedAt":"2015-12-14T13:09:44.000Z"}}],"CaseStudies":[{"id":1,"caseStudyTitle":"Case 1 lkjlkj kjñlkjlkj pkjñlkjñkjñjk klñjñlkjñlkñl ñlkñlkñlkñl ñokñlkñlk ñlkñlkpl","caseStudyProblem":"Transportation problems","caseStudySolution":"Transportation solutions","caseStudyResult":"Transportation results","createdAt":"2015-12-16T13:55:48.000Z","updatedAt":"2015-12-17T20:16:01.000Z","deletedAt":null,"IndustryId":1,"UserCaseStudies":{"ProfileId":1,"CaseStudyId":1,"createdAt":"2015-12-16T13:55:48.000Z","updatedAt":"2015-12-16T13:55:48.000Z","deletedAt":null},"Industry":{"id":1,"industryName":"Transportation & Logistics","industryStatus":true,"createdAt":"2015-12-10T14:37:23.000Z","updatedAt":"2015-12-10T14:37:23.000Z","deletedAt":null}},{"id":2,"caseStudyTitle":"Case 2 case 2 case 2","caseStudyProblem":"Financial Problem","caseStudySolution":"Financial Solution","caseStudyResult":"Financial Result","createdAt":"2015-12-16T13:56:33.000Z","updatedAt":"2015-12-17T20:43:08.000Z","deletedAt":null,"IndustryId":9,"UserCaseStudies":{"ProfileId":1,"CaseStudyId":2,"createdAt":"2015-12-16T13:56:33.000Z","updatedAt":"2015-12-16T13:56:33.000Z","deletedAt":null},"Industry":{"id":9,"industryName":"Financial Services","industryStatus":true,"createdAt":"2015-12-10T14:37:23.000Z","updatedAt":"2015-12-10T14:37:23.000Z","deletedAt":null}}],"Experiences":[{"id":1,"experienceCompany":"Summit Performance Group, LLC","experienceTitle":"CEO","experienceDescription":"Good Work","experienceShow":true,"experienceStart":"2015-12-16T00:00:00.000Z","experienceEnd":"2015-12-18T00:00:00.000Z","createdAt":"2015-12-16T16:17:16.000Z","updatedAt":"2015-12-16T16:17:16.000Z","deletedAt":null,"ProfileId":1},{"id":2,"experienceCompany":"Google Inc.","experienceTitle":"Hacker","experienceDescription":"Search Engine, Play games, Android and Chrome","experienceShow":true,"experienceStart":"2015-12-12T00:00:00.000Z","experienceEnd":"2015-12-16T00:00:00.000Z","createdAt":"2015-12-16T16:17:47.000Z","updatedAt":"2015-12-18T13:50:06.000Z","deletedAt":null,"ProfileId":1}],"Education":[{"id":1,"university":"Central University of Venezuela","degreeDesignation":"Bachelor","degreeName":"Computer Science","createdAt":"2015-12-10T15:24:14.000Z","updatedAt":"2015-12-10T15:24:14.000Z","deletedAt":null,"ProfileId":1}],"Skills":[{"id":1,"skillName":"Develop","skillStatus":true,"skillDescription":null,"createdAt":"2015-12-10T20:35:58.000Z","updatedAt":"2015-12-10T20:35:58.000Z","deletedAt":null,"ProfileSkill":{"ProfileId":1,"SkillId":1,"createdAt":"2015-12-10T20:35:58.000Z","updatedAt":"2015-12-10T20:35:58.000Z"}},{"id":2,"skillName":"Analytics","skillStatus":true,"skillDescription":null,"createdAt":"2015-12-10T20:35:58.000Z","updatedAt":"2015-12-10T20:35:58.000Z","deletedAt":null,"ProfileSkill":{"ProfileId":1,"SkillId":2,"createdAt":"2015-12-10T20:35:58.000Z","updatedAt":"2015-12-10T20:35:58.000Z"}}],"Certifications":[{"id":1,"certificationName":"BigData","certificationType":"Course","certificationDate":"2015-12-10T04:30:00.000Z","issuer":"Spark","createdAt":"2015-12-10T20:34:51.000Z","updatedAt":"2015-12-10T20:35:28.000Z","deletedAt":null,"ProfileId":1},{"id":2,"certificationName":"Machine Learning","certificationType":"Course","certificationDate":"2015-12-22T04:30:00.000Z","issuer":"Coursera","createdAt":"2015-12-22T21:56:06.000Z","updatedAt":"2015-12-22T21:56:06.000Z","deletedAt":null,"ProfileId":1}],"IndustryFunctions":[{"id":4,"functionName":"IT","functionDescription":null,"functionStatus":true,"createdAt":"2015-12-10T14:37:23.000Z","updatedAt":"2015-12-10T14:37:23.000Z","deletedAt":null,"ProfileFunctions":{"ProfileId":1,"IndustryFunctionId":4,"createdAt":"2016-01-05T21:39:35.000Z","updatedAt":"2016-01-05T21:39:35.000Z"}}],"ProfileSetting":{"id":1,"aboutMe":"Business Development Executive with a strong focus on product launch, start up companies, and taking products into the Government (primarily DoD) and into the commercial sector.   Achieved success with all products I have worked with, and carry an incredible rolodex that can network me into almost any opportunity.","publishPress":true,"profileLink":"FranciscoLopez","serviceDescription":"Leadership Skills, with broad and deep expertise in pairing business and technology strategy by creating and maintaining strong relationships with business partners, executives, and key stakeholders located around the globe, focusing on analyzing client needs and delivering strategic architecture, applications, and programs, while reducing complexity, and safeguarding business and technical resilience.","createdAt":"2015-12-10T14:38:50.000Z","updatedAt":"2015-12-14T13:09:44.000Z","deletedAt":null,"ProfileId":1}};
          }
          else if (row.entity.id == 2){
            row.entity= {"id":2,"status":"Active","UserId":3,"FileId":5,"avaliability":"2015-12-14T00:00:00.000Z","User":{"firstName":"Mariana","lastName":"Salcedo","email":"msalcedo@gmail.com","Addresses":[{"CityId":10266,"City":{"cityName":"Aurora","State":{"stateName":"Ontario"}},"UserAddress":{"UserId":3,"AddressId":2,"createdAt":"2015-12-14T18:39:47.000Z","updatedAt":"2015-12-14T18:39:47.000Z","deletedAt":null}},{"CityId":10266,"City":{"cityName":"Aurora","State":{"stateName":"Ontario"}},"UserAddress":{"UserId":3,"AddressId":3,"createdAt":"2015-12-14T18:45:29.000Z","updatedAt":"2015-12-14T18:45:29.000Z","deletedAt":null}}],"Languages":[]},"File":{"id":5,"s3Url":null,"cloudinaryUrl":"v1450117385/kwpzblaxqigy9buj3gjj.jpg","type":"Image","extension":"jpg","size":null,"createdAt":"2015-12-14T18:23:08.000Z","updatedAt":"2015-12-14T18:23:08.000Z","deletedAt":null},"Services":[],"CaseStudies":[],"Experiences":[],"Education":[],"Skills":[],"Certifications":[],"IndustryFunctions":[],"ProfileSetting":{"id":2,"aboutMe":" Because both style information and location information is delimited via the pipe character, style information must appear first in any marker descriptor. Once the Google Static Maps API server encounters a location in the marker descriptor, all other marker parameters are assumed to be locations as well.","publishPress":true,"profileLink":"MarianaSalcedo","serviceDescription":"instead of using these markers, you may wish to use your own custom icon. (For more information, see Custom Icons below.)","createdAt":"2015-12-14T18:19:43.000Z","updatedAt":"2015-12-14T18:24:20.000Z","deletedAt":null,"ProfileId":2}};
          }
          /*res.percentage = row.entity.percentage;
          row.entity = res;*/
          // });
        });
        vm.gridApi.selection.on.rowSelectionChanged(null, function(rows){
          $scope.$parent.rfp.selectedConsultants = vm.gridApi.selection.getSelectedRows();
          //console.log($scope.$parent.rfp.selectedConsultants);
        });

        $timeout(function(data){
          if (!!$scope.$parent.rfp){
            if($scope.$parent.rfp.selectedConsultants.length > 0){
              console.log('02', vm.consultantsGrid.data);
              _.forEach($scope.$parent.rfp.selectedConsultants, function(object){
                var index = _.find(vm.consultantsGrid.data, {'UserId': object.UserId});
                if (index != -1){
                  vm.gridApi.selection.toggleRowSelection(index);
                }
              });
            }
          }
        }, 100);
      }
    };

    function getConsultants() {
      //TODO: Don't Show consultants with completion < 75% 
      
    //  clientServices.getConsultants(vm.consultant_paginationObj).$promise.then(function(res) {
        //console.log('RES', res);
        var res = {"count":2,"rows":[{"id":1,"status":"Active","UserId":2,"FileId":2, "favorite": true, "avaliability":"2016-01-28T00:00:00.000Z","User":{"firstName":"Francisco","lastName":"Lopez","email":"flopez@teravisiontech.com","Addresses":[{"CityId":43116,"City":{"cityName":"Mountain View","State":{"stateName":"California"}},"UserAddress":{"UserId":2,"AddressId":1,"createdAt":"2015-12-11T15:21:28.000Z","updatedAt":"2015-12-11T15:21:28.000Z","deletedAt":null}}]},"File":{"id":2,"s3Url":null,"cloudinaryUrl":"v1449758666/afvosgeiwpjohsucr122.jpg","type":"Image","extension":"jpg","size":null,"createdAt":"2015-12-10T14:44:46.000Z","updatedAt":"2015-12-10T14:44:46.000Z","deletedAt":null},"ProfileSetting":{"profileLink":"FranciscoLopez"},"IndustryFunctions":[{"id":4,"functionName":"IT","functionDescription":null,"functionStatus":true,"createdAt":"2015-12-10T14:37:23.000Z","updatedAt":"2015-12-10T14:37:23.000Z","deletedAt":null,"ProfileFunctions":{"ProfileId":1,"IndustryFunctionId":4,"createdAt":"2016-01-05T21:39:35.000Z","updatedAt":"2016-01-05T21:39:35.000Z"}}]},{"id":2,"status":"Active","UserId":3,"FileId":5, "favorite": false, "avaliability":"2015-12-14T00:00:00.000Z","User":{"firstName":"Mariana","lastName":"Salcedo","email":"msalcedo@gmail.com","Addresses":[{"CityId":10266,"City":{"cityName":"Aurora","State":{"stateName":"Ontario"}},"UserAddress":{"UserId":3,"AddressId":2,"createdAt":"2015-12-14T18:39:47.000Z","updatedAt":"2015-12-14T18:39:47.000Z","deletedAt":null}},{"CityId":10266,"City":{"cityName":"Aurora","State":{"stateName":"Ontario"}},"UserAddress":{"UserId":3,"AddressId":3,"createdAt":"2015-12-14T18:45:29.000Z","updatedAt":"2015-12-14T18:45:29.000Z","deletedAt":null}}]},"File":{"id":5,"s3Url":null,"cloudinaryUrl":"v1450117385/kwpzblaxqigy9buj3gjj.jpg","type":"Image","extension":"jpg","size":null,"createdAt":"2015-12-14T18:23:08.000Z","updatedAt":"2015-12-14T18:23:08.000Z","deletedAt":null},"ProfileSetting":{"profileLink":"MarianaSalcedo"},"IndustryFunctions":[]}]};
        vm.consultants = res.rows;
        vm.consultantsGrid.totalItems = res.count;
        vm.consultantsGrid.data = vm.consultants;

        /*_(vm.consultantsGrid.data).forEach(function(n){
          applicationsServices.getCompletion({id:n.id}).$promise.then(function(response){
            n.percentage = response.percentage;
          });
        });*/
        //console.log('grid:', vm.consultantsGrid.data);
     // }); 
    };
    getConsultants();

    function viewSite (data) {
      // vm.tempConsultant = data;
      // var tabObj = {
      //   title: data.User.firstName + ' ' + data.User.lastName,
      //   content: '/public/templates/_consultant_records.html',
      // };
      // vm.tabs.push(tabObj);
    };

    function favoriteItem (data) {
      console.log("favorite", data);

      data.favorite ^= true;
      // vm.tempConsultant = data;
      // var tabObj = {
      //   title: data.User.firstName + ' ' + data.User.lastName,
      //   content: '/public/templates/_consultant_records.html',
      // };
      // vm.tabs.push(tabObj);
    };

    function updateConsultantStatus(consultantObj, status) {
      var newStatus = {
        id:consultantObj.id,
        avaliability: $filter('date')(consultantObj.avaliability, 'yyyy-MM-dd', 'UTC'),
        status:status
      };
      applicationsServices.updateConsultant(newStatus).$promise.then(function(res) {
        toaster.success({body: 'Update successful'});
        getConsultants();
      });
    };

    function checkAvaliability(consultantObj){
      //console.log('moment: ',$moment(consultantObj.avaliability).fromNow());
      return new Date(consultantObj.avaliability) < new Date();
    }
  };

  /*function NotesPopOutController($uibModalInstance, application, applicationsServices, profileIsLoaded) {
    var vm = this;
    vm.routeMessages = '/public/templates/_note_app_error_messages.html';
    vm.notes = application.ApplicationNotes;
    vm.newNote = '';
    vm.noteObj = null;
    vm.editMode = false;

    vm.saveNote = saveNote;
    vm.editNote = editNote;
    vm.removeNote = removeNote;

    function editNote(index) {
      vm.editMode = true;
      vm.noteForm.$setPristine();
      vm.noteForm.$setUntouched();
      vm.noteObj = vm.notes[index];
      vm.newNote = vm.notes[index].note;
    };

    function saveNote() {
      if (vm.editMode) {
        vm.noteObj.note = vm.newNote;
        applicationsServices.updateNote(vm.noteObj).$promise.then(function (res) {
          $uibModalInstance.close();
        });
      } else {
        vm.noteObj = {
          id: application.id,
          note: vm.newNote,
          User: {
            id: profileIsLoaded.id,
            firstName: profileIsLoaded.firstName,
            lastName: profileIsLoaded.lastName
          }
        };
        vm.notes.push(vm.noteObj);
        applicationsServices.postNote(vm.noteObj).$promise.then(function (res) {
          $uibModalInstance.close();
        });
      }
    };

    function removeNote(index) {
      vm.noteObj = vm.notes[index];
      applicationsServices.deleteNote(vm.noteObj).$promise.then(function(res) {
        vm.notes.splice(index, 1);
      });
    };
  }*/

  /*function ReferencesPopUpController(referenceObj){
    var vm = this;
    vm.reference = referenceObj;
  };*/

  /*function ConsultantPopUpController(row, $filter, $location, _){
    row.entity.applicationDate = $filter('date')(row.entity.applicationDate, 'MM/dd/yyyy', 'UTC');
    _.forEach(row.entity.References, function(obj, val){
      obj.deserialized = {};
      try {
        obj.deserialized = JSON.parse(obj.response);
      }
      catch(e) {}
    });
    //console.log('aaaa', row);
    var vm = this;
    vm.consultant = row.entity;
  };*/

  function quickSearch (form) { 
    console.log('quickSearch ')
    var params = {
      name: form.name,
      location: form.lastName,
      keyword: form.keyword,
      firm: form.firm,
      avaliability: form.avaliability 
    };
    clientServices.search(params).$promise.then(function(res){

    })
  };

})();
