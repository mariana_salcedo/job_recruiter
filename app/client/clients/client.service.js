(function () {
  'use strict';

  angular
    .module('avisare.clientReg')
    .factory('clientServices', clientServices);

  /* @ngInject */
  function clientServices($resource, $q, BASE_API_URI){
    var service = $resource(BASE_API_URI + 'accounts', {},
      {
        post:{
          method: 'POST',
          isArray: true
        },
        getAll: {
          method: 'GET'
        },
        get: {
          method: 'GET',
          url: BASE_API_URI + 'accounts/:id',
          params: {id: '@id'}
        },
        getMe: {
          method: 'GET',
          url: BASE_API_URI + 'accounts/me'
        },
        update: {
          method: 'PUT',
          url: BASE_API_URI + 'accounts/:id',
          params: {id: '@id'}
        },
        delete: {
          method: 'DELETE',
          url: BASE_API_URI + 'accounts/:id',
          params: {id: '@id'}
        },
        response: {
          method: 'PUT',
          url: BASE_API_URI + 'clients/:id/response',
          params: {id: '@id'}
        }
      });
    return service;
  }
})();
