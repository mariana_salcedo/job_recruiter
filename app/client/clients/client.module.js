(function iife() {
  'use strict';

  angular
    .module('avisare.clientReg', [
      'avisare.core'
    ]);

  angular
    .module('avisare.client', [
      'avisare.core',
      'avisare.consultant',
      'angular-momentjs',
      'ui.grid.selection'
    ]);
})();
