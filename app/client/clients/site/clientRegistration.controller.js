(function iife() {
  'use strict';

  angular
    .module('avisare.clientReg')
    .controller('ClientRegistration', ClientRegistration);

  /* @ngInject */
  function ClientRegistration(
    $scope,
    toaster,
    _,
    $window,
    $location,
    anchorSmoothScroll,
    clientServices
  ) {
    var vm = this;
    var missingFields = 'Please fill out required field(s).';
    var validTabs = [
      'experience',
      'reference',
      'basic'
    ];

    var submit = false;
    vm.sending = false;
    vm.title = 'ClientRegistration';
    vm.submitForm = submitForm;

    activate();

    ////////////////
    function activate() {
    }

    function validateForm(form) {
      if (form.$invalid) {
        _.forEach(form.$error, function (currrentObj) {
          _.forEach(currrentObj, function (obj) {
            if (!!obj) {
              obj.$setTouched(true);
              obj.$setDirty(true);
              obj.$validate();
            }
          });
        });
      }
    }

    function takeMeHome() {
      $window.location.href = 'http://' + $window.location.host;
    }

    function submitForm(form) {
      form.email.$setValidity('uniqueEmail', true);
      console.log('EMAIL', form.email);
      if (form.$valid) {
        submit = true;
        var clientObj = {
          companyName: form.companyName.$modelValue,
          firstName: form.firstName.$modelValue,
          lastName: form.lastName.$modelValue,
          message: form.message.$modelValue,
          website: form.website.$modelValue,
          email: form.email.$modelValue,
          status: 'Prospect',
          type: 'Client'
        };

        clientServices.post(clientObj).$promise
        .then(function(res) {
          toaster.success({body: 'Registration saved successfull'});
        })
        .catch(function(res) {
          if (res.data.errors && res.data.errors.length > 0) {
            var errors = res.data.errors;
            var i;
            for (i = 0; i < errors.length; i++) {
              if (errors[i].message.indexOf('email must be unique') > -1) {
                form.email.$setValidity('uniqueEmail', false);
                break;
              }
            }
            form.email.$validate();
          }
        });
      } else {
        //form.$setSubmitted(true);
        var elementError;
        var errors;
        validateForm(form);
        errors = _.toArray(form.$error);
        if (errors.length) {
          elementError = errors[0][0].$name;
          // set the location.hash to the id of
          // the element you wish to scroll to.
          anchorSmoothScroll.scrollTo(elementError);
        }

        toaster.error({body: missingFields});
      }
    }

  }
})();
