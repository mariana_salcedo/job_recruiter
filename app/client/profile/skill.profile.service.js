(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .factory('SkillProfileService', ProfileService);

  /* @ngInject */
  function ProfileService($resource, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'profiles/:id/skills', {id: '@id'},
    {
      addSkills:{
        method: 'POST',
        isArray: true
      },
      getSkills:{
        method: 'GET',
        isArray: true
      },
      removeSkills:{
        method: 'DELETE',
        isArray: true
      }
    });
    return service;
  }
})();
