(function() {
  'use strict';

  angular
    .module('avisare.profile', [
      'avisare.core',
      'avisare.skill',
      'avisare.application'
    ])
    .constant('cloudinary', window.cloudinary);
})();
