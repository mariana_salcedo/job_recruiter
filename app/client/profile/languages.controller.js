/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .controller('LanguagesController', LanguagesController);

  /* @ngInject */
  function LanguagesController($scope, Auth, languagesService, $q, _) {
    var vm = this;
    vm.title = 'LanguagesController';
    vm.saveLanguagesChanges = saveLanguagesChanges;
    var backupLanguages = [];
    activate();

    function activate() {
      vm.user = Auth.currentUser();
      vm.profile = Auth.currentProfile();
      // setting all language options
      languagesService.get({}).$promise
        .then(function (res) {
          vm.languages = res;
        });

      // setting user's languages
      languagesService.getMine({}).$promise
        .then(function(res) {
          if (res.length > 0) {
            vm.currentUserLanguages = res;
            backupLanguages = res;
          } else {
            vm.currentUserLanguages = [];
          }
        })
        .then(function() {
          if (!!vm.currentUserLanguages && vm.currentUserLanguages.length > 0) {
            vm.languageSelected = _.pluck(vm.currentUserLanguages, 'Language');
            vm.currentUserLanguages = _.pluck(vm.currentUserLanguages, 'Language');
            backupLanguages = _.pluck(backupLanguages, 'Language');
          }
        });
    }

    function saveLanguagesChanges(e, token) {
      var newUserLanguages = [];
      var statusResponse = true;
      var newLanguagesId = _.difference(
        _.pluck(vm.languageSelected, 'id'),
        _.pluck(backupLanguages, 'id')
      );
      var currentsId = _.pluck(vm.currentUserLanguages, 'id');
      var allIds = _.union(newLanguagesId, currentsId);
      var oldLanguagesId = _.difference(allIds, _.pluck(vm.languageSelected, 'id'));
      newUserLanguages = _.map(newLanguagesId, function(id) {
        return {id: id};
      });
      var promises = [];
      if (newLanguagesId.length > 0) {
        // Saving user languages
        promises.push(
          languagesService.post(newUserLanguages).$promise
        );
      }
      if (oldLanguagesId.length > 0) {
        promises.push(
          languagesService.delete({languages: oldLanguagesId}).$promise
        );
      }

      if (promises.length) {
        return $q.all(promises);
      } else {
        return $q.resolve();
      }

    }

    function syncLanguages(e, token) {
      saveLanguagesChanges()
      .then(function notifySuccess() {
        $scope.$emit('LanguageSync', token, true);
      })
      .catch(function notifyError() {
        $scope.$emit('LanguageSync', token, false);
      });
    }

    $scope.$on('SyncProfile', syncLanguages);
  }
})();
