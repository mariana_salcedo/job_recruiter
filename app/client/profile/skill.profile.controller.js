(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .controller('ProfileSkillController', ProfileSkillController);

  /* @ngInject */
  function ProfileSkillController($scope, $q, Auth, skillsService, SkillProfileService, _, toaster) {
    var vm = this;
    vm.title = 'ProfileSkillController';
    vm.skills = [];
    vm.newSkills = [];
    vm.oldSkills = [];

    vm.addSkill = addSkill;
    vm.syncSkills = syncSkills;
    vm.removeSkill = removeSkill;
    vm.tagTransform = tagTransform;

    $scope.tagTransform = tagTransform;

    activate();

    ////////////////

    function addSkill(skill) {

      if (_.include(vm.oldSkills, skill)) {

        _.remove(vm.oldSkills, skill);
      } else {
        if (!_.findWhere(vm.mySkills, {id: skill.id})) {

          vm.newSkills.push(skill);
        }
      }
    }

    function removeSkill(skill) {

      if (_.include(vm.newSkills, skill)) {

        _.remove(vm.newSkills, skill);
      } else {
        if (_.findWhere(vm.mySkills, {id: skill.id})) {

          vm.oldSkills.push(skill);
        }
      }
    }

    function tagTransform(item) {
      return {
        skillName: item
      };
    }

    function saveSkills() {
      var promises = [];

      if (vm.newSkills.length > 0) {

        promises.push(
          SkillProfileService.addSkills({
            id: vm.profile.id
          },
          vm.newSkills).$promise
          .then(function addSkills(skills) {
            _.forEach(skills, function skill(newSkill, index) {
              if (!!newSkill.createdAt) {
                _.assign(vm.newSkills[index], newSkill);
                vm.allSkills.push(vm.newSkills[index]);
              }
              vm.mySkills.push(
                _.find(vm.allSkills, function findSkill(skill) {
                  return skill.id === newSkill.id;
                })
              );
            });
            return skills;
          })
        );

      } else {
        promises.push($q.resolve([]));
      }

      if (vm.oldSkills.length > 0) {
        promises.push(
          SkillProfileService.removeSkills({
            id: vm.profile.id,
            skills: _.pluck(vm.oldSkills, 'id')
          }).$promise
          .then(function removeSkills(removedSkills) {
            _.remove(vm.mySkills, function isPresent(skill) {
              return _.include(removedSkills, skill.id);
            });
            return removedSkills;
          })
        );
      } else {
        promises.push($q.resolve([]));
      }

      return $q.all(promises).then(function resetSkills(data) {
        vm.newSkills = [];
        vm.oldSkills = [];
        toaster.success({body:'Skills saved successfully'});
        return data;
      });
    }

    function syncSkills() {

      saveSkills();
    }

    function activate() {

      vm.profile = Auth.currentProfile();

      $q.all([
        SkillProfileService.getSkills({
          id: vm.profile.id
        }).$promise
        .then(function setMySkills(skills) {
          vm.mySkills = _.map(skills, function setSkills(skill) {
            _.pick(skill, _.identity);
            return _.extend(skill.Skill, skill);
          });
        }),
        skillsService.getAll()
        .$promise
        .then(function setSkills(skills) {
          vm.allSkills = _.map(skills, function filterSkill(skill) {
            return _.pick(skill, _.identity);
          });
        })
      ]).then(function setCurrentSkills() {
        vm.skills = _.map(_.pluck(vm.mySkills, 'id'), function setSkills(skillId) {
          return _.find(vm.allSkills, function findSkill(skill) {
            return skill.id === skillId;
          });
        });
      });
    }
  }
})();
