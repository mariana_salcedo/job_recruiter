/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .controller('EducationController', EducationController);

  /* @ngInject */
  function EducationController($scope, Auth, educationService, toaster, _, $uibModal) {
    var vm = this;
    vm.profile = null;
    var modalInstance;
    vm.title = 'EducationController';
    vm.educationList = [];
    vm.saveEducation = saveEducation;
    var statusResponse = true;
    var hasEducation = false;
    var educationId = null;
    vm.openModal = openModal;
    vm.closeModal = closeModal;
    vm.removeEducation = removeEducation;
    vm.education;

    vm.designations = [
      'BA',
      'BS',
      'BSBA',
      'JD',
      'MA',
      'MBA',
      'MEng',
      'MS',
      'PhD'
    ];

    activate();

    function activate() {
      vm.user = Auth.currentUser();
      vm.profile = Auth.currentProfile();
      getEducation();
    }

    function openModal(education) {
      vm.education = education || {};

      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_education_form.html',
        animation: true,
        size: 'lg',
        scope: $scope
      });
      console.log('instance',modalInstance);
    }

    function closeModal() {
      console.log('closing!!!!!');
      if (!!modalInstance && modalInstance.close) {
        modalInstance.close();
      }
    }

    function removeEducation(education) {

      return educationService.delete({
        idEducation: education.id
      }).$promise
      .then(function remove() {
        _.remove(vm.educationList, education);
      });
    }

    function getEducation(){
      educationService.get({idProfile: vm.profile.id}).$promise
        .then(function(res) {
          vm.educationList = res;
        });
    }

    // saving education info
    function saveEducation(education) {
      if (!!education && !!education.id) {
        education.idEducation = education.id;
        education.idProfile = vm.profile.id;
        educationService.update(education).$promise
          .then(function(newEducation) {
            if (newEducation) {
              statusResponse = true;
              getEducation();
              closeModal();
              toaster.success({body:'Profile saved successfully'});
            }
          })
          .catch(function (error) {
            toaster.error({body: 'An error has occurred!'});
          });
      } else {
        education.idProfile = vm.profile.id;
        educationService.post(education).$promise
          .then(function(newEducation) {
            if (newEducation) {
              vm.educationList.push(newEducation);
              closeModal();
              statusResponse = true;
              toaster.success({body:'Profile saved successfully'});
            }
          })
          .catch(function (error) {
            toaster.error({body: 'An error has occurred!'});
          });
      }
    }

    $scope.$on('syncExperience', saveEducation);
  }
})();