(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .factory('profileService', ProfileService);

  /* @ngInject */
  function ProfileService($resource, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'profiles/:id', {id: '@id'},
    {
      update:{
        method: 'PUT'
      },
      get:{
        method: 'GET'
      },
      post:{
        method: 'POST'
      },
      getAll:{
        url: BASE_API_URI + 'users/me/profiles/',
        method: 'GET',
        isArray: true
      },
      getPercentage:{
        url: BASE_API_URI + 'profiles/:id/completion',
        method: 'GET',
        params: {id: '@id'}
      },
      getSetting:{
        url: BASE_API_URI + 'profiles/:profileId/settings',
        method: 'GET',
        params: {profileId: '@profileId'}
      },
      postSetting:{
        url: BASE_API_URI + 'profiles/:profileId/settings',
        method: 'POST',
        params: {profileId: '@profileId'}
      },
      updateSetting:{
        url: BASE_API_URI + 'profiles/:profileId/settings',
        method: 'PUT',
        params: {profileId: '@profileId'}
      },
      getServices:{
        url: BASE_API_URI + 'profiles/:profileId/services',
        method: 'GET',
        params: {profileId: '@profileId'},
        isArray: true
      },
      postServices:{
        url: BASE_API_URI + 'profiles/:profileId/services',
        method: 'POST',
        params: {profileId: '@profileId'},
        isArray: true
      },
      deleteServices:{
        url: BASE_API_URI + 'profiles/:id/services?services=:services',
        params: {id: '@id', services: '@services'},
        method: 'DELETE',
        isArray: true
      },
      checkLink:{
        url: BASE_API_URI + 'profileLink/:profileId/validate',
        params: {profileId: '@profileId'},
        method: 'GET'
      }
    });
    return service;
  }
})();
