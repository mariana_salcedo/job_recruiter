(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .controller('certificationController', Controller);

  /* @ngInject */
  function Controller($scope, Auth, profileCertificationService, toaster, $uibModal, _, $filter) {

    var vm = this;
    var modalInstance;
    var dateFilter = $filter('date');

    vm.certs = [
      'Course',
      'Master',
      'Certificate'
    ];

    vm.certification = {};
    vm.certifications = [];

    vm.saveCert = saveCert;
    vm.openModal = openModal;
    vm.closeModal = closeModal;
    vm.removeCert = removeCert;
    vm.getCertifications = getCertifications;

    activate();

    /////////////////////

    function activate() {
      vm.profile = Auth.currentProfile();
      getCertifications();
    }

    function openModal(certification) {
      if (!!certification) {
        certification.certificationDate = new Date(certification.certificationDate);
      }
      vm.certification = certification || {};

      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_certification_form.html',
        animation: true,
        scope: $scope
      });
    }

    function closeModal() {

      if (!!modalInstance && modalInstance.close) {

        modalInstance.close();
      }
    }

    function removeCert(certification) {
      return profileCertificationService.delete({
        id: certification.id
      }).$promise
      .then(function remove() {
        _.remove(vm.certifications, certification);
      });
    }

    function getCertifications() {

      var promise = profileCertificationService.get({
        idProfile: vm.profile.id
      }).$promise;

      promise.then(function setCertificates(certifications) {
        vm.certifications = _.map(certifications, function methodName(certification) {
          return _.pick(certification, _.identity);
        });
        console.log('certification', vm.certifications);
      });

      return promise;
    }

    function syncCert() {
      console.log('cert', vm.certification.id);
      if (!!vm.certification.id) {
        return profileCertificationService.update({
          id: vm.certification.id
        }, vm.certification).$promise
          .then(function (data) {
            if (data) {
              toaster.success({body: 'Certification saved successfully!'});
            }
          })
          .catch(function (error) {
            toaster.error({body: 'An error has occurred!'});
          });
      } else {
        return profileCertificationService.post({
          idProfile: vm.profile.id
        }, vm.certification).$promise
        .then(function addCert(certification) {
          vm.certifications.push(certification);
          vm.certification = {};
          return certification;
        })
        .then(function (data) {
          if (data) {
            toaster.success({body: 'Certification saved successfully!'});
          }
        })
        .catch(function (error) {
          toaster.error({body: 'An error has occurred!'});
        });
      }
    }

    function saveCert() {

      syncCert()
      .then(function afterSync() {
        closeModal();
      });
    }
  }
})();
