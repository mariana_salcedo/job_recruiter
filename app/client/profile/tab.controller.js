(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .controller('TabController', TabController);

  /* @ngInject */
  function TabController($scope, toaster, _) {
    var vm = this;

    var profileListen = [
      'ProfileSync',
      'AddressSync',
      'LanguageSync',
      'IndustrySync'
    ];

    vm.tabs = [

      {
        title: 'Basic Info',
        content: '/public/templates/_basic_info.html'
      },
      {
        title: 'Experience & Education',
        content: '/public/templates/_experience_education.html'
      },
      {
        title: 'Certifications & Skills',
        content: '/public/templates/_certifications_skills.html'
      }
    ];

    vm.syncProfile = syncProfile;
    vm.syncExperience = syncExperience;

    var profileToken = listenToken($scope, profileListen);

    function syncProfile() {
      profileToken.token = {};
      $scope.$broadcast('SyncProfile', profileToken.token);
    }

    function syncExperience() {
      $scope.$broadcast('syncExperience');
    }

    function listenToken($scope, listen) {
      var response = {
        token: {}
      };

      _.forEach(listen, function(e) {
        $scope.$on(e, handelResponse);
      });

      function handelResponse(event, token, success) {

        var currentToken = response.token;

        if (token === currentToken) {
          currentToken[event.name] = success;
          checkTokenCompletion(listen, currentToken);
        }
      }

      return response;
    }

    function checkTokenCompletion(listen, currentToken) {

      if (_.size(currentToken) === listen.length) {
        toaster.success({body: 'Profile saved successfully'});
      }
    }

  }
})();
