/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular
    .module('avisare.profile')
    .factory('educationService', educationService);

  /* @ngInject */
  function educationService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'educations/:idEducation', {idEducation: '@idEducation'},
      {
        get:{
          url: BASE_API_URI + 'profiles/:idProfile/educations',
          params: {idProfile: '@idProfile'},
          method: 'GET',
          isArray: true
        },
        post:{
          url: BASE_API_URI + 'profiles/:idProfile/educations',
          params: {idProfile: '@idProfile'},
          method: 'POST'
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        }
      });
    return service;
  }

})();
