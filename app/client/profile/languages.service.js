/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular
    .module('avisare.profile')
    .factory('languagesService', languagesService);

  /* @ngInject */
  function languagesService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'languages/', {},
      {
        get:{
          method: 'GET',
          isArray: true
        },
        getMine:{
          url: BASE_API_URI + 'users/me/languages/',
          method: 'GET',
          isArray: true
        },
        post:{
          url: BASE_API_URI + 'users/me/languages/',
          method: 'POST',
          isArray: true
        },
        delete:{
          url: BASE_API_URI + 'users/me/languages/',
          method: 'DELETE',
          isArray: true
        }
      });
    return service;
  }

})();
