(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .controller('ProfileController', ProfileController);

  /* @ngInject */
  function ProfileController(
    _,
    Auth,
    $scope,
    userService,
    ImageService,
    profileService,
    $filter
  ) {
    var vm = this;
    vm.minDate = new Date();
    var dateFilter = $filter('date');
    vm.functionTooltip = 'You can select a maximum of two functions.';
    vm.industryTooltip = 'You can select a maximum of three industries.';
    vm.saveResume = saveResume;

    vm.title = 'ProfileController';
    // jscs:disable requireCamelCaseOrUpperCaseIdentifiers

    vm.availabilities = [
      'disponible',
      'asuente',
      'ocupado',
      'Always'
    ];

    vm.travelAvaliabilities = [
      'Local or Remote Only',
      '25%',
      '50%',
      '75%',
      'Relocate'
    ];

    vm.syncProfileData = syncProfileData;

    activate();

    //////////////////

    function activate() {
      vm.user = _.clone(Auth.currentUser());
      vm.profile = _.clone(Auth.currentProfile());
      vm.picture = vm.profile.File;
      vm.profile.avaliability = new Date(vm.profile.avaliability);

      delete vm.profile.File;
    }

    function saveResume(file) {
      if (file && file.s3Url) {
        profileService.update({id: vm.profile.id}, {resume: file.s3Url}).$promise
          .then(function assignS3URL() {
            vm.profile.resume = file.s3Url;
          });
      }
    }

    function saveProfile() {

      var saveProfile = _.clone(vm.profile);

      saveProfile.avaliability = dateFilter(saveProfile.avaliability, 'yyyy-MM-dd', 'UTC');

      return userService.update(vm.user)
      .$promise
      .then(function createImg(user) {

        Auth.updateUser(user);
        if (vm.picture && !vm.picture.id) {
          return ImageService.post(vm.picture).$promise;
        }
        return vm.picture;
      })
      .then(function updateProfile(picture) {

        if (vm.picture !== picture) {

          _.assign(vm.picture, picture);

          vm.profile.FileId = picture.id;
          saveProfile.FileId = picture.id;

          Auth.updatePicture(vm.picture);
        }
        return profileService.update(_.pick(saveProfile, _.identity))
        .$promise
        .then(function setProfile(profile) {
          Auth.updateProfile(profile);
        });
      });
    }

    function syncProfileData(event, token) {

      saveProfile()
      .then(function notifySuccess() {
        $scope.$emit('ProfileSync', token, true);
      })
      .catch(function notifyError() {
        $scope.$emit('ProfileSync', token, false);
      });

    }

    $scope.$on('SyncProfile',syncProfileData);
  }
})();
