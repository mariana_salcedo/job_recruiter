(function iife() {
  'use strict';

  angular
    .module('avisare.profile')
    .service('profileCertificationService', profileCertificationService);

  /* @ngInject */
  function profileCertificationService($resource, $q, BASE_API_URI) {

    var service = $resource(
      BASE_API_URI + 'profiles/:idProfile/certifications',
      {idProfile: '@idProfile'},
      {
        post:{
          method: 'POST'
        },
        get:{
          method: 'GET',
          isArray: true
        },
        delete:{
          method: 'DELETE',
          url: BASE_API_URI + 'certifications/:id',
          params: {
            id: '@id'
          }
        },
        update:{
          method: 'PUT',
          url: BASE_API_URI + 'certifications/:id',
          params: {
            id: '@id'
          }
        }
      }
    );
    return service;
  }
})();
