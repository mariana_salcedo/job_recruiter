(function() {
  'use strict';

  angular
      .module('avisare.core')
      .constant('BASE_API_URI','/api/')
      .constant('BASE_APP_URI','/')
      .constant('BASE_CLOUDINARY_URL','http://res.cloudinary.com/avisare/image/upload/')
      .constant('AUTH_EVENTS', {
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
      })
      .constant('USER_ROLES', {
        admin: 'admin_role',
        public: 'consultant_role'
      });
})();
