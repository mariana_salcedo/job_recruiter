(function() {
  'use strict';

  angular
    .module('avisare.core', [
      'angular-lodash',
      'ui.bootstrap',
      'ngResource',
      'ngSanitize',
      'ui.select',
      'ngAnimate',
      'ui.router',
      'toaster',
      'avisare.widget',
      'ngMessages'
    ])
    .constant('_', window._);

})();
