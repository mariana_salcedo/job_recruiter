(function() {
    'use strict';

    angular
        .module('avisare.consultant', [
            'avisare.core', 'avisare.invite', 'ui.grid', 'ui.grid.selection','ui.grid.expandable', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.grid.autoResize'
        ]);
})();
