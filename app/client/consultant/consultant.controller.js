(function() {
  'use strict';

  angular
      .module('avisare.consultant')
      .controller('ConsultantController', ConsultantController)
      .controller('NotesPopOutController', NotesPopOutController)
      .controller('ConsultantPopUpController', ConsultantPopUpController)
      .controller('ReferencesPopUpController', ReferencesPopUpController);

  /* @ngInject */
  function ConsultantController(applicationsServices, $filter, $uibModal, profileIsLoaded, toaster, $animate) {
    var vm = this;
    vm.viewSite = viewSite;
    vm.addEditNotes = addEditNotes;
    vm.showReference = showReference;
    vm.showApplications = showApplications;
    vm.getConsultants = getConsultants;
    vm.getApplications = getApplications;
    vm.updateAppStatus = updateAppStatus;
    vm.checkAvaliability = checkAvaliability;
    vm.updateConsultantStatus = updateConsultantStatus;

    vm.tabs = [
      {
        title: 'Invites',
        content: '/public/templates/_invites.html'
      },
      {
        title: 'Applications',
        content: '/public/templates/_consultant_applications.html'/*,
        disabled: true*/
      },
      {
        title: 'Manage',
        content: '/public/templates/_consultant_manage.html'/*,
        disabled: true*/
      }
    ];

    var appStatus = '<div class="ui-grid-cell-contents">' +
      '<span ng-if="grid.getCellValue(row, col) == \'Pending\'" class="label" style="display: block; background-color: #00C0EC">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Reviewed\'" class="label" style="display: block; background-color: #D14CBB" >{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'New\'" class="label" style="display: block; background-color: #FDA631">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Rejected\'" class="label" style="display: block; background-color: #F11E32">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Bench\'" class="label" style="display: block; background-color: #373737">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Accepted\'" class="label" style="display: block; background-color: #00C561">{{COL_FIELD}}</span>' +
      '</div>';

    var appAction = '<div class="ui-grid-cell-contents">' +
      '<div class="btn-group" dropdown dropdown-append-to-body>' +
        '<button type="button" class="btn btn-xs dropdown-toggle" dropdown-toggle>' +
          'Select an action <span class="caret"></span>' +
        '</button>' +
        '<ul class="dropdown-menu" role="menu">' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Pending\')">Pending</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Reviewed\')">Reviewed</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'New\')">New</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Rejected\')">Rejected</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Bench\')">Bench</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateAppStatus(row.entity, \'Accepted\')">Accepted</a></li>' +
        '</ul>' +
      '</div>' +
    '</div>';

    var addEditBtn = '<div class="ui-grid-cell-contents">' +
      '<button class="btn btn-primary btn-xs" ng-click="grid.appScope.consultant.addEditNotes(\'lg\', row.entity)">ADD & EDIT NOTE</button>' +
      '</div>';

    var appResume = '<div ng-include="\'/public/templates/_application_expand_row.html\'" style="background-color: #FAFAFA"></div>';

    vm.app_paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null,
      query: ''
    };

    vm.applicationGrid = {
      data:[],
      columnDefs:[
        {field:'id', name:'ID', cellEditableCondition:false, enableSorting:false, enableColumnMenu:false},
        {field:'User.firstName', name:'First Name', enableHiding:false},
        {field:'User.lastName', name:'Last Name', enableHiding:false},
        {field:'User.email', name:'Email Address', enableHiding:false},
        {field:'applicationDate', name:'Date Applied', cellFilter: 'date:\'MM/dd/yyyy\'', enableHiding:false},
        {field:'referredBy', name:'Referral', enableHiding:false},
        {field:'status', name:'Status', cellTemplate: appStatus, enableHiding:false},
        {field:'Action', name:'Action', allowCellFocus : false, cellTemplate: appAction, enableSorting:false, enableColumnMenu:false},
        {field:'Notes', name:'Notes', cellTemplate: addEditBtn, enableSorting:false, enableColumnMenu:false}
      ],
      enableRowSelection: false,
      enableRowHeaderSelection: true,
      multiSelect : false,
      modifierKeysToMultiSelect : false,
      /*expandableRowHeight: 500,
      expandableRowTemplate: appResume,*/
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi) {
        vm.appGrid = gridApi;
        $animate.enabled(vm.appGrid.grid.element, false);
        vm.appGrid.selection.on.rowSelectionChanged(null, function(row){
          vm.showApplications(row);
        });
        vm.appGrid.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.app_paginationObj.offset = (newPage - 1) * pageSize;
          vm.app_paginationObj.limit = pageSize;
          vm.getApplications();
        });
        vm.appGrid.core.on.sortChanged(null, function(grid, sortColumns) {
          vm.app_paginationObj.offset = 0;
          if (sortColumns.length == 0) {
            vm.app_paginationObj.order = null;
            vm.app_paginationObj.orderBy = null;
          } else {
            vm.app_paginationObj.order = sortColumns[0].sort.direction;
            vm.app_paginationObj.orderBy = sortColumns[0].field;
          }
          vm.getApplications();
        });
      }
    };

    function getApplications() {
      applicationsServices.get(vm.app_paginationObj).$promise.then(function (res) {
        vm.applications = res.rows;
        vm.applicationGrid.totalItems = res.count;
        vm.applicationGrid.data = vm.applications;
      });
    }
    vm.getApplications();

    function updateAppStatus(appObj, status) {
      var newStatus = {
        id:appObj.id,
        applicationDate: $filter('date')(appObj.applicationDate, 'yyyy-MM-dd', 'UTC'),
        status:status
      };
      if (status === 'Rejected' || status === 'Accepted') {
        applicationsServices.respondApplication({'response': status, id:appObj.id})
        .$promise.then(function(res) {
          toaster.success({body: 'Application ' + status});
          vm.getApplications();
          vm.getConsultants();
        })
        .catch(function() {
          toaster.error({body: 'Unable to Approve application'});
        });
      } else {
        applicationsServices.update(newStatus).$promise.then(function(res) {
          vm.getApplications();
          vm.getConsultants();
        });
      }

    }

    function addEditNotes(size, appObj) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_new_app_note.html',
        controller: 'NotesPopOutController',
        controllerAs: 'note',
        animation: true,
        size: size,
        resolve: {
          application: function () {
            return appObj;
          },
          profileIsLoaded: function () {
            return profileIsLoaded;
          }
        }
      });
      modalInstance.result.then(function (res) {
        vm.getApplications();
      });
    }

    function showReference(reference) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_app_reference.html',
        controller: 'ReferencesPopUpController',
        controllerAs: 'reference',
        animation: true,
        size: 'lg',
        resolve: {
          referenceObj: function () {
            return reference;
          }
        }
      });
    }

    function showApplications(row) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_consultant_popup.html',
        controller: 'ConsultantPopUpController',
        controllerAs: 'consultant',
        animation: true,
        size: 'lg',
        resolve: {
          row: function () {
            return row;
          }
        }
      });
      modalInstance.result.then(function (res) {
        vm.appGrid.selection.clearSelectedRows();
      }, function() {
        vm.appGrid.selection.clearSelectedRows();
      });
    };

    var consultantStatus = '<div class="ui-grid-cell-contents">' +
      '<span ng-if="grid.getCellValue(row, col) == \'Pending\'" class="label" style="display: block; background-color: #00C0EC">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Deleted\'" class="label" style="display: block; background-color: #E11C2F" >{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Active\'" class="label" style="display: block; background-color: #00C561">{{COL_FIELD}}</span>' +
      '<span ng-if="grid.getCellValue(row, col) == \'Accepted\'" class="label" style="display: block; background-color: #00C561">{{COL_FIELD}}</span>' +
      '</div>';

    var consultantAction = '<div class="ui-grid-cell-contents">' +
      '<div class="btn-group" dropdown dropdown-append-to-body>' +
        '<button type="button" class="btn btn-xs dropdown-toggle" dropdown-toggle>' +
          'Select an action <span class="caret"></span>' +
        '</button>' +
        '<ul class="dropdown-menu" role="menu">' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Pending\')">Pending</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Deleted\')">Deleted</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Active\')">Active</a></li>' +
          '<li><a href="#" ng-click="grid.appScope.consultant.updateConsultantStatus(row.entity, \'Accepted\')">Accepted</a></li>' +
        '</ul>' +
      '</div>' +
    '</div>';

    var viewSiteBtn = '<div class="ui-grid-cell-contents">' +
      '<a class="btn btn-primary btn-xs btn-block" href="/consultant/{{row.entity.ProfileSetting.profileLink}}" target="_blank">VIEW SITE</a>' +
      '</div>';

      var ProfileProgress = '<div class="ui-grid-cell-contents">' +
      '      <div class="progress progress-md active"> ' +
      '         <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{{COL_FIELD}}" aria-valuemin="0" aria-valuemax="100" style="width: {{COL_FIELD}}%">{{COL_FIELD}}%</div> ' +
      '      </div>' +
      '</div>';

    var nameLink = '<div class="ui-grid-cell-contents">' +
      '<a href="#" ng-click="grid.appScope.consultant.viewSite(row.entity)">{{row.entity.User.firstName}} {{row.entity.User.lastName}}</a>' +
      '</div>';

    var pictureTemplate = '<div class="ui-grid-cell-contents">' +
      '<span ng-if="grid.appScope.consultant.checkAvaliability(row.entity);" class="badge alert-success" style="padding-top: 0px; padding-right: 0px; font-size: 7px">&nbsp;</span>' +
      '<span ng-if="!grid.appScope.consultant.checkAvaliability(row.entity);" class="badge alert-danger" style="padding-top: 0px; padding-right: 0px; font-size: 7px">&nbsp;</span>' +
      '<img  src="http://res.cloudinary.com/avisare/image/upload/w_50,h_50,c_fill/{{row.entity.File.cloudinaryUrl}}" class="pull-right img-responsive" lazy-src/>' +
      '</div>';

    var consultantResume = '<div ng-include="\'/public/templates/_consultant_expand_row.html\'" style="background-color: #FAFAFA"></div>';

    vm.consultant_paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null,
      query: ''
    };

    vm.consultantsGrid = {
      data:[],
      columnDefs: [
        {field:'Picture', name:'Picture', enableSorting:false, enableColumnMenu:false, cellTemplate:pictureTemplate},
        {field:'User.firstName', name:'Name', enableHiding:false, cellTemplate:nameLink},
        {field:'User.email', name:'Email address', enableHiding:false},
        {field:'User.Addresses[0].City.cityName', name:'City', enableColumnMenu:false, enableSorting:false},
        {field:'User.Addresses[0].City.State.stateName', name:'State', enableColumnMenu:false, enableSorting:false},
        {field:'status', name:'Status', enableHiding:false, cellTemplate: consultantStatus},
        {field:'Action', name:'Action', cellTemplate: consultantAction, enableSorting:false, enableColumnMenu:false},
        {field:'percentage', name:'Profile',  enableColumnMenu:false, enableSorting:false, cellTemplate: ProfileProgress},
        {field:'Site', name:'Site', enableSorting:false, enableColumnMenu:false, cellTemplate: viewSiteBtn}
      ],
      rowHeight:70,
      expandableRowTemplate: consultantResume,
      expandableRowHeight: 300,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi) {
        vm.consultantGrid = gridApi;
        /*vm.consultantGrid.edit.on.beginCellEdit(null, function(rowEntity, colDef, newValue, oldValue) {
          //editIndustries(rowEntity);
        });*/
        $animate.enabled(vm.consultantGrid.grid.element, false);
        vm.consultantGrid.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.consultant_paginationObj.offset = (newPage - 1) * pageSize;
          vm.consultant_paginationObj.limit = pageSize;
          vm.getConsultants();
        });
        vm.consultantGrid.core.on.sortChanged(null, function(grid, sortColumns) {
          vm.consultant_paginationObj.offset = 0;
          if (sortColumns.length === 0) {
            vm.consultant_paginationObj.order = null;
            vm.consultant_paginationObj.orderBy = null;
          } else {
            vm.consultant_paginationObj.order = sortColumns[0].sort.direction;
            vm.consultant_paginationObj.orderBy = sortColumns[0].field;
          }
          vm.getConsultants();
        });

        vm.consultantGrid.expandable.on.rowExpandedStateChanged(null ,function(row){
          applicationsServices.getConsultantDetails({id:row.entity.id}).$promise.then(function (res) {
            res.percentage = row.entity.percentage;
            row.entity = res;
          });
        });
      }
    };

    function getConsultants() {
      applicationsServices.getConsultants(vm.consultant_paginationObj).$promise.then(function(res) {
        vm.consultants = res.rows;
        vm.consultantsGrid.totalItems = res.count;
        vm.consultantsGrid.data = vm.consultants;
        _(vm.consultantsGrid.data).forEach(function(n){
          applicationsServices.getCompletion({id:n.id}).$promise.then(function(response){
            n.percentage = response.percentage;
          });
        });
      }); 
    }
    vm.getConsultants();

    function viewSite (data) {
      // vm.tempConsultant = data;
      // var tabObj = {
      //   title: data.User.firstName + ' ' + data.User.lastName,
      //   content: '/public/templates/_consultant_records.html',
      // };
      // vm.tabs.push(tabObj);
    }

    function updateConsultantStatus(consultantObj, status) {
      var newStatus = {
        id:consultantObj.id,
        avaliability: $filter('date')(consultantObj.avaliability, 'yyyy-MM-dd', 'UTC'),
        status:status
      };
      applicationsServices.updateConsultant(newStatus).$promise.then(function(res) {
        toaster.success({body: 'Update successful'});
        vm.getConsultants();
      });
    }

    function checkAvaliability(consultantObj){
      return new Date(consultantObj.avaliability) < new Date();
    }
  }

  function NotesPopOutController($uibModalInstance, application, applicationsServices, profileIsLoaded) {
    var vm = this;
    vm.routeMessages = '/public/templates/_note_app_error_messages.html';
    vm.notes = application.ApplicationNotes;
    vm.newNote = '';
    vm.noteObj = null;
    vm.editMode = false;

    vm.saveNote = saveNote;
    vm.editNote = editNote;
    vm.removeNote = removeNote;

    function editNote(index) {
      vm.editMode = true;
      vm.noteForm.$setPristine();
      vm.noteForm.$setUntouched();
      vm.noteObj = vm.notes[index];
      vm.newNote = vm.notes[index].note;
    }

    function saveNote() {
      if (vm.editMode) {
        vm.noteObj.note = vm.newNote;
        applicationsServices.updateNote(vm.noteObj).$promise.then(function (res) {
          $uibModalInstance.close();
        });
      } else {
        vm.noteObj = {
          id: application.id,
          note: vm.newNote,
          User: {
            id: profileIsLoaded.id,
            firstName: profileIsLoaded.firstName,
            lastName: profileIsLoaded.lastName
          }
        };
        vm.notes.push(vm.noteObj);
        applicationsServices.postNote(vm.noteObj).$promise.then(function (res) {
          $uibModalInstance.close();
        });
      }
    };

    function removeNote(index) {
      vm.noteObj = vm.notes[index];
      applicationsServices.deleteNote(vm.noteObj).$promise.then(function(res) {
        vm.notes.splice(index, 1);
      });
    };
  }

  function ReferencesPopUpController(referenceObj){
    var vm = this;
    vm.reference = referenceObj;
  };

  function ConsultantPopUpController(row, $filter, $location, _, applicationsServices){
    var vm = this;
    applicationsServices.getDetails({id:row.entity.id}).$promise.then(function (res) {
      res.applicationDate = $filter('date')(res.applicationDate, 'MM/dd/yyyy', 'UTC');
      _.forEach(res.References, function(obj, val){
        obj.deserialized = {};
        try {
          obj.deserialized = JSON.parse(obj.response);
        }
        catch(e) {}
      });
      vm.consultant = res;
    });
  };
})();
