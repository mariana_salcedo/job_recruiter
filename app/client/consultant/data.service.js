(function () {
  'use strict';

  angular
    .module('avisare.consultant')
    .factory('applicationsServices', applicationsServices);

  /* @ngInject */
  function applicationsServices($resource, $q, BASE_API_URI){
    var service = $resource(BASE_API_URI + 'applications/:id', {id: '@id'},
      {
        get:{
          method: 'GET'
        },
        getDetails:{
          method: 'GET'
        },
        getConsultants: {
          url: BASE_API_URI + 'consultants',
          method: 'GET'
        },
        getConsultantDetails: {
          url: BASE_API_URI + 'consultants/:id',
          method: 'GET'
        },
        getCompletion: {
          url: BASE_API_URI + 'profiles/:id/completion',
          method: 'GET'
        },
        post:{
          method: 'POST'
        },
        postNote:{
          url: BASE_API_URI + 'applications/:id/notes',
          method: 'POST'
        },
        respondApplication:{
            url: BASE_API_URI + 'applications/:id/response',
            method: 'PUT'
        },
        update:{
          method: 'PUT'
        },
        updateConsultant: {
          url: BASE_API_URI + 'profiles/:id',
          method: 'PUT'
        },
        updateNote:{
          url: BASE_API_URI + 'notes/:id',
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        },
        deleteNote:{
          url: BASE_API_URI + 'notes/:id',
          method: 'DELETE'
        }
      });
    return service;
  }
})();
