(function () {
  'use strict';

  angular
      .module('avisare.skill')
      .factory('skillsService', skillsService);

  /* @ngInject */
  function skillsService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'skills/:id', {id: '@id'},
      {
        get:{
          method: 'GET'
        },
        getAll:{
          method: 'GET',
          isArray: true,
          url: BASE_API_URI + 'skills'
        },
        post:{
          method: 'POST'
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        }
      });
    return service;
  }
})();
