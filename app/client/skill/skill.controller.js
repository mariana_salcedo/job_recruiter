/**
 * avisare
 * Created by Ricardo Colmenarez on 11/11/15.
 * email rcolmenarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular.module('avisare.skill')
      .controller('SkillController', SkillController)
      .controller('SkillModalCtrl', SkillModalCtrl)
      .controller('EditSkillModalCtrl', EditSkillModalCtrl);

  /* @ngInject */

  function SkillController(
    $uibModal,
    toaster,
    skillsService,
    _,
    $animate) {
    var vm = this;

    vm.getSkills = getSkills;
    vm.showForm = showForm;
    vm.editSkill = editSkill;
    vm.deleteSkill = deleteSkill;

    vm.tabs = [
      {
        title: 'Basic Info',
        content: '/public/templates/_skill_info.html'
      }
    ];

    vm.paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null
    };

    vm.gridOptions = {
      columnDefs: [
        {field: 'id', displayName: 'ID', cellEditableCondition:false},
        {field: 'skillName', displayName: 'Name'},
        {field: 'skillDescription', displayName: 'Description'},
        {field: 'skillStatus', displayName: 'Status', cellTemplate:'<div class="ui-grid-cell-contents">{{COL_FIELD ? \'Enabled\' : \'Disabled\'}}</div>'},
        {field: 'createdAt', displayName: 'Created At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false},
        {field: 'updatedAt', displayName: 'Updated At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false},
        {field: 'deletedAt', displayName: 'Deleted At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false},
        { name: 'Action', cellEditableCondition:false, cellTemplate: '<div class="ui-grid-cell-contents"><button class="btn btn-danger btn-xs btn-block" ng-click="grid.appScope.skill.deleteSkill(row.entity.id)"><span class="glyphicon glyphicon-trash"></span></button></div>'}
      ],
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;
        $animate.enabled(vm.gridApi.grid.element, false);
        vm.gridApi.edit.on.beginCellEdit(null, function(rowEntity, colDef, newValue, oldValue) {
          editSkill(rowEntity);
        });
        vm.gridApi.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.paginationObj.offset = (newPage - 1) * pageSize;
          vm.paginationObj.limit = pageSize;
          vm.getSkills();
        });
        vm.gridApi.core.on.sortChanged(null, function(grid, sortColumns) {
          if (sortColumns.length === 0) {
            vm.paginationObj.order = null;
            vm.paginationObj.orderBy = null;
          } else {
            vm.paginationObj.order = sortColumns[0].sort.direction;
            vm.paginationObj.orderBy = sortColumns[0].name;
          }
          vm.getSkills();
        });
      }
    };

    getSkills();
    function getSkills() {
      skillsService.get(vm.paginationObj).$promise.then(function (res) {
        vm.skills = res.rows;
        vm.gridOptions.totalItems = res.count;
        vm.gridOptions.data = vm.skills;
      });
    }

    // Formulario Modal
    function showForm(size) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_skill_form.html',
        controller: 'SkillModalCtrl',
        controllerAs: 'skill',
        animation: true,
      });
      modalInstance.result.then(function (res) {
        //vm.skills.push(res);
        vm.getSkills();
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function editSkill(skillObject) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_skill_form.html',
        controller: 'EditSkillModalCtrl',
        controllerAs: 'skill',
        animation: true,
        resolve: {
          skill: function () {
            return skillObject;
          }
        }
      });
      modalInstance.result.then(function (res) {
        getSkills();
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function deleteSkill(id) {
      skillsService.delete({id:id}).$promise.then(function(res) {
        getSkills();
      });
    }
  }

  function SkillModalCtrl($uibModalInstance, skillsService) {
    var vm = this;
    vm.addNewSkill = addNewSkill;
    vm.skill = {};
    vm.skill.skillStatus = false;
    function addNewSkill(skill) {
      var newSkill = {
        skillName: skill.skillName,
        skillDescription: skill.skillDescription,
        skillStatus: skill.skillStatus
      };
      skillsService.post(newSkill).$promise
        .then(function(res) {
          $uibModalInstance.close(res);
        });
    }
  }

  function EditSkillModalCtrl($uibModalInstance, skill, skillsService) {
    var vm = this;
    vm.addNewSkill = addNewSkill;

    vm.skill = {};
    vm.skill.id = skill.id;
    vm.skill.skillStatus = skill.skillStatus;
    vm.skill.skillName = skill.skillName;
    vm.skill.skillDescription = skill.skillDescription;

    function addNewSkill(skill) {
      var newSkill = {
        id: skill.id,
        skillName: skill.skillName,
        skillDescription: skill.skillDescription,
        skillStatus: skill.skillStatus
      };

      skillsService.update(newSkill).$promise.then(function() {
        $uibModalInstance.close(vm.experience);
      });

    }
  }

})();