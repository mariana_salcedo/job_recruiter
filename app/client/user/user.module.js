(function() {
  'use strict';

  angular
      .module('avisare.user', [
          'avisare.core',
          'avisare.auth'
      ]);
})();
 