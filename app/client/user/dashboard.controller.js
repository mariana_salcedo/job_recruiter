(function() {
  'use strict';

  angular
    .module('avisare.user')
    .controller('DashboardController', DashboardController);
  /* @ngInject */
  function DashboardController(Auth, profileService, userService) {
    var vm = this;
    vm.closeMessage = function() {
      userService.disableMessage({field: 'welcomeMessage'}).$promise.then(function() {
        Auth.currentUser().welcomeMessage = true;
      });

    }
    activate();

    ////////////////////

    function activate() {

      Auth.profileDone.then(function setData() {
        profileService.getPercentage({id: Auth.currentProfile().id}).$promise
        .then(function setPercentage(result) {
          vm.totalProfile = result.percentage;
          vm.showWelcomeMessage = Auth.currentUser().welcomeMessage;
        });
      });
    }

  }
})();
