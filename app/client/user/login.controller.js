(function() {
  'use strict';

  angular
    .module('avisare.user')
    .controller('LoginController', LoginController);

  function LoginController($rootScope, Auth, $window, $state) {
    var vm = this;
    vm.routeMessages = '/public/templates/_login_error_messages.html';

    // get info if a person is logged in
    vm.loggedIn = Auth.isLoggedIn();
    vm.doLogout = doLogout;
    vm.doLogin = doLogin;

    // function to handle login form
    function doLogin() {
      vm.processing = true;

      // clear the error
      vm.error = '';

      Auth.login(vm.loginData.username, vm.loginData.password)
        .then(function(data) {

          vm.processing = false;
          // if a user successfully logs in, redirect to users page
          if (data) {
            $state.transitionTo('consultant.dashboard');
          }

        });
    }

    // function to handle logging out
    function doLogout() {
      Auth.logout();
      vm.user = '';

      $state.transitionTo('login');
    }

  }
})();
