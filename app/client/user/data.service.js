(function() {
  'use strict';

  angular
    .module('avisare.user')
    .service('userService', userService);

  /* @ngInject */
  function userService($resource, BASE_API_URI) {

    var service = $resource(BASE_API_URI + 'users/me', {},
    {
      get:{
        method: 'GET'
      },
      getMe:{
        method: 'GET',
        url: BASE_API_URI + 'users/me',
        params: {
          include: true
        }
      },
      update:{
        method: 'PUT'
      },
      disableMessage:{
        method: 'PUT',
        url: BASE_API_URI + 'dismiss/:field',
        params: {field: '@field'}
      },
      changePass:{
        method: 'PUT',
        url:BASE_API_URI + 'users/me/changePassword'
      }
    });

    return service;
  }
})();
