
(function() {
  'use strict';

  angular
    .module('avisare.user')
    .controller('UserController', UserController);

  /* @ngInject */
  function UserController(
    Auth,
    addressService,
    cityService,
    _,
    $state,
    $rootScope,
    $uibModal,
    $scope,
    userService,
    toaster
  ) {
    var vm = this;
    var modalInstance;
    vm.changePassword = changePassword;
    vm.openChangePassModal = openChangePassModal;
    vm.closeModal = closeModal;
    vm.changePass = {};
    ////////////////////

    activate();

    function changePassword() {
      if (vm.changePass.newPassword === vm.changePass.repeatPassword) {
        userService.changePass(vm.changePass).$promise
          .then(function (data) {
            if (data) {
              toaster.success({body: 'Password was changed successfully!'});
              closeModal();
            }
          })
          .catch(function (error) {
            if (error.status === 403) {
              toaster.error({body: 'Invalid current password'});
            } else {
              toaster.error({body: 'An error has occurred!'});
            }
          });
      }
    }

    function openChangePassModal() {
      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_change_password_form.html',
        animation: true,
        scope: $scope
      });
      modalInstance.result.finally(function resetModel() {
        vm.changePass = {};
        vm.repeatedPasswordCP = undefined;
      });
    }

    function closeModal() {

      if (!!modalInstance && modalInstance.close) {

        modalInstance.close();
      }
    }

    function activate() {

      Auth.profileDone
        .then(function setData() {
          vm.user = Auth.currentUser();
          vm.CurrentDate = new Date();
          vm.profile = Auth.currentProfile();
          vm.isAdmin = Auth.isAdmin();

          addressService.get()
          .$promise.then(function setAddress(addreses) {

            if (!_.isEmpty(addreses)) {
              var firstAddress = _.first(addreses).Address;

              return cityService.getById({id: firstAddress.CityId})
                .$promise.then(function setCity(city) {
                  vm.city = city;
                  vm.state = city.State;
                });
            }

          });
          if ($state.$current.parent.isAdmin) {
            vm.dashboard = 'Admin';
          }else {
            vm.dashboard = 'Consultant';
          }
        });

      vm.toggleSideBar = toggleSideBar;

      function toggleSideBar() {
        if ($rootScope.bodylayout === 'sidebar-collapse') {
          $rootScope.bodylayout = 'sidebar-open';
        } else {
          $rootScope.bodylayout = 'sidebar-collapse';
        }
      }
    }
  }
})();
