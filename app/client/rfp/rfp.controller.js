(function() {
  'use strict';

  angular
      .module('avisare.rfp')
      .controller('RFPController', RFPController)
      .controller('RFPPreviewModalController', RFPPreviewModalController);
  /* @ngInject */
  function RFPController(rfpService, ServicesService, userService, _, toaster, $animate, $uibModal, $scope) {
    var vm = this;
    var clientId;
    vm.saveRFP = saveRFP;
    vm.previewRFP = previewRFP;
    vm.loadServices = loadServices;
    vm.tagTransform = tagTransform;
    vm.addConsultant = addConsultant;
    vm.setDescription = setDescription;
    vm.removeConsultant = removeConsultant;
    vm.toggleAditionalFields = toggleAditionalFields;

    vm.rfpObject = {};
    vm.rfpDuration = false;
    vm.rfpObject.private = false;
    vm.selectedConsultants = [];
    vm.RfpMine = [];

    vm.additionalFields = {
      deliverables : false,
      tools        : false,
      paymentTerms : false,
      requirements : false,
      contactPerson: false,
      other        : false
    };

    function toggleAditionalFields(field) {
      vm.additionalFields[field] = !vm.additionalFields[field];
    }

    activate();

    function activate() {

      var actions = '<div class="ui-grid-cell-contents">' +
        '<div class="btn-group" dropdown dropdown-append-to-body>' +
          '<button type="button" class="btn btn-xs dropdown-toggle" dropdown-toggle>' +
            'Select an action <span class="caret"></span>' +
          '</button>' +
          '<ul class="dropdown-menu" role="menu">' +
            '<li><a href="#" ng-click="grid.appScope.rfpCtrl">View</a></li>' +
            '<li><a href="#" ng-click="grid.appScope.rfpCtrl">Submit interest</a></li>' +
            '<li><a href="#" ng-click="grid.appScope.rfpCtrl">Save this RFP</a></li>' +
            '<li><a href="#" ng-click="grid.appScope.rfpCtrl">Report this RFP</a></li>' +
          '</ul>' +
        '</div>' +
      '</div>';

      var titleLink = '<div class="ui-grid-cell-contents">' +
        '<a href="#" ng-click="grid.appScope.rfpCtrl">{{row.entity.title}} </a>' +
        '</div>';

      var pictureTemplate = '<div class="ui-grid-cell-contents">' +
        '<img  src="http://res.cloudinary.com/avisare/image/upload/w_150,h_150,c_fill/v1450190465/ofwejr0webjvsvse233o.jpg" class="pull-right img-responsive" lazy-src/>' +
        '</div>';

      var consultantResume = '<div class="ui-grid-cell-contents" style="background-color: #FAFAFA">' +
        '<div class="row">' +
        ' <div class="col-md-2" style="color: #7B7B7B;">' +
        '   <strong>RFP ID:</strong>' +
        ' </div>' +
        ' <div class="col-md-10">' +
        '   <span class="blueAvisare">#{{row.entity.id}} </span>' +
        ' </div>' +
        '</div>' +
        '<div class="row">' +
        ' <div class="col-md-2" style="color: #7B7B7B;">' +
        '   <strong>Executive Summary:</strong>' +
        ' </div> ' +
        ' <div class="col-md-10">' +
        '   <p>{{row.entity.summary}} </p>' +
        ' </div>' +
        '</div> ' +
      '</div>';

      var industriesRfp = '<div class="ui-grid-cell-contents">' +
      '  <span ng-repeat="ind in row.entity.Industries">{{$index==row.entity.Industries.length-1 ? ind.industryName : ind.industryName+"," }}&nbsp; </span>' +
      '</div>';

      var servicesRfp = '<div class="ui-grid-cell-contents">' +
      '  <span ng-repeat="ind in row.entity.Services">{{$index==row.entity.Services.length-1 ? ind.serviceName : ind.serviceName+","}}&nbsp;</span>' +
      '</div>';

      var durationRfp = '<div class="ui-grid-cell-contents">' +
      '  <span>{{row.entity.duration == -1 || row.entity.duration==null ? "Undefined" : (row.entity.duration == 1 ? row.entity.duration +" month" : row.entity.duration+" months")}}</span>' +
      '</div>';

      vm.paginationObj = {
        limit : 25,
        offset: 0,
        order: null,
        orderBy: null,
        query: ''
      };
      vm.rfpGrid = {
        data:[],
        columnDefs: [

          {field:'title', name:'Title', enableHiding:false, cellTemplate:titleLink},
          {field:'Industries', name:'Industries', enableHiding:false, cellTemplate:industriesRfp},
          {field:'Services', name:'Services', enableHiding:false, cellTemplate:servicesRfp},
          {field:'duration', name:'Duration', enableHiding:false, cellTemplate:durationRfp},
          {field:'Action', name:'Action', cellTemplate: actions, enableSorting:false, enableColumnMenu:false}
        ],

        expandableRowTemplate: consultantResume,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        useExternalPagination: true,
        useExternalSorting: true,
        onRegisterApi: function(gridApi) {
          vm.rfpGrid = gridApi;
          /*vm.rfpGrid.edit.on.beginCellEdit(null, function(rowEntity, colDef, newValue, oldValue) {
            //editIndustries(rowEntity);
          });*/
          $animate.enabled(vm.rfpGrid.grid.element, false);
          vm.rfpGrid.pagination.on.paginationChanged(null, function (newPage, pageSize) {
            vm.paginationObj.offset = (newPage - 1) * pageSize;
            vm.paginationObj.limit = pageSize;
            vm.getRFP();
          });
          vm.rfpGrid.core.on.sortChanged(null, function(grid, sortColumns) {
            if (sortColumns.length === 0) {
              vm.paginationObj.order = null;
              vm.paginationObj.orderBy = null;
            } else {
              vm.paginationObj.order = sortColumns[0].sort.direction;
              vm.paginationObj.orderBy = sortColumns[0].field;
            }
            vm.getRFP();
          });
        }
      };
      vm.rfpMineGrid = {
        data:[],
        columnDefs: [

          {field:'title', name:'Title', enableHiding:false, cellTemplate:titleLink},
          {field:'Industries[0]', name:'Industries', enableHiding:false},
          {field:'Services[0]', name:'Services', enableHiding:false},
          {field:'duration', name:'Duration', enableHiding:false},
          {field:'Action', name:'Action', cellTemplate: actions, enableSorting:false, enableColumnMenu:false}
        ],

        expandableRowTemplate: consultantResume,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        useExternalPagination: true,
        useExternalSorting: true,
        onRegisterApi: function(gridApi) {
          vm.rfpGrid = gridApi;
          /*vm.rfpGrid.edit.on.beginCellEdit(null, function(rowEntity, colDef, newValue, oldValue) {
            //editIndustries(rowEntity);
          });*/
          $animate.enabled(vm.rfpGrid.grid.element, false);
          vm.rfpGrid.pagination.on.paginationChanged(null, function (newPage, pageSize) {
            vm.paginationObj.offset = (newPage - 1) * pageSize;
            vm.paginationObj.limit = pageSize;
            vm.getMineRFP();
          });
          vm.rfpGrid.core.on.sortChanged(null, function(grid, sortColumns) {
            if (sortColumns.length === 0) {
              vm.paginationObj.order = null;
              vm.paginationObj.orderBy = null;
            } else {
              vm.paginationObj.order = sortColumns[0].sort.direction;
              vm.paginationObj.orderBy = sortColumns[0].field;
            }
            vm.getMineRFP();
          });
        }
      };
      getRFP();
      getMineRFP();

    }

    function getRFP() {
      rfpService.get().$promise
      .then(function (res) {
        vm.rfpGrid.totalItems = res.length;
        vm.rfpGrid.data = res;
      });
    }

    function getMineRFP() {
      // getting the client Id
      userService.getMe().$promise
        .then(function(user) {
          clientId = user.Accounts[0].id;
          console.log('Trying:',clientId, user);
          rfpService.getMine({id: clientId}).$promise
            .then(function(res) {
              vm.RfpMine = res;
              console.log('Mis rfp', vm.RfpMine);
              vm.rfpMineGrid.totalItems = res.length;
              vm.rfpMineGrid.data = res;
            });
        });
    }

    function addConsultant() {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_rfp_search_consultant.html',
        controller: 'ClientController',
        controllerAs: 'client',
        animation: true,
        size: 'lg',
        scope: $scope
      });
    }

    function previewRFP(){
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_rfp_preview_layout.html',
        controller: 'RFPPreviewModalController',
        controllerAs: 'rfp',
        animation: true,
        size: 'lg',
        resolve: {
          rfpObject: function () {
            return vm.rfpObject;
          }
        }
      });
    }

    function removeConsultant(index) {
      vm.selectedConsultants.splice(index, 1);
    }

    function saveRFP(){
      var industriesArray = [];
      _.forEach(vm.rfpObject.industries, function(val){
        industriesArray.push(val.id);
      });

      var servicesArray = [];
      _.forEach(vm.rfpObject.services, function(val){
        servicesArray.push(val.id);
      });
      var newRfp = _.clone(vm.rfpObject);
      newRfp.industries = industriesArray;
      newRfp.services = servicesArray;
      newRfp.status = 'Created';
      newRfp.AccountId = clientId;
      newRfp.duration = vm.rfpDuration ? -1 : newRfp.duration;
      if (vm.rfpObject.private){
        var consultantsArray = [];
        _.forEach(vm.selectedConsultants, function(val){
          consultantsArray.push(val.id);
        });
        newRfp.consultants = consultantsArray;
      }
      console.log(newRfp);
      rfpService.post(newRfp).$promise.then(function(res){
        toaster.success({body: 'RFP created Successfully'});
      });
    }

    function tagTransform(newTag) {
      var item = {
        serviceName: newTag,
        serviceDescription: ''
      };
      return item;
    }

    function setDescription($item, $model) {
      $item.serviceDescription = '';
      return $item;
    }

    function removeService($item) {
      if ($item.id) {
        var objDelete = {
          id: vm.profile.id,
          services: [$item.id]
        };
        return profileService.deleteServices(objDelete).$promise
          .then(function methodName() {
            _.remove(vm.myServices, function removeElement(service) {
              return service.id === $item.id;
            });
          });
      }
    }

    function loadServices(){
      vm.serviceArray = ServicesService.get();
      console.log('00', vm.serviceArray);
    }
    loadServices();
  }

  function RFPPreviewModalController($uibModalInstance, rfpObject) {
    var vm = this;
    vm.rfpObject = rfpObject;
  }
})();
