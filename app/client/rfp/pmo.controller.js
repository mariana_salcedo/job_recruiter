(function iife() {
  'use strict';

  angular
    .module('avisare.rfp')
    .controller('TabsPMOController', TabsPMOController);

  /* @ngInject */
  function TabsPMOController($scope, toaster, _) {
    var vm = this;

    vm.tabs = [

      {
        title: 'Open RFPs',
        content: '/public/templates/_open_rfps.html'
      },
      {
        title: 'Current Projects',
        content: '/public/templates/_experience_education.html'
      },
      {
        title: 'Closed Projects',
        content: '/public/templates/_certifications_skills.html'
      }
    ];

  }
})();
