(function () {
  'use strict';

  angular
    .module('avisare.rfp')
    .factory('rfpService', rfpService);

  /* @ngInject */
  function rfpService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'rfps', {},
      {
        get:{
          method: 'GET',
          isArray: true
        },
        post:{
          method: 'POST',
          isArray: true
        },
        update:{
          method: 'PUT',
          url: BASE_API_URI + 'rfp/:id',
          params: {id: '@id'}
        },
        getMine:{
          method: 'GET',
          url: BASE_API_URI + 'rfps/client/:id',
          params: {id: '@id'},
          isArray: true
        }
      });
    return service;
  }
})();