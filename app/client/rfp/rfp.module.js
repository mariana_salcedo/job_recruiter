(function() {
  'use strict';

  angular
      .module('avisare.rfp', [
          'avisare.core', 'NgSwitchery'
      ]);
})();
