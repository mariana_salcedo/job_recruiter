(function() {
  'use strict';

  angular
    .module('avisare.requestReset')
    .controller('RequestResetController', RequestResetController);

  /* @ngInject */
  function RequestResetController(requestResetService, $location, toaster,$window) {
    var vm = this;
    vm.routeMessages = '/public/templates/_login_error_messages.html';
    vm.email = '';
    vm.request = function() {
      requestResetService.get({
        email: vm.email
      }).$promise
        .then(function(data) {
          if (data) {
            toaster.success({body: 'Reset Successful!'});
            $location.url('/');
            $window.location.reload();
          }
        })
        .catch(function(error) {
          if (error.status === 404) {
            toaster.error({body: 'Email not found'});
          }else {
            toaster.error({body: 'An error has occurred!'});
          }
        });

    };

  }
})();
