(function() {
  'use strict';

  angular
      .module('avisare.requestReset', [
          'avisare.core'
      ])
    .config(configHttpReset);

  /* @ngInject */
  function configHttpReset($locationProvider) {
    $locationProvider.html5Mode(true);
  }
})();
