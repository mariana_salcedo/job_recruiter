(function() {
  'use strict';

  angular
    .module('avisare.requestReset')
    .service('requestResetService', requestResetService);

  /* @ngInject */
  function requestResetService($resource, BASE_API_URI) {

    var service = $resource(BASE_API_URI + 'users/reset', {},
    {
      get:{
        method: 'GET'
      }
    });

    return service;
  }
})();
