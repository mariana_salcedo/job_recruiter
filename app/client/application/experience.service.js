/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular
    .module('avisare.application')
    .factory('experienceService', experienceService);

  /* @ngInject */
  function experienceService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'experiences/:idExperience',
      {idExperience: '@idExperience'},
      {
        get:{
          method: 'GET',
          isArray: true
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        },
        getMeExperience:{
          url: BASE_API_URI + 'profiles/:idProfile/experiences',
          params: {idProfile: '@idProfile'},
          method: 'GET',
          isArray: true
        },
        post:{
          url: BASE_API_URI + 'profiles/:idProfile/experiences',
          params: {idProfile: '@idProfile'},
          method: 'POST'
        }
      });
    return service;
  }

})();
