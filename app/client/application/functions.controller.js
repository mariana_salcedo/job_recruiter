/**
 * avisare
 * Created by Andres Juarez on 10/12/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .controller('FunctionController', FunctionController);

  /* @ngInject */
  function FunctionController($scope, Auth, functionsService, $q, _) {
    var vm = this;

    vm.title = 'FunctionController';
    var profileId = null;
    var currentFunctionId = null;
    vm.functionsSelected = [];
    vm.functionSelected = [];
    vm.saveFunctionChanges = saveFunctionChanges;
    var currentFunction = null;
    vm.functions = [];

    activate();

    ///////////////////

    function activate() {

      Auth.profileDone.then(function setData() {
        vm.user = Auth.currentUser();
        vm.profile = Auth.currentProfile();
        profileId = vm.profile.id;
        functionsService.getMe({profileId: profileId}).$promise
          .then(function(res) {
            vm.functionsSelected = _.pluck(res, 'IndustryFunction');

            if (vm.functionsSelected.length > 0) {
              vm.functionSelected = vm.functionsSelected[0];

              currentFunctionId = vm.functionSelected.id;
            }
            currentFunction = _.pluck(res, 'IndustryFunction');
          });
      });

      // getting all functions from DB
      functionsService.get({}).$promise.then(function (res) {
        vm.functions = res;
      });
    }

    function saveFunctionChanges() {
      console.log("function");
      var newUserFunctions = [];
      var statusResponse = true;
      var newFunctionsId = _.difference(
        _.pluck(vm.functionsSelected, 'id'),
        _.pluck(currentFunction, 'id')
      );
      var currentsId = _.pluck(currentFunction, 'id');
      var allIds = _.union(newFunctionsId, currentsId);
      var oldFunctionsId = _.difference(allIds, _.pluck(vm.functionsSelected, 'id'));
      newUserFunctions = _.map(newFunctionsId, function(id) {
        return {id: id};
      });
      var promises = [];
      if (newFunctionsId.length > 0) {
        // Saving user languages
        _.forEach(vm.functionsSelected, function(obj) {
          if (_.contains(newFunctionsId, obj.id)) {
            obj.profileId = profileId;
            obj.industryFunctionId = obj.id;
            promises.push(
              functionsService.post(_.pick(obj, _.identity)).$promise
            );
          }
        });
      }

      if (oldFunctionsId.length > 0) {
        _.forEach(oldFunctionsId, function (id) {
          promises.push(
            functionsService.delete({profileId: profileId, functionId: id}).$promise
          );
        });
      }

      if (promises.length) {
        return $q.all(promises);
      } else {
        return $q.resolve();
      }
    }

    function saveFunctionApplicationChanges() {
      console.log("functionApp", !_.isEmpty(vm.functionsSelected));

      var promises = [];
      profileId = Auth.currentProfile().id;

      if (!!vm.functionSelected && currentFunctionId !== vm.functionSelected.id) {
        if (!!currentFunction && currentFunction.length > 0) {
          _.forEach(currentFunction, function (obj) {
            promises.push(
              functionsService.delete({profileId: profileId, functionId: obj.id}).$promise
            );
          });
        }
        vm.functionSelected.profileId = profileId;
        vm.functionSelected.industryFunctionId = vm.functionSelected.id;

        if (!_.isEmpty(vm.functionSelected)) {
          promises.push(
            functionsService.post(_.pick(vm.functionSelected, _.identity)).$promise
          );
        };
        
      }
      if (promises.length) {
        return $q.all(promises);
      } else {
        return $q.resolve();
      }
    }

    function syncFunction(event, token) {
      saveFunctionChanges()
      .then(function notifySuccess() {
        $scope.$emit('FunctionSync', token, true);
      });
    }
    function syncFunctionApplication(event, token) {
      saveFunctionApplicationChanges()
      .then(function notifySuccess() {
        $scope.$emit('FunctionSync', token, true);
      })
      .catch(function() {
        $scope.$emit('FunctionSync', token, false);
      });
    }

    $scope.$on('SyncProfile', syncFunction);
    $scope.$on('FunctionSetSync', syncFunctionApplication);
  }
})();
