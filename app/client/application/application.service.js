(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .factory('ApplicationService', ApplicationService);

  /* @ngInject */
  function ApplicationService($resource, BASE_API_URI) {

    var service = $resource(BASE_API_URI + 'users/applications', {},
    {
      post:{
        method: 'POST'
      },
      update:{
        method: 'PUT'
      },
      // TODO normalize settings to include it on this module
      getSetting:{
        url: BASE_API_URI + 'profiles/:profileId/settings',
        method: 'GET',
        params: {profileId: '@profileId'}
      },
      postSetting:{
        url: BASE_API_URI + 'profiles/:profileId/settings',
        method: 'POST',
        params: {profileId: '@profileId'}
      },
      updateSetting:{
        url: BASE_API_URI + 'profiles/:profileId/settings',
        method: 'PUT',
        params: {profileId: '@profileId'}
      },
      myApplication: {
        url: BASE_API_URI + 'applications/me',
        method: 'GET'
      },
      addApplication: {
        url: BASE_API_URI + 'applications/me',
        method: 'POST'
      },
      completeApplication: {
        url: BASE_API_URI + 'applications/me/complete',
        method: 'PUT'
      },
      updateApplication: {
        url: BASE_API_URI + 'applications/me',
        method: 'PUT'
      }
    });

    return service;
  }
})();
