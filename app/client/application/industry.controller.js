/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .controller('IndustryController', IndustryController);

  /* @ngInject */
  function IndustryController($scope, Auth, industryService, $q, _, $state) {
    var vm = this;

    vm.title = 'IndustryController';
    vm.industries = [];
    vm.myIndustries = [];
    vm.industryTooltip = 'Select one primary industry. If you are approved to join the platform, ';
    vm.industryTooltip += 'you can select up to 3 industries to search for consulting opportunities. ';
    vm.industryTooltip += 'The more narrow your focus, the easier it will be for companies to find you.';
    vm.saveIndustryChanges = saveIndustryChanges;
    var hasIndustry = false;
    activate();
    var currentIndustries = [];
    var profileId;

    ///////////////////

    function activate() {

      if ($state.$current.url.source !== '/client/new/rfp') {
        Auth.profileDone.then(function setData() {
          vm.user = Auth.currentUser();
          vm.profile = Auth.currentProfile();
          profileId = vm.profile.id;
          // setting user's industry
          industryService.get({id: profileId}).$promise
            .then(function (res) {
              vm.industry = _.pluck(res, 'Industry');

              if (!_.isEmpty(res)) {
                currentIndustries = _.pluck(res, 'Industry');
                vm.industryApplication = _.clone(_.find(vm.industries, _.last(currentIndustries)));
              }

            });
        });
      }

      // setting industries to dropDown field
      industryService.getAll({}).$promise.then(function (res) {
        vm.industries = res;
      });
    }

    function saveApplicationIndustryChanges() {

      vm.profile = Auth.currentProfile();
      profileId = vm.profile.id;
      vm.industry = vm.industryApplication;
      if (!!vm.industry && !_.find(currentIndustries, {id: vm.industry.id})) {
        return industryService.post({id: profileId, IndustryId: vm.industry.id})
          .$promise
          .then(function setData(service) {
            currentIndustries.push(vm.industry);
            if (currentIndustries.length > 1) {
              var oldIndustry = _.first(currentIndustries);
              return industryService.removeIndustry({id: vm.profile.id, industry: oldIndustry.id})
              .$promise
              .then(function removeIndustry() {
                _.remove(currentIndustries, oldIndustry);
              });
            } else {
              return currentIndustries;
            }
          });
      } else {
        return $q.resolve(true);
      }
    }

    function saveIndustryChanges() {

      var newUserIndustries = [];
      var statusResponse = true;
      var newIndustriesId = _.difference(
        _.pluck(vm.industry, 'id'),
        _.pluck(currentIndustries, 'id')
      );
      var currentsId = _.pluck(currentIndustries, 'id');
      var allIds = _.union(newIndustriesId, currentsId);
      var oldIndustriesId = _.difference(allIds, _.pluck(vm.industry, 'id'));
      newUserIndustries = _.map(newIndustriesId, function(id) {
        return {id: id};
      });
      var promises = [];
      if (newIndustriesId.length > 0) {
        // Saving user industries
        _.forEach(vm.industry, function(obj) {
          if (_.contains(newIndustriesId, obj.id)) {
            promises.push(
              industryService.post({id: profileId}, {IndustryId: obj.id}).$promise
            );
          }
        });
      }

      if (oldIndustriesId.length > 0) {
        _.forEach(oldIndustriesId, function (id) {
          promises.push(
            industryService.removeIndustry({id: profileId, industry: id}).$promise
          );
        });
      }

      if (promises.length) {
        return $q.all(promises);
      } else {
        return $q.resolve();
      }
    }

    function syncIndustry(event, token) {

      if (!!event.targetScope.siteCtrl) {
        saveApplicationIndustryChanges()
          .then(function notifySuccess() {
            $scope.$emit('IndustrySync', token, true);
          });
      } else {
        saveIndustryChanges()
          .then(function notifySuccess() {
            $scope.$emit('IndustrySync', token, true);
          });
      }

    }

    $scope.$on('SyncProfile', syncIndustry);
  }
})();
