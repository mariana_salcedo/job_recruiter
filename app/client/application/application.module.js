(function iife() {
  'use strict';

  angular
    .module('avisare.application', [
      'avisare.core',
      'avisare.auth',
      'avisare.address',
      'avisare.referral'
    ]);
})();
