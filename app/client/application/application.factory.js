(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .factory('ApplicationFactory', ApplicationFactory);

  /* @ngInject */
  function ApplicationFactory($q, Auth, ApplicationService) {

    var application;
    var service = {
      setApplication: setApplication,
      applicationDone: $q.reject('Application Unavailable'),
      currentApplication: currentApplication
    };

    activate();

    return service;

    ////////////////

    function activate() {
      service.applicationDone = Auth.profileDone
      .then(function setProfile() {
        return ApplicationService.myApplication().$promise
        .then(function setApplicationData(data) {
          service.setApplication(data);
        });
      });
    }

    function currentApplication() {
      return application;
    }

    function setApplication(newApplication) {
      application = newApplication;
    }
  }
})();
