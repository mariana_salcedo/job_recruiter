/**
 * avisare
 * Created by Andres Juarez on 24/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .factory('CaseStudiesService', CaseStudiesService);

  /* @ngInject */
  function CaseStudiesService($resource, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'caseStudies/:id', {id: '@id'},
      {
        get:{
          method: 'GET',
          isArray: true
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        },
        getMine:{
          method: 'GET',
          url: BASE_API_URI + 'profiles/:profileId/caseStudies',
          params: {profileId: '@profileId'},
          isArray: true
        },
        postMine:{
          method: 'POST',
          url: BASE_API_URI + 'profiles/:profileId/caseStudies',
          params: {profileId: '@profileId'}
        }
      });

    return service;
  }
})();
