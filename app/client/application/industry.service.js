/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular
    .module('avisare.application')
    .factory('industryService', industryService);
  /* @ngInject */

  function industryService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'industries/', {},
      {
        getAll:{
          method: 'GET',
          isArray: true
        },
        getById:{
          method: 'GET',
          url: BASE_API_URI + 'industries/:id',
          params: {id: '@id'}
        },
        get:{
          url: BASE_API_URI + 'profiles/:id/industries',
          params: {id: '@id'},
          method: 'GET',
          isArray: true
        },
        delete: {
          url: BASE_API_URI + 'industries/:id',
          params: {id: '@id'},
          method: 'DELETE'
        },
        removeIndustry: {
          url: BASE_API_URI + 'profiles/:id/industries',
          params: {id: '@id'},
          method: 'DELETE'
        },
        post:{
          url: BASE_API_URI + 'profiles/:id/industries',
          params: {id: '@id'},
          method: 'POST'
        },
        update:{
          url: BASE_API_URI + 'profiles/:id/industries',
          params: {id: '@id'},
          method: 'PUT'
        },
        getFunctions:{
          url: BASE_API_URI + 'profiles/:id/industryFunctions',
          method: 'GET',
          isArray: true
        }
      });
    return service;
  }

})();
