(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .controller('ApplicationController', ApplicationController);

  /* @ngInject */
  function ApplicationController(
  $q,
  $scope,
  $window,
  Auth,
  AuthToken,
  ConfirmFactory,
  ApplicationFactory,
  ApplicationService,
  _) {
    var vm = this;
    vm.title = 'ApplicationController';

    vm.application = {};
    vm.applicationData = {};
    vm.setting = {};
    vm.saveApplication = saveApplication;
    vm.saveMyApplicationData = saveMyApplicationData;
    vm.primaryFunctionTooltip = 'Select one primary function. If you are approved to join the ';
    vm.primaryFunctionTooltip += 'platform, you can select up to 2 functions to search for ';
    vm.primaryFunctionTooltip += 'consulting opportunities. Choosing the function that best ';
    vm.primaryFunctionTooltip += 'fits your level and skillset is the fastest way to get ';
    vm.primaryFunctionTooltip += 'found for opportunities.';
    vm.maxSizeFile = 'Maximum file size is 5 Mb';
    activate();

    ////////////////

    function activate() {
      ApplicationFactory.setApplication(vm.applicationData);
      Auth.profileDone.then(function setData() {
        vm.application = pickUser(Auth.currentUser());
        vm.application.profile = pickProfile(Auth.currentProfile());

        return $q.all([
          ApplicationService.myApplication().$promise
          .then(function setApplication(data) {

            if (data.status === 'New') {
              ConfirmFactory.alert(takeMeHome, {
                header: 'Thank you',
                acceptText: 'Accept',
                body: 'Your application has been successfully submitted'
              });

              AuthToken.setToken();
              return $q.reject('Allready submited.');
            }
            vm.applicationData = data || {};

            ApplicationFactory.setApplication(vm.applicationData);
          }),

          ApplicationService.getSetting({
            profileId:vm.application.profile.id
          }).$promise
          .then(function setApplication(data) {
            if (!!data) {
              vm.setting = data;
            }
          })
        ]);
      });
    }

    function takeMeHome() {
      $window.location.href = 'http://' + $window.location.host;
    }

    function completeApplication() {
      return ApplicationService.completeApplication({}, {}).$promise;
    }

    function pickUser(user) {
      return _.pick(user, 'id', 'email', 'lastName', 'firstName');
    }

    function pickProfile(profile) {
      return _.pick(profile, 'id', 'skype', 'resume', 'primaryFunction', 'linkedin');
    }

    function saveApplication() {
      var application = pickUser(vm.application);
      var profile = pickProfile(vm.application.profile);

      application.profile = _.pick(profile, _.identity);

      var resume = application.profile.resume;
      if (!!resume && _.isObject(resume)) {
        application.profile.resume = resume.s3Url;
      }

      if (!!vm.application.id) {
        return ApplicationService.update(application).$promise;
      } else {
        return ApplicationService.post(application).$promise
        .then(function setData(user) {

          vm.application.id = user.id;
          vm.application.profile.id = user.profile.id;
          var profile = user.profile;
          var token = user.token;

          Auth.updateUser(user);
          Auth.updateProfile(profile);

          // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
          AuthToken.setToken(token.access_token);

          delete user.roles;
          delete user.token;
          delete user.profile;
        });
      }
    }

    function saveMyApplicationData() {
      var applicationData = vm.applicationData;

      var sample = applicationData.writingSample;

      if (!!sample && _.isObject(sample)) {
        applicationData.writingSample = sample.s3Url;
      }
      if (!!applicationData.id) {
        return ApplicationService.updateApplication({}, _.pick(applicationData, _.identity))
        .$promise;
      } else {
        return ApplicationService.addApplication({}, applicationData).$promise
        .then(function setData(application) {
          _.assign(vm.applicationData, application);
          ApplicationFactory.setApplication(vm.applicationData);
        });
      }
    }

    function saveSettings(paramsSave) {

      var setting = vm.setting;
      var profile = {
        profileId: Auth.currentProfile().id
      };
      if (!!setting.aboutMe) {
        if (!!setting.id) {
          return ApplicationService.updateSetting(profile, _.pick(setting, _.identity))
          .$promise;
        } else {
          return ApplicationService.postSetting(profile, setting).$promise
          .then(function setData(setting) {
            _.assign(vm.setting, setting);
          });
        }
      } else {
        $q.resolve(true);
      }
    }

    function syncMyApplication(event, token) {
      return saveApplication()
      .then(function saveAboutMe(paramsSave) {
        $scope.$broadcast('FunctionSetSync', token, true);
        return saveSettings(paramsSave);
      })
      .then(function saveData() {
        return saveMyApplicationData();
      }).then(function notifySuccess() {
        $scope.$emit('ApplicationSync', token, true);
      }).catch(function notifyError() {
        $scope.$emit('ApplicationSync', token, false);
      });
    }

    function endApplication() {
      completeApplication()
      .then(function sendSuccess() {
        $scope.$emit('ApplicationCompleted', true);
      })
      .catch(function sendSuccess() {
        $scope.$emit('ApplicationCompleted', false);
      });
    }

    $scope.$on('CompleteApplication', endApplication);

    $scope.$on('SyncApplication', syncMyApplication);
  }
})();
