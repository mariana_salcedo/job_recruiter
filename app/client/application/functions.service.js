/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular
    .module('avisare.application')
    .factory('functionsService', functionsService);
  /* @ngInject */

  function functionsService($resource, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'industryFunctions', {},
      {
        get:{
          method: 'GET',
          isArray: true
        },
        getMe:{
          url: BASE_API_URI + 'profiles/:profileId/industryFunctions',
          params: {profileId: '@profileId'},
          method: 'GET',
          isArray: true
        },
        post:{
          url: BASE_API_URI + 'profiles/:profileId/industryFunctions',
          params: {profileId: '@profileId'},
          method: 'POST'
        },
        delete:{
          url: BASE_API_URI + 'profiles/:profileId/industryFunctions',
          params: {profileId: '@profileId'},
          method: 'DELETE'
        }
      });
    return service;
  }

})();
