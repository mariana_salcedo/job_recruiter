/**
 * avisare
 * Created by Andres Juarez on 24/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .controller('caseStudiesController', CaseStudiesController);

  /* @ngInject */
  function CaseStudiesController(
  $scope,
  $q,
  toaster,
  Auth,
  industryService,
  _,
  CaseStudiesService,
  $uibModal) {
    var vm = this;
    vm.title = 'caseStudiesController';
    var modalInstance;

    vm.caseStudy = {};
    vm.caseStudies = [];
    vm.industries = [];

    // functions:
    vm.openModal = openModal;
    vm.closeModal = closeModal;
    vm.saveCaseStudy = saveCaseStudy;
    vm.syncCaseStudy = syncCaseStudy;
    vm.deleteCaseStudy = deleteCaseStudy;
    vm.caseStudyExample = caseStudyExample;

    activate();

    //////////////////

    function activate() {

      industryService.getAll().$promise
        .then(function setIndustries(industries) {
          vm.industries = industries;
          if (vm.application && vm.caseStudies.length === 0) {
            vm.caseStudies = [{}, {}];
          }
        });
      Auth.profileDone.then(function setData() {
        vm.profile = _.clone(Auth.currentProfile());
        CaseStudiesService.getMine({profileId: vm.profile.id}).$promise
          .then(function(res) {
            vm.caseStudies = _.pluck(res, 'CaseStudy');
            _.each(vm.caseStudies, function methodName(caseStudy) {
              industryService.getById({id: caseStudy.IndustryId}).$promise
              .then(function setIndustry(industry) {
                caseStudy.industrySelected = _.find(vm.industries, {id: industry.id});
              });
            });
            if (vm.application && vm.caseStudies.length < 2) {
              for (var i = vm.caseStudies.length; i < 2; i++) {
                vm.caseStudies.push({});
              }
            }
          });
      });
    }

    function openModal(caseStudy) {
      vm.caseStudy = caseStudy || {};

      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_case_studies_form.html',
        animation: true,
        scope: $scope,
        size: 'lg'
      });
    }

    function caseStudyExample() {
      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_case_study_example.html',
        animation: true
      });
    }

    function closeModal() {

      if (!!modalInstance && modalInstance.close) {

        modalInstance.close();
      }
    }

    function saveCaseStudy(caseStudyData) {

      vm.profile = Auth.currentProfile();

      var caseStudy = caseStudyData || vm.caseStudy;
      caseStudy.IndustryId = caseStudy.industrySelected.id;
      if (!!caseStudy.id) {
        return CaseStudiesService.update({id: caseStudy.id}, caseStudy).$promise;
      } else {
        return CaseStudiesService.postMine({profileId: vm.profile.id}, caseStudy).$promise
        .then(function setData(newCaseStudy) {
          _.assign(caseStudy, newCaseStudy);
          if (!_.find(vm.caseStudies, caseStudy)) {
            vm.caseStudies.push(caseStudy);
          }
        });
      }
    }

    function syncCaseStudy() {
      saveCaseStudy().then(function close() {
        closeModal();
      });
    }

    function saveCaseStudies() {
      var promises = _.map(vm.caseStudies, function save(caseStudy) {

        var size = _.size(_.pick(caseStudy,
        'id',
        'industrySelected',
        'caseStudyTitle',
        'caseStudyProblem',
        'caseStudySolution',
        'caseStudyResult'));
        if (size < 5) {
          return $q.resolve(false);
        }
        return saveCaseStudy(caseStudy)
        .catch(function returnNotification() {

          return caseStudy;
        });
      });

      return $q.all(promises);
    }

    function syncCaseStudies(e, token) {
      saveCaseStudies()
      .then(function notifySuccess(caseStudies) {
        var caseStudiesSaved = true;
        _.forEach(caseStudies, function reviewCaseStudies(caseStudy) {

          if (caseStudy === false) {
            caseStudiesSaved = false;
            toaster.warning({
              body: 'Your progress has been saved, but you need to complete all the case studies ' +
              'before your application can proceed'});
          }
          return caseStudy;
        });
        $scope.$emit('CaseStudiesSync', token, caseStudiesSaved);
      })
      .catch(function notifyError() {
        $scope.$emit('CaseStudiesSync', token, false);
      });
    }

    $scope.$on('SyncProfile', syncCaseStudies);

    function deleteCaseStudy(caseStudy) {
      CaseStudiesService.delete({id: caseStudy.id}).$promise
        .then(function(res) {
          _.remove(vm.caseStudies, caseStudy);
        });
    }
  }
})();
