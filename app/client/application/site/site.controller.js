(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .controller('SiteController', SiteController);

  /* @ngInject */
  function SiteController(
    $state,
    $rootScope,
    $scope,
    toaster,
    AuthToken,
    _,
    ConfirmFactory,
    $window,
    $location,
    anchorSmoothScroll
  ) {
    var vm = this;
    var missingFields = 'Please fill out required field(s).';
    var validTabs = [
      'experience',
      'reference',
      'basic'
    ];

    var submit = false;

    var listen = [
      'ReferencesSync',
      'CaseStudiesSync',
      'IndustrySync',
      'FunctionSync',
      'AddressSync'
    ];
    vm.sending = false;
    vm.title = 'SiteController';
    vm.stepOne = stepOne;
    vm.stepTwo = stepTwo;
    vm.syncApplication = syncApplication;
    vm.submitApplication = submitApplication;

    activate();

    $rootScope.$on('$stateChangeSuccess', onStateChange);

    ////////////////
    function validateForm(form) {
      if (form.$invalid) {
        _.forEach(form.$error, function (currrentObj) {
          _.forEach(currrentObj, function (obj) {
            if (!!obj) {
              obj.$setTouched(true);
              obj.$setDirty(true);
              obj.$validate();
            }
          });
        });
      }
    }
    function onStateChange(event, toState, toParams, fromState, fromParams) {

      setTab();
      if (!_.include(validTabs, $state.params.tab)) {
        $state.transitionTo('personal', {tab: 'basic'});
      }
    }

    function listenToken($scope, listen) {
      var response = {
        token: {}
      };

      _.forEach(listen, function(e) {
        $scope.$on(e, handelResponse);
      });

      function handelResponse(event, token, success) {

        var currentToken = response.token;

        if (token === currentToken) {
          currentToken[event.name] = success;
          checkTokenCompletion(listen, currentToken);
        }
      }

      return response;
    }

    function checkTokenCompletion(listen, currentToken) {

      if (_.size(currentToken) === listen.length) {
        vm.sending = false;
        var canSubmit = true;

        if (submit === true) {
          _.forEach(currentToken, function(current) {
            if (!current) {
              canSubmit = false;
            }
            return canSubmit;
          });
          if (canSubmit) {
            ConfirmFactory.confirm(completeApplication, {
              header: 'Your application has been saved',
              acceptText: 'Submit My Application',
              declineText: 'Keep Editing',
              body: 'This information will be submitted to Avisare and you will ' +
              ' not have a chance to edit it. Are you sure you want to submit?'
            });
          }
        } else {
          ConfirmFactory.confirm(takeMeHome, {
            header: 'Application saved successfully',
            acceptText: 'Take me Home',
            declineText: 'Keep Editing',
            body: 'Your application has been saved successfully.'+
            ' You can continue now or come back later to finish filling out the form.'
          });
        }

        submit = false;
      }
    }

    function completeApplication() {
      $scope.$broadcast('CompleteApplication');
    }

    function takeMeHome() {
      $window.location.href = 'http://' + $window.location.host;
    }

    function stepOne(form) {

      if (form.$invalid) {
        validateForm(form);
        var elementError;
        var errors = _.toArray(form.$error);
        if (errors.length) {
          elementError = errors[0][0].$name;
          // set the location.hash to the id of
          // the element you wish to scroll to
          anchorSmoothScroll.scrollTo(elementError);
        }
        toaster.error({body: missingFields});
      } else {

        $state.transitionTo('personal', {tab: 'experience'});
      }
    }

    function stepTwo(form) {

      if (!!form && form.$invalid) {
        validateForm(form);
        var elementError;
        var errors = _.toArray(form.$error);
        if (errors.length) {
          elementError = errors[0][0].$name;
          // set the location.hash to the id of
          // the element you wish to scroll to
          anchorSmoothScroll.scrollTo(elementError);
        }
        toaster.error({body: missingFields});
      } else {

        $state.transitionTo('personal', {tab: 'reference'});
      }
    }

    function activate() {
      setTab();
    }

    function submitApplication() {
      if (vm.basicForm.$valid &&
      vm.experienceForm.$valid &&
      vm.caseForm.$valid &&
      vm.ReferenceForm.$valid) {
        submit = true;
        syncApplication(vm.experienceForm);
      } else {
        //form.$setSubmitted(true);
        var elementError;
        var errors;
        if (vm.basicForm.$invalid) {
          $state.transitionTo('personal', {tab: 'basic'});
          validateForm(vm.basicForm);
          errors = _.toArray(vm.basicForm.$error);
          if (errors.length) {
            elementError = errors[0][0].$name;
            anchorSmoothScroll.scrollTo(elementError);
          }
        }
        if (vm.experienceForm.$invalid) {
          $state.transitionTo('personal', {tab: 'experience'});
          validateForm(vm.experienceForm);
          errors = _.toArray(vm.experienceForm.$error);
          if (errors.length) {
            elementError = errors[0][0].$name;
            anchorSmoothScroll.scrollTo(elementError);
          }
        }
        if (vm.caseForm.$invalid) {
          $state.transitionTo('personal', {tab: 'experience'});
          validateForm(vm.caseForm);
          errors = _.toArray(vm.caseForm.$error);
          if (errors.length) {
            elementError = errors[0][0].$name;
            anchorSmoothScroll.scrollTo(elementError);
          }
        }
        validateForm(vm.ReferenceForm);
        errors = _.toArray(vm.ReferenceForm.$error);
        if (errors.length) {
          elementError = errors[0][0].$name;
          // set the location.hash to the id of
          // the element you wish to scroll to.
          // $location.hash(elementError);

          // call $anchorScroll()
          // $anchorScroll();
          anchorSmoothScroll.scrollTo(elementError);
        }

        toaster.error({body: missingFields});
      }
    }

    function syncApplication(form) {
      if (form.$invalid) {

        var elementError;
        var messageDiv = $('<div>').addClass('error-messages');
        var message = $('<div>');
        var error = '';
        validateForm(form);

        var errors = _.toArray(form.$error);
        elementError = errors[0][0].$name;

        // set the location.hash to the id of
        // the element you wish to scroll to.
        anchorSmoothScroll.scrollTo(elementError);
        toaster.error({body: missingFields});

      } else {
        vm.sending = true;
        var token = {};
        $scope.$broadcast('SyncApplication', token);
      }
    }

    function applicationCompleted (e, success) {

      if (success) {
        AuthToken.setToken();
        takeMeHome();
      } else {
        toaster.error({
          body: 'And error has occurred: your application was saved but not submitted.'
        });
      }
    }

    // TODO move to service
    var applicationToken = listenToken($scope, listen);

    function syncOthers(event, token, success) {
      applicationToken.token = {};

      if (success) {
        $scope.$broadcast('SyncProfile', applicationToken.token);
      } else {
        vm.sending = false;
      }
    }

    $scope.$on('ApplicationCompleted', applicationCompleted);

    $scope.$on('ApplicationSync', syncOthers);

    function setTab() {
      vm.currentTab = $state.params.tab;
    }
  }
})();
