(function() {
  'use strict';

  angular
      .module('avisare.application')
      .constant('TOKEN_NAME','application_token');
})();
