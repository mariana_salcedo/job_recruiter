(function() {
  'use strict';

  angular
    .module('avisare.application')
    .config(uiRouterConfig)
    .run(uiRouterRun);

  /* @ngInject */
  function uiRouterConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/application/basic');
    $httpProvider.interceptors.push('AuthInterceptor');

    $stateProvider
    .state('personal', {
      url: '/application/:tab'
    });
  }

  /* @ngInject */
  function uiRouterRun(Auth) {

    Auth.init();
  }

})();
