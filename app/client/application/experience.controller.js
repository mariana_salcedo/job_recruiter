/**
 * avisare
 * Created by Andres Juarez on 19/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.application')
    .controller('ExperienceController', ExperienceController)
    .filter("PresentDateFilter", function($filter) {
    return function(value) {
      if(value) {
          return $filter("date")(value);
      }else{
        return "Present";  
      }

      return value;
    }  
});

  /* @ngInject */
  function ExperienceController($q, $scope, $filter, Auth, experienceService, $uibModal, _, toaster) {

    var modalInstance;

    var dateFilter = $filter('date');

    var vm = this;
    vm.title = 'ExperienceController';
    vm.profile = null;
    vm.experience = {};
    vm.experiences = [];
    vm.openModal = openModal;
    vm.closeModal = closeModal;
    vm.deleteExperience = deleteExperience;
    vm.toggleShowExperienceEndDate = toggleShowExperienceEndDate;
    vm.saveExperience = saveExperience;
    var alreadyChecked = false;
    vm.experienceTooltip = 'You can select maximum three experiences to show on your website';
    vm.dateStartTooltip = 'Only the year will be displayed on your website';
    vm.dateEndTooltip = 'Only the year will be displayed on your website';
    activate();

    /////////////////////

    function openModal(experience) {
      if (!!experience) { 
        experience.experienceStart = new Date(experience.experienceStart);
        if(!!experience.experienceEnd){
          experience.experienceEnd = new Date(experience.experienceEnd);
        }else{
          experience.endExperienceShow = true;
        }
      } else {
        experience = {};
        experience.experienceShow = true;
        experience.experienceEnd = '';
      }
      vm.experience = experience || {};

      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_experience_form.html',
        animation: true,
        scope: $scope
      });
    }
    function toggleShowExperienceEndDate($event){
        var checkbox = $event.target;

        if (checkbox.checked){
          vm.experience.experienceEnd = null;
        }

    }

    function closeModal() {

      if (!!modalInstance && modalInstance.close) {

        modalInstance.close();
      }
    }

    function activate() {

      Auth.profileDone.then(function getData() {
        vm.user = Auth.currentUser();
        vm.profile = Auth.currentProfile();

        experienceService.getMeExperience({idProfile: vm.profile.id}).$promise
          .then(function(res) {
            vm.experiences = res;
          });
      });
    }
    function canBeSaved(experience) {
      // In this function we verify if the experience can be saved
      // we verify if already exist 3 experiences with show set in true
      if (experience.experienceShow) {
        var counter = 0;

        var cpyExperience = _.clone(vm.experiences);
        var clearExperience = [];
        _.forEach(cpyExperience, function(curExp) {
          if (curExp.id !== experience.id) {
            clearExperience.push(curExp);
          }
        });
        var showArray = _.pluck(clearExperience, 'experienceShow');
        _.forEach(showArray, function(value) {
          if (value) {
            counter++;
          }
        });
        if (counter > 2) {
          toaster.error({body: 'You can only have 3 experiences checked to show on your website'});
          return false;
        } else {
          return true;
        }
      } else {
        // The current experience isn't set experienceShow
        return true;
      }
    }

    function validateForm(form) {
      if (form.$invalid) {
        _.forEach(form.$error, function (currrentObj) {
          _.forEach(currrentObj, function (obj) {
            if (!!obj) {
              obj.$setTouched(true);
              obj.$setDirty(true);
              obj.$validate();
            }
          });
        });
      }
    }

    function saveExperience(form) {

      if (!!form && form.$invalid) {
        validateForm(form);
        return $q.reject('Invalid Form');
      }

      var experience = _.clone(vm.experience);
      experience.experienceStart = dateFilter(experience.experienceStart, 'yyyy-MM-dd');
      if(experience.experienceEnd){
        experience.experienceEnd = dateFilter(experience.experienceEnd, 'yyyy-MM-dd');
      }else{
        experience.experienceEnd = null;
      }

      if (!experience.id) {
        if (canBeSaved(experience)) {
          return experienceService.post({
            idProfile: vm.profile.id
          }, _.omit(experience, _.isNull)).$promise
          .then(function setExperience(newExperience) {
            _.assign(experience, newExperience);
            vm.experiences.push(newExperience);
            vm.experience = {};
            toaster.success({body: 'Experience saved successfully!'});
            closeModal();
          });
        } else {
          return false;
        }
      } else {
        if (canBeSaved(experience)) {
          return experienceService.update({
            idExperience: experience.id
          }, processExperienceDate(experience)).$promise
          .then(function experienceUpdated() {
            toaster.success({body: 'Experience saved successfully!'});
            closeModal();
          });
        } else {
          return false;
        }

      }
    }
    function processExperienceDate(experienceChange){
        var experienceData = _.clone(experienceChange);
        experienceData = _.omit(experienceData, _.isNull);
        return experienceData;

    }

    function deleteExperience(experienceId) {
      experienceService.delete({idExperience: experienceId}).$promise
        .then(function(res) {
          experienceService.getMeExperience({idProfile: vm.profile.id}).$promise
            .then(function(res) {
              vm.experiences = res;
            });
        });
    }
  }
})();
