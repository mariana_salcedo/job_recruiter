(function() {
  'use strict';

  angular
    .module('avisare.industry', [
      'avisare.core',
      'ui.grid',
      'ui.grid.edit',
      'ui.grid.expandable',
      'ui.grid.pagination',
      'ui.grid.resizeColumns'
    ]);
})();
