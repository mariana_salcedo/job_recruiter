(function () {
  'use strict';

  angular
      .module('avisare.industry')
      .factory('industriesService', industriesService);

  /* @ngInject */
  function industriesService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'industries/:id', {id: '@id'},
      {
        get:{
          method: 'GET'
        },
        post:{
          method: 'POST'
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        }
      });
    return service;
  }
})();
