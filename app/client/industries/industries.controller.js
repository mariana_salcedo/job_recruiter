/**
 * avisare
 * Created by Ricardo Colmenarez on 11/11/15.
 * email rcolmenarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular.module('avisare.industry')
    .controller('IndustriesController', IndustriesController)
    .controller('IndustriesModalController', IndustriesModalController)
    .controller('EditIndustriesModalController', EditIndustriesModalController);

  /* @ngInject */

  function IndustriesController(
    $uibModal,
    toaster,
    industriesService,
    _,
    $animate) {
    var vm = this;

    vm.getIndustries = getIndustries;
    vm.showForm = showForm;
    vm.editIndustries = editIndustries;
    vm.deleteIndustries = deleteIndustries;

    vm.tabs = [
      {
        title: 'Basic Info',
        content: '/public/templates/_industries_info.html'
      }
    ];

    vm.paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null
    };

    vm.gridOptions = {
      columnDefs: [
        {field: 'id', displayName: 'ID', cellEditableCondition:false},
        {field: 'industryName', displayName: 'Name'},
        {field: 'industryStatus', displayName: 'Status', cellTemplate:'<div class="ui-grid-cell-contents">{{COL_FIELD ? \'Enabled\' : \'Disabled\'}}</div>'},
        {field: 'createdAt', displayName: 'Created At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        {field: 'updatedAt', displayName: 'Updated At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        {field: 'deletedAt', displayName: 'Deleted At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        {name: 'Action', cellEditableCondition:false, cellTemplate: '<div class="ui-grid-cell-contents"><button class="btn btn-danger btn-xs btn-block" ng-click="grid.appScope.industry.deleteIndustries(row.entity.id)"><span class="glyphicon glyphicon-trash"></span></button></div>'}
      ],
      enableRowSelection: true,
      expandableRowHeight: 200,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;
        $animate.enabled(vm.gridApi.grid.element, false);
        vm.gridApi.edit.on.beginCellEdit(null, function(rowEntity) {
          editIndustries(rowEntity);
        });
        vm.gridApi.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.paginationObj.offset = (newPage - 1) * pageSize;
          vm.paginationObj.limit = pageSize;
          vm.getIndustries();
        });
        vm.gridApi.core.on.sortChanged(null, function(grid, sortColumns) {
          if (sortColumns.length === 0) {
            vm.paginationObj.order = null;
            vm.paginationObj.orderBy = null;
          } else {
            vm.paginationObj.order = sortColumns[0].sort.direction;
            vm.paginationObj.orderBy = sortColumns[0].name;
          }
          getIndustries();
        });
      }
    };
    getIndustries();

    function getIndustries() {
      industriesService.get(vm.paginationObj).$promise.then(function (res) {
        vm.industries = res.rows;
        vm.gridOptions.totalItems = res.count;
        vm.gridOptions.data = vm.industries;
      });
    }

    // Formulario Modal
    function showForm(size) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_industries_form.html',
        controller: 'IndustriesModalController',
        controllerAs: 'industry',
        animation: true,
      });
      modalInstance.result.then(function (res) {
        vm.industries.push(res);
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function editIndustries(industryObject) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_industries_form.html',
        controller: 'EditIndustriesModalController',
        controllerAs: 'industry',
        animation: true,
        resolve: {
          industry: function () {
            return industryObject;
          }
        }
      });
      modalInstance.result.then(function (res) {
        getIndustries();
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function deleteIndustries(id) {
      industriesService.delete({id:id}).$promise.then(function(res){
        getIndustries();
      });
    }
  }

  function IndustriesModalController($uibModalInstance, industriesService) {
    var vm = this;
    vm.addNewIndustry = addNewIndustry;
    vm.industry = {};
    vm.industry.industryStatus = false;
    function addNewIndustry(industry) {
      var newIndustry = {
        industryName: industry.industryName,
        industryStatus: industry.industryStatus
      };
      industriesService.post(newIndustry).$promise
        .then(function(res) {
          $uibModalInstance.close(res);
        });
    }
  }

  function EditIndustriesModalController($uibModalInstance, industry, industriesService) {
    var vm = this;
    vm.addNewIndustry = addNewIndustry;
    vm.industry = {};
    vm.industry.id = industry.id;
    vm.industry.industryStatus = industry.industryStatus;
    vm.industry.industryName = industry.industryName;

    function addNewIndustry(industry) {
      var newIndustry = {
        id: industry.id,
        industryName: industry.industryName,
        industryStatus: industry.industryStatus
      };

      industriesService.update(newIndustry).$promise.then(function() {
        $uibModalInstance.close(vm.experience);
      });

    }
  }

})();