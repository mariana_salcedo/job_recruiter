(function() {
  'use strict';

  angular
    .module('avisare.reset')
    .constant('TOKEN_NAME','access_token');
})();
