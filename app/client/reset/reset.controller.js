(function() {
  'use strict';

  angular
    .module('avisare.reset')
    .controller('ResetController', ResetController);

  function ResetController(resetService, $location, toaster,$window,Auth) {
    var vm = this;
    vm.routeMessages = '/public/templates/_login_error_messages.html';
    vm.resetData = {};

    vm.reset = function() {
      if (vm.resetData.password === vm.newPassword) {
        resetService.update(vm.resetData).$promise
          .then(function(data) {
            if (data) {
              toaster.success({body: 'Reset Successful!'});
              $location.url('/');
              $window.location.reload();
            }
          })
          .catch(function() {
            toaster.error({body: 'An error has occurred!'});
            $location.path('/reset');
            Auth.init();
          });
      }

    };

  }
})();
