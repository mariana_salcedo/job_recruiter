(function() {
  'use strict';

  angular
    .module('avisare.reset')
    .service('resetService', resetService);

  /* @ngInject */
  function resetService($resource, BASE_API_URI) {

    var service = $resource(BASE_API_URI + 'users/reset', {},
    {
      update:{
        method: 'PUT'
      }
    });

    return service;
  }
})();
