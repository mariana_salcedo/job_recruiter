(function() {
  'use strict';

  angular
      .module('avisare.reset', [
          'avisare.core',
          'avisare.auth'
      ])
    .config(configHttpReset)
    .run(uiRouterRun);

  /* @ngInject */
  function configHttpReset($locationProvider,$httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
    $locationProvider.html5Mode(true);
  }

  /* @ngInject */
  function uiRouterRun(Auth) {
    Auth.init();
  }

})();
 