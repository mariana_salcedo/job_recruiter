/**
 * avisare
 * Created by Ricardo Colmenarez on 11/11/15.
 * email rcolmenarez@teravisiontech.com
 */
(function () {
  'use strict';

  angular.module('avisare.prospect')
      .controller('ProspectsController', ProspectsController)
      .controller('ProspectsModalController', ProspectsModalController)
      .controller('EditProspectsModalController', EditProspectsModalController);

  /* @ngInject */

  function ProspectsController(
    $uibModal,
    toaster,
    prospectService,
    _,
    $animate) {
    var vm = this;

    vm.getProspects = getProspects;
    vm.showForm = showForm;
    vm.editProspects = editProspects;
    vm.deleteProspects = deleteProspects;
    vm.approveProspect = approveProspect;
    vm.rejectProspect = rejectProspect;

    vm.tabs = [
      {
        title: 'Basic Info',
        content: '/public/templates/_prospects_info.html'
      }
    ];

    vm.paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null
    };

    var actions = '<div class="ui-grid-cell-contents">' +
        '<div class="btn-group" uib-dropdown dropdown-append-to-body>' +
          '<button type="button" class="btn btn-xs uib-dropdown-toggle" uib-dropdown-toggle>' +
            'Select an action <span class="caret"></span>' +
          '</button>' +
          '<ul class="uib-dropdown-menu" role="menu">' +
            '<li><a href="#" ng-click="grid.appScope.prospect.approveProspect(row.entity.id)">Approve</a></li>' +
            '<li><a href="#" ng-click="grid.appScope.prospect.rejectProspect(row.entity.id)">Reject</a></li>' +
            '<li><a href="#" ng-click="grid.appScope.prospect.deleteProspects(row.entity.id)">Delete</a></li>' +
          '</ul>' +
        '</div>' +
      '</div>';

    vm.gridOptions = {
      columnDefs: [
        {field: 'id', displayName: 'ID', cellEditableCondition:false},
        {field: 'firstName', displayName: 'First name'},
        {field: 'lastName', displayName: 'Last name'},
        {field: 'companyName', displayName: 'Company name'},
        {field: 'email', displayName: 'Email'},
        {field: 'phoneNumber', displayName: 'Phone number'},
        {field: 'message', displayName: 'Message'},
        {field: 'createdAt', displayName: 'Created At', cellFilter: 'date:\'MM/dd/yyyy\'', cellEditableCondition:false },
        //{field: 'updatedAt', displayName: 'Updated At', cellFilter: 'date:\'MM/dd/yyyy hh:mm\'', cellEditableCondition:false },
        //{field: 'deletedAt', displayName: 'Deleted At', cellFilter: 'date:\'MM/dd/yyyy hh:mm\'', cellEditableCondition:false },
        //{name: 'Action', cellEditableCondition:false, cellTemplate: '<div class="ui-grid-cell-contents"><button class="btn btn-danger btn-xs btn-block" ng-click="grid.appScope.prospect.deleteProspects(row.entity.id)"><span class="glyphicon glyphicon-trash"></span></button></div>'}
        {name: 'Action', cellEditableCondition:false, cellTemplate: actions}
      ],
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi) {
        vm.gridApi = gridApi;
        $animate.enabled(vm.gridApi.grid.element, false);
        vm.gridApi.edit.on.beginCellEdit(null, function(rowEntity, colDef, newValue, oldValue) {
          editProspects(rowEntity);
        });

        vm.gridApi.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.paginationObj.offset = (newPage - 1) * pageSize;
          vm.paginationObj.limit = pageSize;
          vm.getProspects();
        });

        vm.gridApi.core.on.sortChanged(null, function(grid, sortColumns) {
          if (sortColumns.length === 0) {
            vm.paginationObj.order = null;
            vm.paginationObj.orderBy = null;
          } else {
            vm.paginationObj.order = sortColumns[0].sort.direction;
            vm.paginationObj.orderBy = sortColumns[0].name;
          }
          vm.getProspects();
        });
      }
    };

    getProspects();

    function getProspects() {
      prospectService.get(vm.paginationObj).$promise.then(function (res) {
        vm.prospect = res.rows;
        vm.gridOptions.totalItems = res.count;
        vm.gridOptions.data = vm.prospect;
      });
    }

    // Formulario Modal
    function showForm(size) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_prospects_form.html',
        controller: 'ProspectsModalController',
        controllerAs: 'prospect',
        animation: true,
      });
      modalInstance.result.then(function (res) {
        vm.prospect.push(res);
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function editProspects(prospectsObject) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_prospects_form.html',
        controller: 'EditProspectsModalController',
        controllerAs: 'prospect',
        animation: true,
        resolve: {
          prospects: function () {
            return prospectsObject;
          }
        }
      });
      modalInstance.result.then(function (res) {
        getProspects();
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function deleteProspects(id) {
      prospectService.delete({id:id}).$promise.then(function(res) {
        getProspects();
      });
    }

    function approveProspect(id) {
      prospectService.approve({id:id, response: 'Approved'}).$promise.then(function(res) {
        getProspects();
      });
    }

    function rejectProspect(id) {
      prospectService.approve({id:id, response: 'Rejected'}).$promise.then(function(res) {
        getProspects();
      });
    }
  }

  function ProspectsModalController($uibModalInstance, prospectService) {
    var vm = this;
    vm.addNewProspect = addNewProspect;
    vm.prospect = {};
    function addNewProspect(prospects) {
      var newProspect = {
        firstName: prospects.firstName,
        lastName: prospects.lastName,
        companyName: prospects.companyName,
        email: prospects.email,
        phoneNumber: prospects.phoneNumber,
        message: prospects.message
      };
      prospectService.post(newProspect).$promise
        .then(function(res) {
          $uibModalInstance.close(res);
        });
    }
  }

  function EditProspectsModalController($uibModalInstance, prospects, prospectService) {
    var vm = this;
    vm.addNewProspect = addNewProspect;
    vm.prospect = {};
    vm.prospect.id = prospects.id;
    vm.prospect.firstName = prospects.firstName;
    vm.prospect.lastName = prospects.lastName;
    vm.prospect.companyName = prospects.companyName;
    vm.prospect.email = prospects.email;
    vm.prospect.phoneNumber = prospects.phoneNumber;
    vm.prospect.message = prospects.message;
    function addNewProspect(prospects) {
      var newProspect = {
        id: prospects.id,
        firstName: prospects.firstName,
        lastName: prospects.lastName,
        companyName: prospects.companyName,
        email: prospects.email,
        phoneNumber: prospects.phoneNumber,
        message: prospects.message
      };
      prospectService.update(newProspect).$promise.then(function() {
        $uibModalInstance.close(vm.prospect);
      });
    }
  }

})();

