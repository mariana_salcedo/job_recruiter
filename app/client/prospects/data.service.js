(function () {
  'use strict';

  angular
      .module('avisare.prospect')
      .factory('prospectService', prospectService);

  /* @ngInject */
  function prospectService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'prospects/:id', {id: '@id'},
      {
        get:{
          method: 'GET'
        },
        post:{
          method: 'POST'
        },
        update:{
          method: 'PUT'
        },
        approve:{
          method: 'PUT',
          url: BASE_API_URI + 'clients/:id/response',
          params: {id: '@id'}
        },
        delete:{
          method: 'DELETE'
        }
      });
    return service;
  }
})();
