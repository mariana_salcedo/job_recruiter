(function () {
  'use strict';

  angular
      .module('avisare.address')
      .factory('countryService', countryService);

  /* @ngInject */
  function countryService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'countries/', {},
    {
      get:{
        method: 'GET',
        cache: true,
        isArray: true
      }
    });
    return service;
  }

})();
