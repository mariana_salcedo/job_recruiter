(function () {
  'use strict';

  angular
      .module('avisare.address')
      .factory('cityService', cityService);

  /* @ngInject */
  function cityService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'cities/?state=:stateCode', {stateCode: '@stateCode'},
    {
      get:{
        method: 'GET',
        isArray: true
      },
      getById:{
        url: BASE_API_URI + 'cities/:id/',
        params: {
          id: '@id',
          include: true
        },
        method: 'GET'
      }
    });
    return service;
  }

})();
