(function () {
  'use strict';

  angular
      .module('avisare.address')
      .factory('stateService', stateService);

  /* @ngInject */
  function stateService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'states/?country=:countryCode', {countryCode: '@countryCode'},
    {
      get:{
        method: 'GET',
        isArray: true
      }
    });
    return service;
  }

})();
