(function () {
  'use strict';

  angular
      .module('avisare.address')
      .factory('addressService', addressService);

  /* @ngInject */
  function addressService($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'addresses/:id', {id: '@id'},
      {
        get:{
          url: BASE_API_URI + 'users/me/addresses',
          method: 'GET',
          isArray: true
        },
        post:{
          url: BASE_API_URI + 'users/me/addresses',
          method: 'POST'
        },
        update:{
          method: 'PUT'
        },
        delete:{
          method: 'DELETE'
        }
      });
    return service;
  }

})();
