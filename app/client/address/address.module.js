(function iife() {
  'use strict';

  angular
    .module('avisare.address', [
      'avisare.auth'
    ]);
})();
