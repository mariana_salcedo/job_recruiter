(function iife() {
  'use strict';

  angular
    .module('avisare.address')
    .controller('AddressController', AddressController);

  /* @ngInject */
  function AddressController(
    $scope,
    $q,
    Auth,
    cityService,
    stateService,
    addressService,
    countryService,
    _) {

    var vm = this;
    vm.title = 'AddressController';
    vm.currentCountry = null;
    vm.currentState = null;
    vm.address = {};

    vm.changeCity = changeCity;
    vm.changeState = changeState;
    vm.changeCountry = changeCountry;

    activate();

    ////////////////

    function activate() {
      countryService.get().
      $promise.then(function setUserAddress(countries) {
        vm.countries = countries;
      });
      Auth.profileDone.then(function setData() {
        addressService.get().
        $promise.then(function setUserAddress(addresses) {
          vm.addresses = addresses;
          if (addresses.length) {
            vm.address = _.pick(addresses[0].Address, _.identity);
            cityService.getById({
              id: vm.address.CityId
            })
            .$promise
            .then(function getCity(city) {
              vm.country = city.State.Country;
              changeCountry(vm.country.id);
              vm.state = city.State;
              changeState(vm.state.id);
              vm.city = city;
              changeCity(vm.city.id);
            });
          } else {
            vm.address = {};
          }
        });
      });
    }

    function changeCountry(idCountry) {

      if (idCountry !== vm.currentCountry && !!idCountry) {
        vm.currentCountry = idCountry;

        clearCity();
        clearStates();
        getStates(idCountry);
      }

      if (vm.country.countryName === 'United States' && vm.address.phoneNumber === undefined) {
        vm.address.phoneNumber = '+1';
      } else {
        if (vm.address.phoneNumber === '+1') {
          vm.address.phoneNumber = undefined;
        }
      }
    }

    function changeState(idState) {
      if (idState !== vm.currentState && !!idState) {
        vm.currentState = idState;
        clearCity();
        getCities(idState);
      }
    }

    function changeCity(idCity) {
      vm.address.CityId = idCity;
    }

    function getStates(idCountry) {
      stateService.get({
        countryCode: idCountry
      }).$promise
      .then(function setState(states) {
        vm.states = states;
      });
    }

    function getCities(idState) {
      cityService.get({
        stateCode: idState
      }).$promise
      .then(function setCities(cities) {
        vm.cities = cities;
      });
    }

    function clearCity() {
      delete vm.city;
      delete vm.cities;
      delete vm.address.CityId;
    }

    function clearStates() {

      delete vm.state;
      delete vm.states;
    }

    function saveAddress() {

      if (!vm.address.addressLineOne) {
        vm.address.addressLineOne = '';
      }

      var attributes = _.size(_.pick(vm.address,
        'addressLineOne',
        'CityId'
      ));

      if (attributes >= 2) {
        if (!vm.address.id) {
          return addressService.post(vm.address).$promise
          .then(function(newAddress) {
            vm.address.id = newAddress.AddressId;
            return newAddress;
          });
        } else {
          return addressService.update(vm.address).$promise;
        }
      } else {
        return $q.resolve(false);
      }

    }

    function syncProfileData(event, token) {

      saveAddress(vm.address)
      .then(function notifySuccess() {
        $scope.$emit('AddressSync', token, true);
      })
      .catch(function notifyError() {
        $scope.$emit('AddressSync', token, true);
      });

    }

    $scope.$on('SyncProfile', syncProfileData);
  }
})();
