(function() {
  'use strict';

  angular
    .module('avisare.router', [
      'avisare.user',
      'avisare.core'
    ])
    .config(uiRouterConfig)
    .run(uiRouterRun);

  /* @ngInject */
  function uiRouterConfig($stateProvider, $urlRouterProvider, USER_ROLES, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/consultant/dashboard');

    $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: '/public/templates/_user_login.html',
      controller: 'LoginController',
      controllerAs: 'login',
      authenticate: false
    })
    .state('reset', {
      url: '/reset',
      templateUrl: '/public/templates/_reset_password.html',
      controller: 'ResetController',
      controllerAs: 'reset'
    })
    .state('consultant', {
      url: '/consultant',
      abstract: true,
      templateUrl: '/public/templates/_consultant_dashboard.html',
      controller: 'UserController',
      controllerAs: 'userCtrl',
      resolve: {
        profileIsLoaded: profileDone
      },
      authenticate: true
    })
    .state('consultant.profile', {
      url: '/profile',
      templateUrl: '/public/templates/_manage_profile.html',
      controller: 'TabController',
      controllerAs: 'main',
      authenticate: true
    })
    .state('consultant.dashboard', {
      url: '/dashboard',
      templateUrl: '/public/templates/_consultant_home.html',
      controller: 'DashboardController',
      controllerAs: 'dash',
      authenticate: true
    })
    .state('consultant.invite', {
      url: '/invite',
      templateUrl: '/public/templates/_invites_layout.html',
      authenticate: true
    })
    .state('consultant.website', {
      url: '/website',
      templateUrl: '/public/templates/_edit_website.html',
      controller: 'TabWebSiteController',
      controllerAs: 'tabsWebSiteCtrl',
      authenticate: true
    })
    .state('admin', {
      url: '/admin',
      abstract: true,
      templateUrl: '/public/templates/_superadmin_dashboard.html',
      controller: 'UserController',
      controllerAs: 'userCtrl',
      resolve: {
        profileIsLoaded: profileDone
      },
      authenticate: true,
      isAdmin: true
    })
    .state('admin.consultants', {
      url: '/consultants',
      templateUrl: '/public/templates/_manage_consultants.html',
      controller: 'ConsultantController',
      controllerAs: 'consultant',
      authenticate: true,
      isAdmin: true
    })
    .state('admin.skills', {
      url: '/skills',
      templateUrl: '/public/templates/_manage_skill.html',
      controller: 'SkillController',
      controllerAs: 'skill',
      authenticate: true,
      isAdmin: true
    })
    .state('admin.industries', {
      url: '/industries',
      templateUrl: '/public/templates/_manage_industries.html',
      controller: 'IndustriesController',
      controllerAs: 'industry',
      authenticate: true,
      isAdmin: true
    })
    .state('admin.industryFunctions', {
      url: '/industryFunctions',
      templateUrl: '/public/templates/_manage_industryFunctions.html',
      controller: 'IndustryFunctionsController',
      controllerAs: 'industryFunctions',
      authenticate: true,
      isAdmin: true
    })
    .state('admin.prospects', {
      url: '/prospects',
      templateUrl: '/public/templates/_manage_prospects.html',
      controller: 'ProspectsController',
      controllerAs: 'prospect',
      authenticate: true,
      isAdmin: true
    })
    .state('client', {
      url: '/client',
      abstract: true,
      templateUrl: '/public/templates/_client_dashboard.html',
      controller: 'UserController',
      controllerAs: 'userCtrl',
      resolve: {
        profileIsLoaded: profileDone
      },
      authenticate: true
    })
    .state('client.dashboard', {
      url: '/dashboard',
      templateUrl: '/public/templates/_client_home.html',
      controller: 'DashboardController',
      controllerAs: 'dash',
      authenticate: true
    })
    .state('client.listrfp', {
      url: '/rfp',
      templateUrl: '/public/templates/_list_rfp.html',
      controller: 'RFPController',
      controllerAs: 'rfpCtrl',
      authenticate: true
    })
    .state('client.consultants', {
      url: '/consultants',
      templateUrl: '/public/templates/_client_consultants.html',
      controller: 'ClientController',
      controllerAs: 'client',
      authenticate: true
    })
    .state('client.newrfp', {
      url: '/new/rfp',
      templateUrl: '/public/templates/_rfp_layout.html',
      controller: 'RFPController',
      controllerAs: 'rfpCtrl',
      authenticate: true
    })
    .state('client.previewrfp', {
      url: '/preview/rfp',
      templateUrl: '/public/templates/_preview_rfp.html',
      controller: 'RFPController',
      controllerAs: 'rfpCtrl',
      authenticate: true
    })
    .state('client.pmo', {
      url: '/pmo',
      templateUrl: '/public/templates/_pmo_tabs.html',
      controller: 'TabsPMOController',
      controllerAs: 'tabsCtrl',
      authenticate: true
    });

    /* @ngInject */
    function profileDone(Auth, $q) {
      return Auth.profileDone;
    }
  }

  /* @ngInject */
  function uiRouterRun($rootScope, $state, Auth) {
    $rootScope
    .$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      if (toState.authenticate && !Auth.isLoggedIn()) {
        // User isn’t authenticated
        $state.transitionTo('login');
        event.preventDefault();
      }
      if (toState.isAdmin && !Auth.isAdmin()) {
        // User isn’t authenticated
        $state.transitionTo('consultant.dashboard');
        event.preventDefault();
      }
    });

    Auth.init();
  }

})();
