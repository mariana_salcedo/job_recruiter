(function() {
  'use strict';

  angular
      .module('avisare.invite')
      .controller('InviteController', InviteController)
      .controller('InvitePopOutController', InvitePopOutController);

  /* @ngInject */
  function InviteController(allInvites, remindInvites, $uibModal, sendInvite, toaster, _, $state, $animate){
    var vm = this;

    vm.editMode = false;
    vm.invitedEmails = [];
    vm.tempInvitation = {};
    vm.paginationObj = {
      limit : 25,
      offset: 0,
      order: null,
      orderBy: null,
      query: ''
    };
    vm.gridOptions = {
      columnDefs: [
        {field: 'firstName', displayName:'First Name'},
        {field: 'lastName', displayName:'Last Name'},
        {field: 'email', displayName: 'Email'},
        {field: 'createdAt', displayName:'Created At', cellFilter: 'date:\'MM/dd/yyyy\''},
        {field: 'lastRemind', displayName: 'Last Remind', cellFilter: 'date:\'MM/dd/yyyy\''},
        {field: 'status', displayName:'Status'},
        {name: 'Reminds', cellTemplate: '<div class="ui-grid-cell-contents"><button class="btn btn-primary btn-xs" ng-click="grid.appScope.invite.remindInvite(row.entity.id)"><span class="glyphicon glyphicon-refresh"></span>&nbsp;<span ng-show="row.entity.reminds >0 ? true : false" > <strong>{{row.entity.reminds}}</strong></span></button></div>'}
      ],
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: true,
      onRegisterApi: function(gridApi){
        vm.gridApi = gridApi;
        $animate.enabled(vm.gridApi.grid.element, false);
        vm.gridApi.pagination.on.paginationChanged(null, function (newPage, pageSize) {
          vm.paginationObj.offset = (newPage - 1) * pageSize;
          vm.paginationObj.limit = pageSize;
          vm.getInvites();
        });

        vm.gridApi.core.on.sortChanged(null, function(grid, sortColumns) {
          if (sortColumns.length == 0) {
            vm.paginationObj.order = null;
            vm.paginationObj.orderBy = null;
          } else {
            vm.paginationObj.order = sortColumns[0].sort.direction;
            vm.paginationObj.orderBy = sortColumns[0].name;
          }
          vm.getInvites();
        });
      }
    };

    vm.getInvites = getInvites;
    vm.showForm = showForm;
    vm.remindInvite = remindInvite;

    function getInvites() {
      if($state.$current.parent.isAdmin){
        allInvites.get(vm.paginationObj).$promise.then(function (res) {
          vm.gridOptions.totalItems = res.count;
          vm.gridOptions.data = res.rows;
        });
      }else{
        allInvites.getMine(vm.paginationObj).$promise.then(function (res) {
          vm.gridOptions.totalItems = res.count;
          vm.gridOptions.data = res.rows;
        });
      }


    };
    vm.getInvites();

    function showForm(size) {
      var modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_new_invite.html',
        controller: 'InvitePopOutController',
        controllerAs: 'invite',
        animation: true,
        size: size,
        resolve: {
          invitedEmails: function () {
            return vm.invitedEmails;
          },
          tempInvitation: function () {
            return vm.tempInvitation;
          }
        }
      });
      modalInstance.result.then(function (res) {
        toaster.pop({
          type:'note',
          title:'',
          body:'Sending invites, please wait',
          showCloseButton: false
        });
        sendInvite.post(res).$promise.then(function (response) {
          vm.getInvites();
          var error = false;
          _.forEach(response, function(responseObj){
            if (!responseObj.success){
              toaster.error({body: 'Unable to send invite to '+responseObj.email});
              error = true;
            }
          });
          if (!error){
            toaster.success({body: 'Invites sent Successfully'});
          }
        }).catch(function(errors) {
          _.forEach(errors.config.data, function(error){
            toaster.error({body: 'Unable to send invite to '+error.email});
          });
        });
        vm.invitedEmails = [];
        vm.tempInvitation = {};
      }, function () {
        vm.invitedEmails = [];
        vm.tempInvitation = {};
      });
    }

    function remindInvite(id){
      var inviteObj = {
        id: id
      };
      remindInvites.update(inviteObj).$promise.then(function(){
        getInvites();
      });
    }
  }

  function InvitePopOutController($uibModalInstance, invitedEmails, tempInvitation, $state) {
    var vm = this;
    vm.routeMessages = '/public/templates/_invite_error_messages.html';
    vm.invitedEmails = invitedEmails;
    vm.tempInvitation = tempInvitation;
    vm.editMode = false;
    vm.isAdmin = $state.$current.parent.isAdmin;

    vm.sendInvites = sendInvites;
    vm.pushInvitation = pushInvitation;
    vm.editInvite = editInvite;
    vm.saveInvite = saveInvite;
    vm.removeInvite = removeInvite;

    function sendInvites() {
      $uibModalInstance.close(vm.invitedEmails);
    }

    function pushInvitation() {
      //console.log('asdasd',vm.tempInvitation);
      vm.tempInvitation.type = 'Consultant';
      vm.invitedEmails.push(vm.tempInvitation);

      if (vm.tempInvitation.saveMessage) {
        var message = vm.tempInvitation.message;
        vm.tempInvitation = {};
        //console.log('var', message);
        vm.tempInvitation = {message: message, saveMessage: true};
      }else {
        vm.tempInvitation = {};
      }
      //clear form validations
      vm.inviteForm.$setPristine();
      vm.inviteForm.$setUntouched();
    };

    function editInvite(invite) {
      //console.log('Edit Invite', invite);
      vm.editMode = true;
      vm.tempInvitation = invitedEmails[invite];

    };

    function saveInvite(invite) {

      vm.tempInvitation = null;
      vm.editMode = false;

      //clear form validations
      vm.inviteForm.$setPristine();
      vm.inviteForm.$setUntouched();
    };

    function removeInvite(email) {
      //console.log(email);
      vm.invitedEmails.splice(email, 1);
    };
  }
})();