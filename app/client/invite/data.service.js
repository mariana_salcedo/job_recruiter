(function () {
  'use strict';

  angular
      .module('avisare.invite')
      .factory('allInvites', allInvites)
      .factory('sendInvite', sendInvite)
      .factory('remindInvites', remindInvites)

  /* @ngInject */
  function allInvites($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'invites', {},
    {
      get:{
          method: 'GET'
        },
      getMine:{
        url: BASE_API_URI + 'invites/me',
        method: 'GET'
      },
    });
    return service;
  }
  function sendInvite($resource, $q, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'invites/bulk', {},
    {
      post:{
        method: 'POST',
        isArray: true
      }
    });
    return service;
  }
  function remindInvites($resource, $q, BASE_API_URI){
    var service = $resource(BASE_API_URI + 'invites/:id/remind', {id: '@id'},
      {
        update:{
          method: 'PUT'
        }
      });
    return service;
  }

})();
