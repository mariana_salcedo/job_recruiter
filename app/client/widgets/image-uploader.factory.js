(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .factory('CloudinaryFactory', CloudinaryFactory);

  /* @ngInject */
  function CloudinaryFactory($q, BASE_CLOUDINARY_URL, cloudinary) {
    var service = {
      createWidget: createWidget,
      buildUrl: buildUrl,
      cropUrl: cropUrl
    };
    return service;

    ////////////////

    function buildUrl(ImagePath) {

      return BASE_CLOUDINARY_URL + ImagePath;
    }

    function cropUrl(ImagePath, width, height) {
      return BASE_CLOUDINARY_URL +
      'w_' + (width || 150) +
      ',h_' + (height || 150) +
      ',c_fill' + '/' + ImagePath;
    }

    function createWidget() {
      var deferred = $q.defer();
      cloudinary.createUploadWidget({
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        upload_preset: 'rssvmgxx',
        cloud_name: 'avisare',
        cropping: 'server',
        multiple: false,
        cropping_aspect_ratio: 1
      },
      function(error, result) {
        if (!!error) {
          deferred.reject(error);
        } else {
          deferred.resolve(result);
        }
      }).open();
      return deferred.promise;
    }
  }
})();
