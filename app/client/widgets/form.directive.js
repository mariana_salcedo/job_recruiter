(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('avisareForm', avisareForm);

  /* @ngInject */
  function avisareForm(_) {

    var directive = {
      restrict: 'A',
      link: link,
      require: '^form'
    };
    return directive;

    function link(scope, element, attrs, ngForm) {
      scope.$watch(ngForm.$name + '.$submitted', onSubmit);

      function onSubmit(value) {
        if (!!value && ngForm.$valid) {

          var fields = _.values(ngForm.$$success).concat(_.values(ngForm.$error));
          _.forEach(_.unique(_.flatten(fields)), setTouched);
        }
      }

      function setTouched(field) {
        field.$setTouched();
        field.$validate();
      }
    }
  }
})();
