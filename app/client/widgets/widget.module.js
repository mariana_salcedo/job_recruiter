(function() {
  'use strict';

  angular
      .module('avisare.widget', [
        'ui.bootstrap'
      ]);
})();
