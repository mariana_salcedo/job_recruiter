(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('avisareEmail', avisareEmail);

  /* @ngInject */
  function avisareEmail(EmailService, $q) {

    var directive = {
      restrict: 'A',
      link: link,
      require: '?ngModel'
    };
    return directive;

    function link(scope, element, attrs, ngModel) {
      console.log('Add validation');
      ngModel.$asyncValidators.uniqueEmail = function validateEmail(email) {
        console.log('Do validation');
        return EmailService.validateError({
          email: email
        })
        .$promise.then(function response(emailCheck) {
          $(element).trigger('validate');
          if (emailCheck.isTaked) {
            return $q.reject(false);
          }
          return true;
        });
      };
    }
  }
})();
