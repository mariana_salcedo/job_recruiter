(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('avisareFile', avisareForm);

  /* @ngInject */
  function avisareForm(FILE_SIZE, FileService, _, toaster) {

    var fileSize = FILE_SIZE || 5000;

    var directive = {
      restrict: 'AE',
      link: link,
      require: 'ngModel',
      templateUrl: '/public/templates/_file.directive.html',
      replace: true,
      controller: Controller,
      bindToController: true,
      controllerAs: 'fileCtrl',
      scope: {
        file: '=ngModel',
        placeholder: '@',
        require: '@'
      }
    };
    return directive;

    function link(scope, element, attrs, ngModel) {

      function checkSize(file) {

        var notHuge = file.size < fileSize;

        ngModel.$setValidity('fileToBig', notHuge);

        return notHuge;
      }

      function handleFileSelect(evt) {

        var files = evt.target.files;
        if (files.length > 0) {
          var file = files[0];
          if (checkSize(file)) {
            sendFile(file);
          } else {
            toaster.error({
              body: 'File ' + file.name + ' is bigger than 5MB, please choose another one.'
            });
          }
        }
      }

      function sendFile(file) {

        var fromData = new FormData();

        var data = {
          type: 'File',
          name: file.name,
          fileObject: file
        };

        _.forEach(data, function appendToForm(value, key) {
          fromData.append(key, value);
        });

        FileService.post({}, fromData).$promise
        .then(function setFile(fileData) {

          toaster.success({body: 'File ' + file.name + ' uploaded succesfully'});

          scope.file = fileData;

          ngModel.$setViewValue(fileData);
        }, angular.noop, function (event) {
          console.log('Progress:', event);
        })
        .catch(function uploadFail() {
          toaster.error({body: 'Fail to upload file ' + file.name});
        });
      }

      element[0].addEventListener('change', handleFileSelect, false);
    }

    function Controller() {

    }
  }
})();
