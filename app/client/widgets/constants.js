(function() {
  'use strict';

  angular
    .module('avisare.widget')
    .constant('BASE_API_URI','/api/')
    .constant('BASE_CLOUDINARY_URL','http://res.cloudinary.com/avisare/image/upload/')
    .constant('DATE_FORMAT', 'MM/dd/yyyy')
    .constant('FILE_SIZE', 1e+7)
    .constant('cloudinary', window.cloudinary);
})();
