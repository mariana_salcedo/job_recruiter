(function() {
  'use strict';

  angular
    .module('avisare.widget')
    .factory('EmailService', EmailService);

  /* @ngInject */
  function EmailService($resource, BASE_API_URI) {
    return $resource(BASE_API_URI + 'invites/validate', {},{
      validateError:{
        method: 'GET'
      }
    });
  }
})();
