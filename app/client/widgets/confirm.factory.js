(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .factory('ConfirmFactory', ConfirmFactory);

  /* @ngInject */
  function ConfirmFactory($uibModal, _) {

    var basicAttr = {
      controller: Controller,
      controllerAs: 'Modal',
      animation: true,
      replace: true
    };

    var modalInstance;
    var service = {
      confirm: confirm,
      alert: alert
    };

    var defaultConfirm = {
      header: 'Are you sure?',
      body: 'Do you want to continue',
      acceptText: 'Accept',
      declineText: 'Decline'
    };

    var defaultAlert = {
      header: 'Alert',
      body: '',
      acceptText: 'Accept'
    };
    return service;

    ////////////////

    function alert(accept, options) {
      modalInstance = $uibModal.open(_.assign({
        templateUrl: '/public/templates/_alert.directive.html',
        resolve: {
          data: function() {
            var confirmOption = options || {};
            return _.assign(_.clone(defaultAlert), confirmOption);
          }
        }
      }, basicAttr));
      modalInstance.result
      .finally(accept);
    }

    function confirm(accept, options) {

      modalInstance = $uibModal.open(_.assign({
        templateUrl: '/public/templates/_confirm.directive.html',
        resolve: {
          data: function() {
            var confirmOption = options || {};
            confirmOption.accept = accept;
            return _.assign(_.clone(defaultConfirm), confirmOption);
          }
        }
      }, basicAttr));
    }

    function closeModal() {
      if (!!modalInstance && modalInstance.close) {
        modalInstance.close();
      }
    }

    function Controller(data) {
      var vm = this;
      vm.data = data;
      vm.close = closeModal;
    }
  }
})();
