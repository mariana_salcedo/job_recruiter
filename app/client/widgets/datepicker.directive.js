(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('avisareDatepicker', datepickerDirective);

  /* @ngInject */
  function datepickerDirective() {

    var directive = {
      bindToController: true,
      controller: Controller,
      controllerAs: 'vm',
      templateUrl: '/public/templates/_datepicker.directive.html',
      restrict: 'E',
      replace: true,
      require: 'ngModel',
      scope: {
        date: '=ngModel',
        minDate:  '='
      }
    };
    return directive;

  }

  /* @ngInject */
  function Controller(DATE_FORMAT) {

    var vm = this;
    vm.open = false;
    vm.dateFormat = DATE_FORMAT;
  }
})();
