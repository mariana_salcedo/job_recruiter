(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('cloudinaryUploader', CloudinaryUploader);

  /* @ngInject */
  function CloudinaryUploader(cloudinary) {

    var directive = {
      bindToController: true,
      templateUrl: '/public/templates/_cloudinary.directive.html',
      controller: CloudinaryUploaderCtrl,
      controllerAs: 'vm',
      transclude: true,
      link: link,
      restrict: 'E',
      scope: {
        img: '=ngModel'
      }
    };
    return directive;

    function link(scope, element, attrs) {
    }
  }

  /* @ngInject */
  function CloudinaryUploaderCtrl($scope, CloudinaryFactory) {

    var vm = this;

    vm.open = openWidget;

    vm.buildUrl = CloudinaryFactory.buildUrl;

    activate();

    ////////////////////

    function activate() {
      if (!!vm.img) {
        vm.img.url = CloudinaryFactory.buildUrl(vm.img.cloudinaryUrl);
      }
    }

    function openWidget() {
      CloudinaryFactory.createWidget()
      .then(function setImage(data) {
        if (data.length > 0) {

          var img = data[0];

          vm.img = {
            // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
            type: 'Image',
            extension: 'jpg',
            cloudinaryUrl: img.path,
            size: img.bites,
            url: img.secure_url,
            height: 251,
            width: 188
          };
        }
      });
    }
  }
})();
