(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .filter('avisareurl', urlFilter);

  /* @ngInject */
  function urlFilter(_) {
    return function (value) {
      console.log(value);
      var lastSlash = _.lastIndexOf(value, '/');
      value = String(value);
      var newValue = value.substr(lastSlash + 14); // +14 because the timestamp
      return newValue;
    };
  }
})();
