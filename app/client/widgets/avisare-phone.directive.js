(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('avisarePhone', PhoneDirective);

  var maskList = $.masksSort(
    $.masksLoad('/public/site/js/phone-codes.json'), ['#'], /[0-9]|#/, 'mask');

  var maskOpts = {
    inputmask: {
      definitions: {
        '#': {
          validator: '[0-9]',
          cardinality: 1
        }
      },
      showMaskOnHover: false,
      autoUnmask: false
    },
    match: /[0-9]/,
    replace: '#',
    list: maskList,
    listKey: 'mask'
  };

  /* @ngInject */
  function PhoneDirective() {

    var directive = {
      require: 'ngModel',
      restrict: 'A',
      link: link
    };
    return directive;

    function link(scope, element, attr, ngModel) {

      $(element).keyup(updateViewValue);

      $(element).inputmasks(maskOpts);

      ngModel.$validators.phone = validatePhone;

      function validatePhone(value) {
        return !/_/g.test(value);
      }

      function updateViewValue() {
        ngModel.$setViewValue(element.val());
      }
    }
  }
})();
