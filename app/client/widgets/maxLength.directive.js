(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('myMaxlength', myMaxlength);

  /* @ngInject */
  function myMaxlength($compile, $log) {

    var directive = {
      restrict: 'A',
      require: 'ngModel',
      link: link
    };
    function link(scope, elem, attrs, ctrl) {
      attrs.$set('ngTrim', 'false');
      console.log('elem', elem);
      var maxlength = parseInt(attrs.myMaxlength, 10);
      ctrl.$parsers.push(function (value) {
        //elem.after($('<p>').append('Hola'));
        elem.closest('.form-group').find('p').html('Characters left: '+(maxlength-value.length));
        //$log.info('In parser function value = [' + value + '].');
        if (value.length > maxlength)
        {
          //$log.info('The value [' + value + '] is too long!');
          value = value.substr(0, maxlength);
          ctrl.$setViewValue(value);
          ctrl.$render();
          //$log.info('The value is now truncated as [' + value + '].');
        }
        return value;
      });
    }
    return directive;

  }
})();



