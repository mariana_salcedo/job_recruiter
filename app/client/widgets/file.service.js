(function() {
  'use strict';

  angular
    .module('avisare.widget')
    .factory('FileService', FileService);

  /* @ngInject */
  function FileService($resource, BASE_API_URI) {
    return $resource(BASE_API_URI + 'uploadFiles', {},{
      post:{
        method: 'POST',
        headers: {
          'Content-Type': undefined
        }
      }
    });
  }
})();
