(function() {
  'use strict';

  angular
    .module('avisare.widget')
    .factory('ImageService', ImageService);

  /* @ngInject */
  function ImageService($resource, BASE_API_URI) {
    return $resource(BASE_API_URI + 'images', {},{
      post:{
        method: 'POST'
      }
    });
  }
})();
