(function iife() {
  'use strict';

  angular
    .module('avisare.widget')
    .directive('avisareInput', inputFormDirective);

  /* @ngInject */
  function inputFormDirective(_) {

    var directive = {
      restrict: 'A',
      link: link,
      require: '?ngModel'
    };
    return directive;

    function link(scope, element, attrs, ngModel) {

      if (attrs.required) {
        element.closest('.form-group').find('label').first().addClass('required');
      }

      var messageDiv = $('<div>').addClass('error-messages');
      var message = $('<div>');
      var error = '';

      element.closest('.form-group').append(messageDiv);
      messageDiv.hide();

      ngModel.$validators.all = parseData;

      $(element).on('blur validate', function blur(e) {
        scope.$evalAsync(parseData);
      });

      function parseData(value) {
        _.forEach(ngModel.$error, function(num, key) {
          switch (key) {
            case 'url':
              error = 'The field must be a valid URL';
              break;
            case 'date':
              error = 'The field must be a date';
              break;
            case 'uniqueEmail':
              error = 'This email is already assign to another user';
              break;
            case 'required':
              error = 'Field required';
              break;
            case 'email':
              error = 'The field must be an email';
              break;
            case 'minlength':
              error = 'Too short';
              break;
            case 'phone':
              error = 'Invalid phone number';
              break;
            case 'maxlength':
              error = 'Too long';
              break;
            case 'uniqueLink':
              error = 'The url already exist';
              break;
            default:
              error = 'An error has occured';
              break;
          }
          return false;
        });
        if (!ngModel.$valid && ngModel.$touched) {
          message.text(error);
          messageDiv.html(message);
          element.closest('.form-group').addClass('has-error');
          messageDiv.show();
        } else {
          element.closest('.form-group').removeClass('has-error');
          message.text('');
          messageDiv.hide();
        }
        return true;
      }
    }
  }
})();
