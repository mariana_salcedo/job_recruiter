(function iife() {
  'use strict';

  angular
    .module('avisare.auth')
    .factory('AuthInterceptor', AuthInterceptor);

  function AuthInterceptor($q, $location, AuthToken) {

    var interceptorFactory = {
      request: request,
      responseError: responseError
    };

    return interceptorFactory;

    // this will happen on all HTTP requests
    function request(config) {

      // grab the token
      var token = AuthToken.getToken();

      // if the token exists, add it to the header as x-access-token
      if (token) {
        config.headers['Authorization'] = 'Bearer ' + token;
      }
      return config;
    }

    // happens on response errors
    function responseError(response) {
      console.log(response);
      if (response.status === 401) {
        AuthToken.setToken();
        $location.path('/login');
      }

      // return the errors from the server as a promise
      return $q.reject(response);
    }
  }

})();
