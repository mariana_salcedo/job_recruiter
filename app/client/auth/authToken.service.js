(function iife() {
  'use strict';

  angular
    .module('avisare.auth')
    .factory('AuthToken', AuthToken);

  function AuthToken($window, TOKEN_NAME) {

    var authTokenFactory = {
      getToken: getToken,
      setToken: setToken
    };

    // get the token out of local storage
    function getToken() {
      return $window.localStorage.getItem(TOKEN_NAME);
    }

    // function to set token or clear token
    // if a token is passed, set the token
    // if there is no token, clear it from local storage
    function setToken(token) {
      if (!!token) {
        $window.localStorage.setItem(TOKEN_NAME, token);
      }
      else {
        $window.localStorage.removeItem(TOKEN_NAME);
      }
    }

    return authTokenFactory;
  }

})();
