(function iife() {
  'use strict';

  angular
    .module('avisare.auth')
    .factory('Auth', Auth);

  /* @ngInject */
  function Auth(
    AuthToken,
    BASE_API_URI,
    AuthService,
    toaster,
    $q,
    $location,
    CloudinaryFactory,
    _
  ) {

    var user = {};
    var profile = {};

    // create auth factory object
    var authFactory = {
      init: init,
      login: login,
      logout: logout,
      getUser: getUser,
      isAdmin: isAdmin,
      isClient: isClient,
      updateUser: updateUser,
      isLoggedIn: isLoggedIn,
      currentUser: currentUser,
      updateProfile: updateProfile,
      updatePicture: updatePicture,
      currentProfile: currentProfile,
      profileDone: $q.reject('User is not loggin')
    };

    return authFactory;

    function updateUser(newUser) {
      _.assign(user, newUser);
    }

    function currentUser() {
      return user;
    }

    function updatePicture(picture) {

      if (!!picture) {
        profile.profileImage = CloudinaryFactory.cropUrl(picture.cloudinaryUrl);
      }
    }

    function updateProfile(newProfile) {
      _.assign(profile, newProfile);
    }

    function currentProfile() {
      return profile;
    }
    function isAdmin() {

      return !!_.find(user.Roles, {roleName: 'super'});
    }

    function isClient() {

      return !!_.find(user.Roles, {roleName: 'client'});
    }

    // log a user in
    function login(username, password) {

      // return the promise object and its data
      return AuthService.login({
        email: username,
        password: password
      }).$promise
      .then(function(token) {
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers

        AuthToken.setToken(token.access_token);
        return getUser()
        .then(function notifyLoginSuccess(user) {

          toaster.success({body: 'Login Successful!'});
          return token;
        });
      })
      .catch(function methodName(data) {

        toaster.error({body: 'Login Fail, username or password invalid!'});
      });
    }

    // log a user out by clearing the token
    function logout() {

      // clear profile and user data
      user = {};
      profile = {};
      // clear the token
      AuthToken.setToken();
      toaster.success({body: 'Logout Successful!'});
    }

    // check if a user is logged in
    // checks if there is a local token
    function isLoggedIn() {
      return !!AuthToken.getToken();
    }

    // get the logged in user
    function getUser() {
      authFactory.profileDone = AuthService.getMe().$promise
      .then(function setUser(response) {
        user = response;

        if (!!response && !!response.Profiles && response.Profiles.length) {
          profile = response.Profiles[0];

          updatePicture(profile.File);
        }

        return response;
      });

      return authFactory.profileDone;
    }

    function init() {
      var token = _.pick($location.$$search, ['access_token', 'refresh_token']);
      if (!_.isEmpty(token)) {
        AuthToken.setToken(token.access_token);
        authFactory.profileDone = AuthService.verify().$promise
        .then(function getTheUser() {
          return getUser();
        });
      } else {
        if (isLoggedIn()) {
          authFactory.profileDone = getUser();
        }
      }
    }

  }

})();
