(function iife() {
  'use strict';

  angular
    .module('avisare.auth')
    .factory('AuthService', AuthService);

  /* @ngInject */
  function AuthService($resource, BASE_API_URI, AuthToken) {

    var service = $resource(BASE_API_URI + 'users/me', {},
    {
      getMe:{
        method: 'GET',
        url: BASE_API_URI + 'users/me',
        params: {
          include: true
        }
      },
      refresh:{
        method: 'POST',
        url: BASE_API_URI + 'refresh'
      },
      verify: {
        method: 'GET',
        url: BASE_API_URI + 'verifyToken'
      },
      login:{
        url: BASE_API_URI + 'login',
        method:'POST'
      }
    });

    return service;
  }

})();
