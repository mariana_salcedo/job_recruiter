(function iife() {
  'use strict';

  angular
    .module('avisare.auth', [
      'avisare.core'
    ]);
})();
