(function() {
  'use strict';

  angular
      .module('avisare.referral')
      .constant('TOKEN_NAME','referral_token');
})();
