(function iife() {
  'use strict';

  angular
    .module('avisare.referral')
    .controller('SiteReferralController', SiteReferralController);

  /* @ngInject */
  function SiteReferralController(
    referralInfo,
    ConfirmFactory,
    $stateParams,
    ReferenceService,
    $location,
    $window,
    toaster,
    _,
    TimeZones
  ) {

    var vm = this;
    vm.title = 'SiteReferralController';
    vm.user = referralInfo;
    vm.contact = {};
    vm.referral = {};
    vm.referralInfo = referralInfo;
    vm.referralUser = referralInfo.Application.User;
    vm.timeZones = TimeZones;
    vm.timesToCall = ['Morning', 'Afternoon', 'Evening'];
    vm.setDefaultTimezone = setDefaultTimezone;

    vm.sendResponse = sendResponse;

    setDefaultTimezone();

    activate();

    ////////////////

    function activate() {

      checkStatus();
      vm.contact.phone = referralInfo.phone;
    }

    function checkStatus() {
      if (referralInfo.status === 'Responded') {
        ConfirmFactory.alert(takeMeHome, {
          header: 'Thank you',
          acceptText: 'Accept',
          body: 'Your referral has been successfully submitted'
        });
        return false;
      }
      return true;
    }

    function sendResponse(form) {

      if (checkStatus()) {
        var data;

        if (vm.speakOnPhone === true) {
          data = _.cloneDeep(vm.contact);

          data.timezone = data.timezone.value;
        } else {
          data = _.clone(vm.referral);

          if (!!data.response) {
            data.response = JSON.stringify(data.response);
          }

        }

        if (form.$valid) {

          var params = _.clone($stateParams);
          params.responded = true;
          ReferenceService.update(params, data)
          .$promise.then(function notifySucess() {

            ConfirmFactory.alert(takeMeHome, {
              header: 'Thank you for your input!',
              acceptText: 'Accept',
              body: 'We received your response and will use it in the evaluation process.'
            });
          });
        } else {
          toaster.error({body : 'Please fill out required field(s).'});
        }
      }
    }

    function setDefaultTimezone() {
      var timezone = -((new Date()).getTimezoneOffset() / 60);
      vm.contact.timezone = _.find(TimeZones, {offset: timezone});
    }

    function takeMeHome() {
      $window.location.href = 'http://' + $window.location.host;
    }
  }
})();
