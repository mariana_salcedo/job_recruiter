(function() {
  'use strict';

  angular
    .module('avisare.referral')
    .config(uiRouterConfig)
    .run(uiRouterRun);

  /* @ngInject */
  function uiRouterConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('AuthInterceptor');

    $stateProvider
    .state('/referral', {
      url: '/referral/:id',
      controller: 'SiteReferralController',
      controllerAs: 'SiteCtrl',
      resolve: {
        referralInfo: findReferral
      }
    });

    /* @ngInject */
    function findReferral(Auth, ReferenceService, $stateParams) {
      return Auth.profileDone.then(function searchReferral() {
        return ReferenceService.get($stateParams).$promise;
      });
    }
  }

  /* @ngInject */
  function uiRouterRun(Auth) {

    Auth.init();
  }

})();
