/**
 * avisare
 * Created by Andres Juarez on 24/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.referral')
    .factory('ReferenceService', ReferenceService);

  /* @ngInject */
  function ReferenceService($resource, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'references/:id', {id: '@id'},
      {
        update:{
          method: 'PUT'
        },
        post:{
          url: BASE_API_URI + 'applications/:id/references',
          params: {id: '@id'},
          method: 'POST',
          isArray: false
        },
        getMine:{
          method: 'GET',
          url: BASE_API_URI + 'applications/:id/references',
          params: {id: '@id'},
          isArray: true
        }
      });

    return service;
  }
})();
