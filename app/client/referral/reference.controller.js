/**
 * avisare
 * Created by Andres Juarez on 24/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.referral')
    .controller('ReferenceController', ReferenceController);

  /* @ngInject */
  function ReferenceController(
  ApplicationFactory,
  ReferenceService,
  toaster,
  $scope,
  $q,
  _) {
    var vm = this;
    vm.title = 'ReferenceController';

    vm.reference = {};
    vm.references = [vm.reference, {}, {}];

    vm.status = ['Pending', 'Read', 'Responded'];
    vm.relation = ['Client', 'Manager', 'Colleague'];
    vm.communitacionMethod = ['Email', 'Phone', 'No preference'];

    vm.saveReference = saveReference;

    activate();

    //////////////////

    function activate() {
      ApplicationFactory.applicationDone.then(function setData() {
        vm.application = ApplicationFactory.currentApplication();
        if (vm.application.id) {
          ReferenceService.getMine({id: vm.application.id})
          .$promise
          .then(function setReferences(references) {
            vm.references = references;
            if (vm.references.length < 3) {
              for (var i = vm.references.length; i < 3; i++) {
                vm.references.push({});
              }
            }
            vm.reference = _.last(references);
          });
        }
      });
    }

    function saveReference(reference) {
      var application = ApplicationFactory.currentApplication();
      var referenceData = reference || vm.reference;
      if (!!referenceData.id) {
        return ReferenceService.update({
          id: referenceData.id
        },_.pick(referenceData, _.identity)).$promise;
      } else {
        referenceData.status = 'Pending';
        return ReferenceService.post({id: application.id}, referenceData).$promise
        .then(function setData(newReference) {
          _.assign(reference, newReference);
          if (!_.find(vm.references, reference)) {
            vm.references.push(reference);
          }
        });
      }
    }

    function saveReferences() {
      console.log('saveReferences', arguments, vm.references);

      var emails = _.remove(_.pluck(vm.references, 'email'), undefined);
      var phones = _.remove(_.pluck(vm.references, 'phone'), undefined);

      var duplicatedEmails = emails.length !== _.unique(emails).length;
      var duplicatedPhone = phones.length !== _.unique(phones).length;

      if (duplicatedEmails) {
        toaster.error({
          body: 'You have duplicated emails on your references'
        });
      }

      if (duplicatedPhone) {
        toaster.error({
          body: 'You have duplicated phones on your references'
        });
      }

      if (duplicatedEmails || duplicatedPhone) {

        return $q.reject(false);
      }

      var promises = _.map(vm.references, function save(reference) {

        var size = _.size(_.pick(reference,
          'id',
          'firstName',
          'lastName',
          'communitacionMethod',
          'email',
          'phone',
          'relation'));
        if (size < 4) {
          console.log('false size:', size);
          return $q.resolve(false);
        }
        return saveReference(reference)
          .catch(function returnNotification() {
            return reference;
          });
      });

      return $q.all(promises);
    }

    function syncReference(e, token) {
      saveReferences()
        .then(function notifySuccess(references) {

          _.forEach(references, function reviewCaseStudies(reference) {

            if (reference === false) {
              toaster.warning({
                body: 'Your progress has been saved, but you need to include 3 ' +
                'references before your application can proceed'});
            }
            return reference;
          });
          $scope.$emit('ReferencesSync', token, true);
        })
        .catch(function notifyError() {
          $scope.$emit('ReferencesSync', token, false);
        });
    }

    $scope.$on('SyncProfile', syncReference);
    $scope.$on('syncApplication', syncReference);
  }
})();
