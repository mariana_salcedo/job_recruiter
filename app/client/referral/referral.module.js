(function iife() {
  'use strict';

  angular
    .module('avisare.referral', [
      'avisare.core',
      'avisare.auth'
    ]);
})();
