(function () {
  'use strict';
  angular

  .module('avisare', [
    'avisare.core',
    'avisare.profile',
    'avisare.application',
    'avisare.website',
    'avisare.consultant',
    'avisare.router',
    'avisare.user',
    'avisare.skill',
    'avisare.industry',
    'avisare.industryFunctions',
    'avisare.prospect',
    'avisare.reset',
    'avisare.invite',
    'avisare.clientReg',
    'avisare.rfp',
    'avisare.client'
  ])
  .config(httpInterceptorConfig);
  /* @ngInject */
  function httpInterceptorConfig($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  }
})();