(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .controller('PressController', PressController);

  /* @ngInject */
  function PressController(
    $q,
    $scope,
    $uibModal,
    Auth,
    ImageService,
    PressService,
    CloudinaryFactory,
    _
  ) {
    var vm = this;
    var modalInstance;

    vm.title = 'PressController';

    vm.press = {};
    vm.presses = [];
    vm.openModal = openModal;
    vm.closeModal = closeModal;
    vm.removePress = removePress;
    vm.getSmallImage = getSmallImage;

    vm.savePress = savePress;

    activate();

    ////////////////

    function openModal(press) {
      if (!!press) {
        press.pressDate = new Date(press.pressDate);
      }
      vm.press = press || {};

      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_presses_form.html',
        animation: true,
        scope: $scope
      });
    }

    function closeModal() {

      if (!!modalInstance && modalInstance.close) {

        modalInstance.close();
      }
    }

    function removePress(press) {
      return PressService.delete({
        id: press.id
      }).$promise
      .then(function remove() {
        _.remove(vm.presses, press);
      });
    }

    function saveImage() {
      if (_.has(vm.press, 'File.id')) {
        return $q.resolve(vm.press.File);
      } else {
        return ImageService.post(vm.press.File).$promise
        .then(function setImage(image) {
          _.assign(vm.press.File, image);
          return image;
        });
      }
    }

    function getSmallImage(press) {

      if (press.FileId) {
        return CloudinaryFactory.cropUrl(press.File.cloudinaryUrl, 100, 100);
      }
    }

    function savePress() {
      return saveImage()
      .then(function savePress(image) {

        var data = _.pick(vm.press, ['pressName', 'pressFileUrl', 'pressDate']);

        data.FileId = image.id;

        if (vm.press.id) {
          return PressService.update({id: vm.press.id}, data).$promise;
        } else {

          return PressService.post({id: vm.profile.id}, data).$promise;
        }
      }).then(function setResult(pastClient) {

        if (!vm.press.id) {
          vm.presses.push(vm.press);
        }
        _.assign(vm.press, pastClient);
        closeModal();
      });
    }

    function activate() {

      vm.profile = Auth.currentProfile();
      PressService.getMine({id: vm.profile.id})
      .$promise
      .then(function setPress(presses) {
        vm.presses = presses;
        vm.press = _.last(presses);
      });
    }
  }
})();
