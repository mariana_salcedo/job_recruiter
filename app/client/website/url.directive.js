(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .directive('avisareUrl', avisareUrl);

  /* @ngInject */
  function avisareUrl(profileService, Auth, $q) {

    var directive = {
      restrict: 'A',
      link: link,
      require: '?ngModel'
    };
    return directive;

    function link(scope, element, attrs, ngModel) {
      ngModel.$asyncValidators.uniqueLink = function validateUrl(url) {
        if (url.length < 5) {
          return $q.resolve(false);
        } else {

          return profileService.checkLink({
            profileId: Auth.currentProfile().id,
            link: url
          })
          .$promise.then(function response(urlCheck) {
            if(!urlCheck.isFree) {
            return $q.reject();
            }
          });
        }
      };
    }
  }

  /* @ngInject */
  function Controller() {

  }
})();
