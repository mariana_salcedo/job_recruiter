/**
 * avisare
 * Created by Andres Juarez on 23/11/15.
 * email ajuarez@teravisiontech.com
 */
(function() {
  'use strict';

  angular
    .module('avisare.website', [
      'avisare.core',
      'avisare.profile',
      'avisare.widget',
      'avisare.application',
      'ngclipboard'
    ]);
})();
