(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .controller('PastClientController', PastClientController);

  /* @ngInject */
  function PastClientController(
    $q,
    $scope,
    $uibModal,
    Auth,
    ImageService,
    PastClientService,
    CloudinaryFactory,
    _
  ) {
    var vm = this;
    var modalInstance;

    vm.title = 'PastClientController';

    vm.client = {};
    vm.clients = [];
    vm.openModal = openModal;
    vm.closeModal = closeModal;
    vm.removeClient = removeClient;
    vm.getSmallImage = getSmallImage;

    vm.savePastClient = savePastClient;

    activate();

    ////////////////

    function openModal(client) {
      vm.client = client || {};

      modalInstance = $uibModal.open({
        templateUrl: '/public/templates/_pastClients_form.html',
        animation: true,
        scope: $scope
      });
    }

    function closeModal() {

      if (!!modalInstance && modalInstance.close) {

        modalInstance.close();
      }
    }

    function removeClient(client) {
      return PastClientService.delete({
        id: client.id
      }).$promise
      .then(function remove() {
        _.remove(vm.clients, client);
      });
    }

    function saveImage() {
      if (_.has(vm.client, 'File.id')) {
        return $q.resolve(vm.client.File);
      } else {
        return ImageService.post(vm.client.File).$promise
        .then(function setImage(image) {
          _.assign(vm.client.File, image);
          return image;
        });
      }
    }

    function getSmallImage(client) {
      return CloudinaryFactory.cropUrl(client.File.cloudinaryUrl, 100, 100);
    }

    function savePastClient() {
      return saveImage()
      .then(function saveClient(image) {

        var data = {
          pastClientName: vm.client.pastClientName,
          FileId: image.id
        };
        if (vm.client.id) {

          return PastClientService.update({id: vm.client.id}, data).$promise;
        } else {

          return PastClientService.post({id: vm.profile.id}, data).$promise;
        }
      }).then(function setResult(pastClient) {

        if (!vm.clients.id) {
          vm.clients.push(vm.client);
        }
        _.assign(vm.client, pastClient);
        closeModal();
      });
    }

    function activate() {

      vm.profile = Auth.currentProfile();
      PastClientService.getMine({id: vm.profile.id})
      .$promise
      .then(function setPastClients(pastClients) {
        vm.clients = pastClients;
        vm.client = _.last(pastClients);
      });
    }
  }
})();
