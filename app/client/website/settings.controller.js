/**
 * avisare
 * Created by Andres Juarez on 23/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .controller('SettingsController', SettingsController);

  /* @ngInject */
  function SettingsController($scope, $location, Auth, ServicesService, profileService, $q, _) {
    var vm = this;
    vm.tooltip = 'Add in 2-4 sentences describing the services that you offer. ';
    vm.tooltip += 'See the Sample website for an example';
    vm.serviceTagTooltip = 'Add in service tags in multiples of 3 I.e. ';
    vm.serviceTagTooltip += 'Social Media Marketing, Financial Projections, ';
    vm.serviceTagTooltip += 'Product Requirements Documents, Negotiation Training, etc.';
    vm.title = 'SettingsController';
    var serviceBkp = [];
    vm.serviceArray = [];
    vm.serviceSelected = [];
    vm.myServices = [];
    vm.saveService = saveService;
    vm.tagTransform = tagTransform;
    vm.removeService = removeService;
    vm.setDescription = setDescription;
    vm.syncSettingsData = syncSettingsData;

    function tagTransform(newTag) {

      var item = {
        serviceName: newTag,
        serviceDescription: ''
      };
      return item;
    }

    activate();

    //////////////////



    function activate() {
      Auth.profileDone.then(function setData() {
        vm.profile = _.clone(Auth.currentProfile());
        var profileId = vm.profile.id;
        return $q.all([
          ServicesService.get().$promise,
          profileService.getServices({profileId: profileId}).$promise,
          profileService.getSetting({profileId: profileId}).$promise
        ]).then(function (promises) {
            vm.serviceArray = promises[0];
            vm.myServices = _.map(promises[1], function getService(myService) {
              return myService.Service;
            });
            vm.serviceSelected = _.map(vm.myServices, function filter(myService) {
              var id = myService.id;
              return _.find(vm.serviceArray, function find(service) {
                return service.id === id;
              });
            });
            var settings = promises[2];
            vm.settings = _.isEmpty(settings) ? {} : settings;
            setUrl();
          });
      });

      $scope.$on('SyncAboutAll', syncSettingsData);
    }

    function saveSettings() {

      var data = _.pick(vm.settings, [
        'aboutMe',
        'profileLink',
        'serviceDescription'
      ]);
      if (!!vm.settings.id) {
        return profileService.updateSetting({profileId: vm.profile.id}, data).$promise;
      } else {
        return profileService.postSetting({profileId: vm.profile.id}, data).$promise
        .then(function setData(newSetting) {
          return _.assign(vm.settings, newSetting);
        });
      }
    }

    function saveService() {
      var newServices = _.filter(vm.serviceSelected, function getNewServices(service) {
        return !service.id || !_.find(vm.myServices, {id: service.id});
      });
      if (newServices.length) {
        return profileService.postServices({
          profileId: vm.profile.id
        }, newServices).$promise
        .then(function setServices(newServices) {
          _.forEach(newServices, function setNewService(service, index) {
            if (!!service.createdAt) {
              vm.serviceArray.push(_.assign(newServices[index], service));
            }
          });
        });
      } else {
        return $q.resolve(vm.serviceSelected);
      }
    }

    function setUrl() {
      var port = $location.port();
      Auth.currentProfile().websiteUrl = 'http://' +
      $location.host() + (!!port ? ':' + $location.port() : '') +
      '/consultant/' +
      vm.settings.profileLink;
      console.log(Auth.currentProfile().websiteUrl);
    }

    function saveAll() {
      return saveSettings().then(function sycService() {
        return saveService();
      });
    }

    function syncSettingsData(event, token) {
      saveAll()
        .then(function notifySuccess() {
          setUrl();
          $scope.$emit('AboutAllFinished', token, true);
        })
        .catch(function notifyError() {
          $scope.$emit('AboutAllFinished', token, false);
        });
    }

    function setDescription($item, $model) {
      $item.serviceDescription = '';
      return $item;
    }

    function removeService($item) {
      if ($item.id) {
        var objDelete = {
          id: vm.profile.id,
          services: [$item.id]
        };
        return profileService.deleteServices(objDelete).$promise
        .then(function methodName() {
          _.remove(vm.myServices, function removeElement(service) {
            return service.id === $item.id;
          });
        });
      }
    }
  }
})();
