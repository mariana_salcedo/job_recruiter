(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .factory('PastClientService', PastClientService);

  /* @ngInject */
  function PastClientService($resource, BASE_API_URI) {
    return $resource(BASE_API_URI + 'pastClients/:id', {id: '@id'},
    {
      get:{
        method: 'GET'
      },
      update:{
        method: 'PUT'
      },
      delete:{
        method: 'DELETE'
      },
      post:{
        method: 'POST',
        url: BASE_API_URI + 'profiles/:id/pastClients',
        params: {id: '@id'}
      },
      getMine:{
        method: 'GET',
        url: BASE_API_URI + 'profiles/:id/pastClients',
        params: {id: '@id'},
        isArray: true
      }
    });
  }
})();
