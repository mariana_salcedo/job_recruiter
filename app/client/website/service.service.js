/**
 * avisare
 * Created by Andres Juarez on 23/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .factory('ServicesService', ServicesService);

  /* @ngInject */
  function ServicesService($resource, BASE_API_URI) {
    var service = $resource(BASE_API_URI + 'services', {},
      {
        get:{
          method: 'GET',
          isArray: true
        },
        post:{
          method: 'POST'
        },
        update:{
          url: BASE_API_URI + 'services/:id',
          params: {id: '@id'},
          method: 'PUT'
        },
        getById:{
          url: BASE_API_URI + 'services/:id',
          params: {id: '@id'}
        }
      });

    return service;
  }
})();
