/**
 * avisare
 * Created by Andres Juarez on 23/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .controller('TabWebSiteController', TabWebSiteController);

  /* @ngInject */
  function TabWebSiteController($scope, toaster, _) {
    var vm = this;

    var listen = [
      'AboutAllFinished'
    ];

    vm.tabs = [
      {
        title: 'About/Services',
        content: '/public/templates/_about_services.html'
      },
      {
        title: 'Case Studies',
        content: '/public/templates/_cases_studies.html'
      },
      {
        title: 'Clients/Press',
        content: '/public/templates/_clients_press.html'
      },
      {
        title: 'Preview Website',
        content: '/public/templates/_preview.html'
      }
    ];
    vm.tabChange = function (value){
      console.log("value", value);
      if (value === 3){
        console.log("Im In");

      }
    }

    vm.syncAbout = syncAbout;

    _.forEach(listen, function(e) {
      $scope.$on(e, handelResponse);
    });

    function handelResponse(event, token, success) {
      if (token === currentToken) {
        currentToken[event.name] = success;
        checkTokenCompletion();
      }
    }

    function checkTokenCompletion() {
      if (_.size(currentToken) === listen.length) {
        toaster.success({body: 'Website info saved successfully'});
      }
    }

    var currentToken;

    function syncAbout() {
      currentToken = {};
      $scope.$broadcast('SyncAboutAll', currentToken);
    }

  }
})();
