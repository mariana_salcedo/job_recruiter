/**
 * avisare
 * Created by Andres Juarez on 23/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .controller('WebSiteController', WebSiteController);

  /* @ngInject */
  function WebSiteController($scope, Auth, _, BASE_API_URI) {
    var vm = this;

    vm.title = 'ProfileController';

    vm.syncWebSiteData = syncWebSiteData; 

    activate();

    function activate() {
      vm.user = _.clone(Auth.currentUser());
      vm.profile = _.clone(Auth.currentProfile());
      vm.uri = BASE_API_URI + 'consultant/';
    }

    function syncWebSiteData(event, token) {


    }

    $scope.$on('SyncProfile',syncWebSiteData);
  }
})();
