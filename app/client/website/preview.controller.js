/**
 * avisare
 * Created by Andres Juarez on 30/11/15.
 * email ajuarez@teravisiontech.com
 */
(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .controller('PreviewController', PreviewController);

  /* @ngInject */
  function PreviewController($scope, Auth, _, BASE_API_URI) {
    var vm = this;

    vm.title = 'PreviewController';

    vm.syncWebSiteData = syncWebSiteData;

    activate();

    function activate() {
      vm.user = _.clone(Auth.currentUser());
      vm.profile = _.clone(Auth.currentProfile());
      vm.uri = BASE_API_URI + 'consultant/';
    }

    function syncWebSiteData(event, token) {

    }

    $scope.$on('SyncProfile',syncWebSiteData);
  }
})();
