(function iife() {
  'use strict';

  angular
    .module('avisare.website')
    .factory('PressService', PressService);

  /* @ngInject */
  function PressService($resource, BASE_API_URI) {
    return $resource(BASE_API_URI + 'presses/:id', {id: '@id'},
    {
      get:{
        method: 'GET'
      },
      update:{
        method: 'PUT'
      },
      delete:{
        method: 'DELETE'
      },
      post:{
        method: 'POST',
        url: BASE_API_URI + 'profiles/:id/presses',
        params: {id: '@id'}
      },
      getMine:{
        method: 'GET',
        url: BASE_API_URI + 'profiles/:id/presses',
        params: {id: '@id'},
        isArray: true
      }
    });
  }
})();
