'use strict';

var SwaggerExpress = require('swagger-express-mw');
var express = require('express');
var nunjucks = require('express-nunjucks');
var path = require('path');
var initSwagger = require('./api/helpers/commons');
var consultant = require('./api/controllers/consultant');
var promise = require('bluebird');
var dateFilter = require('nunjucks-date-filter');
var env = process.env.NODE_ENV;

var config = {
  appRoot: __dirname // required config
};

var app = express();

app.locals.env = env;

app.set('view engine', 'html');

app.get(['/request', '/request/*'], function(req, res) {
  res.render('site/request');
});

app.get(['/reset', '/reset/*'], function(req, res) {
  res.render('site/reset');
});
// Application route
app.get(['/application', '/application/*'], function(req, res) {
  res.render('site/application');
});

app.get(['/about'], function(req, res) {
  res.render('site/about');
});
app.get(['/bio'], function(req, res) {
  res.render('site/bio');
});
app.get(['/client'], function(req, res) {
  res.render('site/client');
});
app.get(['/contact'], function(req, res) {
  res.render('site/contact');
});
app.get(['/example'], function(req, res) {
  res.render('site/example');
});
app.get(['/howitworks'], function(req, res) {
  res.render('site/howitworks');
});
app.get(['/referral/*'], function(req, res) {
  res.render('site/reference');
});
app.get(['/'], function(req, res) {
  res.render('site/home');
});
app.get(['/join'], function(req, res) {
  res.render('site/join');
});
app.get(['/terms'], function(req, res) {
  res.render('site/terms');
});

// App views
app.get(['/app', '/app/*'], renderStart);

function renderStart(req, res) {
  res.render('start');
}

app.get('/consultant/:profilePage', consultant.getWebSite);
app.get('/consultant/:profilePage/screen', consultant.getScreen);

app.use("/public/lib", express.static(path.join(__dirname + '/app/lib')));
app.use("/public/", express.static(path.join(__dirname + '/public')));

nunjucks.setup({
  autoescape: true,
  watch: true,
  tags: {
    blockStart: '<%',
    blockEnd: '%>',
    variableStart: '<$',
    variableEnd: '$>',
    commentStart: '<#',
    commentEnd: '#>'
  }
}, app, function(env) {

  env.addFilter('date', dateFilter);
});

initSwagger.setSwaggerOptions();

promise.promisifyAll(SwaggerExpress);

module.exports = SwaggerExpress.createAsync(config).then(function handleResponse(swaggerExpress) {

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  return app;
});
