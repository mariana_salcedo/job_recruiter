/**
 * Created by franzsilva on 26/10/15.
 */
"use strict";

var cloudinary = require('cloudinary');
var config = require('config').cloudinary;

cloudinary.config(config);

module.exports = {

  uploadImagebyUrl: uploadImagebyUrl

};

function uploadImagebyUrl(imageUrl) {

  cloudinary.uploader.upload(imageUrl, function(result) {
    console.log(result);
  });

}