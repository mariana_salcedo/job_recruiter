'use strict';

// packages
var Sequelize = require('sequelize');
var fs = require('fs');
var debug = require('debug')('sequelize-log');

// config
var config = require('../config').mysql;

config.logging = debug;
config.omitNull = false;

// variables
var database = config.database;
var username = process.env.DATABASE_USER || config.username;
var password = process.env.DATABASE_PASS || config.password;

// models path
var modelPath = __dirname + "/models";

if (!!process.env.DATABASE_HOST) {
  config.host = process.env.DATABASE_HOST;
}

// sequelize
var sequelize = new Sequelize(database, username, password, config);

// get models
var models = fs.readdirSync(modelPath);

models.forEach(function iterateModels(model) {

  if (model === "index.js") {
    return;
  }

  debug("Import sequelize models");
  // import model
  var modelCreated = sequelize.import(modelPath + "/" + model);

  debug("Model created %s", modelCreated);
  process.nextTick(function() {
    debug("Associate " + model + " model");
    return modelCreated.associate && modelCreated.associate(sequelize.models);
  });
});

// sequelize.sync().then(function() {debug("Database sync successful");});

module.exports = sequelize;