'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Certifications', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      certificationName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      certificationType: {
        allowNull: false,
        type: Sequelize.ENUM('Course', 'Master', 'Certificate')
      },
      certificationDate: {
        allowNull: false,
        type: Sequelize.DATE
      },
      issuer: {
        allowNull: false,
        type: Sequelize.STRING
      },
      ProfileId: {
        type: Sequelize.INTEGER,
        references: { model: "Profiles", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('Certifications');
  }
};
