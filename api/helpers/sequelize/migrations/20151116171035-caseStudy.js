'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('CaseStudies', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      caseStudyTitle: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      caseStudyProblem: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      caseStudySolution: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      caseStudyResult: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      IndustryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "Industries", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    })
    .then(function createProfileServicesTable() {
      return queryInterface.createTable('UserCaseStudies', {
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        ProfileId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Profiles", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        CaseStudyId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "CaseStudies", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('UserCaseStudies').then(function deleteSkill() {
      return queryInterface.dropTable('CaseStudies');
    });
  }
};
