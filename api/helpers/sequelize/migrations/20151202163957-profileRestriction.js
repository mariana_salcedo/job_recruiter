'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.changeColumn('Profiles','avaliability',{
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: function(queryInterface, Sequelize) {
    return queryInterface.changeColumn('Profiles','avaliability',{
      type: Sequelize.STRING,
      allowNull: false
    });
  }
};
