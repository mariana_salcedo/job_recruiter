'use strict';

var _ = require('lodash');

module.exports = {
  up: function (queryInterface, Sequelize) {

    return queryInterface.createTable('Industries',{
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      industryName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      industryStatus: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }
    }).then(function(){
      var industries = [
        {"id":"1","industryName":"Transportation & Logistics","industryStatus":true},
        {"id":"2","industryName":"Chemicals","industryStatus":true},
        {"id":"9","industryName":"Financial Services","industryStatus":true},
        {"id":"11","industryName":"Retail","industryStatus":true},
        {"id":"22","industryName":"Professional Services","industryStatus":true},
        {"id":"23","industryName":"Automotive","industryStatus":true},
        {"id":"25","industryName":"Utilities","industryStatus":false},
        {"id":"26","industryName":"Aerospace & Defense","industryStatus":true},
        {"id":"27","industryName":"Consumer Packaged Goods","industryStatus":true},
        {"id":"28","industryName":"Travel/Hospitality/Leisure","industryStatus":true},
        {"id":"29","industryName":"Education","industryStatus":true},
        {"id":"30","industryName":"Electronics","industryStatus":true},
        {"id":"31","industryName":"Energy & Environmental","industryStatus":true},
        {"id":"32","industryName":"Food & Beverage","industryStatus":true},
        {"id":"33","industryName":"Government","industryStatus":true},
        {"id":"34","industryName":"Healthcare","industryStatus":true},
        {"id":"35","industryName":"Insurance","industryStatus":true},
        {"id":"36","industryName":"Legal","industryStatus":true},
        {"id":"37","industryName":"Manufacturing","industryStatus":true},
        {"id":"38","industryName":"Media  & Entertainment","industryStatus":true},
        {"id":"39","industryName":"Mining & Extraction","industryStatus":true},
        {"id":"40","industryName":"Other","industryStatus":false},
        {"id":"41","industryName":"Pharma","industryStatus":true},
        {"id":"42","industryName":"Biotech","industryStatus":true},
        {"id":"43","industryName":"Technology","industryStatus":true},
        {"id":"44","industryName":"Telecommunications","industryStatus":true},
        {"id":"45","industryName":"Web & Design","industryStatus":true},
        {"id":"47","industryName":"Print & Media","industryStatus":false},
        {"id":"48","industryName":"Avisare Test Industry","industryStatus":false}
      ];
      var add = {createdAt: queryInterface.sequelize.fn('NOW'), updatedAt: queryInterface.sequelize.fn('NOW')};
      _.map(industries, function(industry) {_.extend(industry, add);});
      return queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('Industries', industries));
    }).then(function createIndustryFunctions(){
      return queryInterface.createTable('IndustryFunctions',{
        id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        functionName: {
          allowNull: false,
          type: Sequelize.STRING
        },
        functionDescription: {
          allowNull: true,
          type: Sequelize.TEXT
        },
        functionStatus: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true
        }
      }).then(function (){
        var functions = [
          {"functionName":"Business Development","functionStatus":true},
          {"functionName":"Finance","functionStatus":true},
          {"functionName":"Human Resources (HR)","functionStatus":true},
          {"functionName":"IT","functionStatus":true},
          {"functionName":"Interim C-Suite","functionStatus":true},
          {"functionName":"Marketing","functionStatus":true},
          {"functionName":"Operations","functionStatus":true},
          {"functionName":"Research and Development (R&D)","functionStatus":true},
          {"functionName":"Sales","functionStatus":true},
          {"functionName":"Strategy & Innovation<","functionStatus":true},
          {"functionName":"Technology","functionStatus":true}
        ];
        var add = {createdAt: queryInterface.sequelize.fn('NOW'), updatedAt: queryInterface.sequelize.fn('NOW')};
        _.map(functions, function(functionObj) {_.extend(functionObj, add);});
        return queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('IndustryFunctions', functions));
      });
    }).then(function createProfileFunctions(){
      return queryInterface.createTable('ProfileFunctions',{
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        ProfileId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Profiles", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        IndustryFunctionId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "IndustryFunctions", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    }).then(function createProfileIndustry(){
      return queryInterface.createTable('ProfileIndustries',{
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        ProfileId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Profiles", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        IndustryId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Industries", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    });
  },

  down: function (queryInterface) {
    return Promise.all([
      queryInterface.dropTable('ProfileFunctions'),
      queryInterface.dropTable('IndustryFunctions'),
      queryInterface.dropTable('ProfileIndustries'),
      queryInterface.dropTable('Industries')
    ]);
  }
};
