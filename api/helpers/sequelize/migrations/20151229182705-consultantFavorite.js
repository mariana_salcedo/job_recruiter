'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('ConsultantFavorites', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      AccountUserProfileId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "AccountUserProfiles", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      },
      ProfileId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "Profiles", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('ConsultantFavorites');
  }
};
