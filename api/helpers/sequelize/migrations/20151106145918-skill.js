'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {

    return queryInterface.createTable('Skills',{
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      skillName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      skillStatus: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      skillDescription: {
        type: Sequelize.STRING
      }
    }).then(function createProfileSkillTable() {
      return queryInterface.createTable('ProfileSkills',{
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        ProfileId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Profiles", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        SkillId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Skills", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    });
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('ProfileSkills').then(function deleteSkill() {
      return queryInterface.dropTable('Skills');
    });
  }
};
