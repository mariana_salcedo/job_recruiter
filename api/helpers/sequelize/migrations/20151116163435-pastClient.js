'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('PastClients', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      pastClientName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      FileId: {
        type: Sequelize.INTEGER,
        references: { model: "Files", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict',
        allowNull: true
      },
      ProfileId: {
        type: Sequelize.INTEGER,
        references: { model: "Profiles", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('PastClients');

  }
};
