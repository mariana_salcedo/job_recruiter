'use strict';

var _ = require('lodash');

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.createTable('Users', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        firstName: {
          type: Sequelize.STRING,
          allowNull: false
        },
        lastName: {
          type: Sequelize.STRING,
          allowNull: false
        },
        email: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        },
        password: {
          type: Sequelize.STRING,
          allowNull: false
        },
        welcomeMessage: {
          type: Sequelize.BOOLEAN,
          allowNull: true
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      queryInterface.createTable('Languages', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        languageCode: {
          allowNull: false,
          type: Sequelize.STRING
        },
        languageName: {
          allowNull: false,
          type: Sequelize.STRING
        },
        languageStatus: {
          allowNull: false,
          defaultValue: true,
          type: Sequelize.BOOLEAN
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      queryInterface.createTable('Roles', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        roleName:  {
          allowNull: false,
          type: Sequelize.STRING
        },
        roleDescription: {
          allowNull: true,
          type: Sequelize.STRING
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      })
    ])
    .then(function createUserRoles() {
      return queryInterface.createTable('UserRoles', {
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        UserId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Users", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        RoleId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Roles", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    })
    .then(function populateTables() {

      var languages = [
        {
          languageCode: "ab",
          languageName: "Abkhaz"
        },{
          languageCode: "aa",
          languageName: "Afar"
        },{
          languageCode: "af",
          languageName: "Afrikaans"
        },{
          languageCode: "ak",
          languageName: "Akan"
        },{
          languageCode: "sq",
          languageName: "Albanian"
        },{
          languageCode: "am",
          languageName: "Amharic"
        },{
          languageCode: "ar",
          languageName: "Arabic"
        },{
          languageCode: "an",
          languageName: "Aragonese"
        },{
          languageCode: "hy",
          languageName: "Armenian"
        },{
          languageCode: "as",
          languageName: "Assamese"
        },{
          languageCode: "av",
          languageName: "Avaric"
        },{
          languageCode: "ae",
          languageName: "Avestan"
        },{
          languageCode: "ay",
          languageName: "Aymara"
        },{
          languageCode: "az",
          languageName: "Azerbaijani"
        },{
          languageCode: "bm",
          languageName: "Bambara"
        },{
          languageCode: "ba",
          languageName: "Bashkir"
        },{
          languageCode: "eu",
          languageName: "Basque"
        },{
          languageCode: "be",
          languageName: "Belarusian"
        },{
          languageCode: "bn",
          languageName: "Bengali"
        },{
          languageCode: "bh",
          languageName: "Bihari"
        },{
          languageCode: "bi",
          languageName: "Bislama"
        },{
          languageCode: "bs",
          languageName: "Bosnian"
        },{
          languageCode: "br",
          languageName: "Breton"
        },{
          languageCode: "bg",
          languageName: "Bulgarian"
        },{
          languageCode: "my",
          languageName: "Burmese"
        },{
          languageCode: "ca",
          languageName: "Catalan; Valencian"
        },{
          languageCode: "ch",
          languageName: "Chamorro"
        },{
          languageCode: "ce",
          languageName: "Chechen"
        },{
          languageCode: "ny",
          languageName: "Chichewa; Chewa; Nyanja"
        },{
          languageCode: "zh",
          languageName: "Chinese"
        },{
          languageCode: "cv",
          languageName: "Chuvash"
        },{
          languageCode: "kw",
          languageName: "Cornish"
        },{
          languageCode: "co",
          languageName: "Corsican"
        },{
          languageCode: "cr",
          languageName: "Cree"
        },{
          languageCode: "hr",
          languageName: "Croatian"
        },{
          languageCode: "cs",
          languageName: "Czech"
        },{
          languageCode: "da",
          languageName: "Danish"
        },{
          languageCode: "dv",
          languageName: "Divehi; Dhivehi; Maldivian;"
        },{
          languageCode: "nl",
          languageName: "Dutch"
        },{
          languageCode: "en",
          languageName: "English"
        },{
          languageCode: "eo",
          languageName: "Esperanto"
        },{
          languageCode: "et",
          languageName: "Estonian"
        },{
          languageCode: "ee",
          languageName: "Ewe"
        },{
          languageCode: "fo",
          languageName: "Faroese"
        },{
          languageCode: "fj",
          languageName: "Fijian"
        },{
          languageCode: "fi",
          languageName: "Finnish"
        },{
          languageCode: "fr",
          languageName: "French"
        },{
          languageCode: "ff",
          languageName: "Fula; Fulah; Pulaar; Pular"
        },{
          languageCode: "gl",
          languageName: "Galician"
        },{
          languageCode: "ka",
          languageName: "Georgian"
        },{
          languageCode: "de",
          languageName: "German"
        },{
          languageCode: "el",
          languageName: "Greek, Modern"
        },{
          languageCode: "gn",
          languageName: "Guaraní"
        },{
          languageCode: "gu",
          languageName: "Gujarati"
        },{
          languageCode: "ht",
          languageName: "Haitian; Haitian Creole"
        },{
          languageCode: "ha",
          languageName: "Hausa"
        },{
          languageCode: "he",
          languageName: "Hebrew (modern)"
        },{
          languageCode: "hz",
          languageName: "Herero"
        },{
          languageCode: "hi",
          languageName: "Hindi"
        },{
          languageCode: "ho",
          languageName: "Hiri Motu"
        },{
          languageCode: "hu",
          languageName: "Hungarian"
        },{
          languageCode: "ia",
          languageName: "Interlingua"
        },{
          languageCode: "id",
          languageName: "Indonesian"
        },{
          languageCode: "ie",
          languageName: "Interlingue"
        },{
          languageCode: "ga",
          languageName: "Irish"
        },{
          languageCode: "ig",
          languageName: "Igbo"
        },{
          languageCode: "ik",
          languageName: "Inupiaq"
        },{
          languageCode: "io",
          languageName: "Ido"
        },{
          languageCode: "is",
          languageName: "Icelandic"
        },{
          languageCode: "it",
          languageName: "Italian"
        },{
          languageCode: "iu",
          languageName: "Inuktitut"
        },{
          languageCode: "ja",
          languageName: "Japanese"
        },{
          languageCode: "jv",
          languageName: "Javanese"
        },{
          languageCode: "kl",
          languageName: "Kalaallisut, Greenlandic"
        },{
          languageCode: "kn",
          languageName: "Kannada"
        },{
          languageCode: "kr",
          languageName: "Kanuri"
        },{
          languageCode: "ks",
          languageName: "Kashmiri"
        },{
          languageCode: "kk",
          languageName: "Kazakh"
        },{
          languageCode: "km",
          languageName: "Khmer"
        },{
          languageCode: "ki",
          languageName: "Kikuyu, Gikuyu"
        },{
          languageCode: "rw",
          languageName: "Kinyarwanda"
        },{
          languageCode: "ky",
          languageName: "Kirghiz, Kyrgyz"
        },{
          languageCode: "kv",
          languageName: "Komi"
        },{
          languageCode: "kg",
          languageName: "Kongo"
        },{
          languageCode: "ko",
          languageName: "Korean"
        },{
          languageCode: "ku",
          languageName: "Kurdish"
        },{
          languageCode: "kj",
          languageName: "Kwanyama, Kuanyama"
        },{
          languageCode: "la",
          languageName: "Latin"
        },{
          languageCode: "lb",
          languageName: "Luxembourgish, Letzeburgesch"
        },{
          languageCode: "lg",
          languageName: "Luganda"
        },{
          languageCode: "li",
          languageName: "Limburgish, Limburgan, Limburger"
        },{
          languageCode: "ln",
          languageName: "Lingala"
        },{
          languageCode: "lo",
          languageName: "Lao"
        },{
          languageCode: "lt",
          languageName: "Lithuanian"
        },{
          languageCode: "lu",
          languageName: "Luba-Katanga"
        },{
          languageCode: "lv",
          languageName: "Latvian"
        },{
          languageCode: "gv",
          languageName: "Manx"
        },{
          languageCode: "mk",
          languageName: "Macedonian"
        },{
          languageCode: "mg",
          languageName: "Malagasy"
        },{
          languageCode: "ms",
          languageName: "Malay"
        },{
          languageCode: "ml",
          languageName: "Malayalam"
        },{
          languageCode: "mt",
          languageName: "Maltese"
        },{
          languageCode: "mi",
          languageName: "Māori"
        },{
          languageCode: "mr",
          languageName: "Marathi (Marāṭhī)"
        },{
          languageCode: "mh",
          languageName: "Marshallese"
        },{
          languageCode: "mn",
          languageName: "Mongolian"
        },{
          languageCode: "na",
          languageName: "Nauru"
        },{
          languageCode: "nv",
          languageName: "Navajo, Navaho"
        },{
          languageCode: "nb",
          languageName: "Norwegian Bokmål"
        },{
          languageCode: "nd",
          languageName: "North Ndebele"
        },{
          languageCode: "ne",
          languageName: "Nepali"
        },{
          languageCode: "ng",
          languageName: "Ndonga"
        },{
          languageCode: "nn",
          languageName: "Norwegian Nynorsk"
        },{
          languageCode: "no",
          languageName: "Norwegian"
        },{
          languageCode: "ii",
          languageName: "Nuosu"
        },{
          languageCode: "nr",
          languageName: "South Ndebele"
        },{
          languageCode: "oc",
          languageName: "Occitan"
        },{
          languageCode: "oj",
          languageName: "Ojibwe, Ojibwa"
        },{
          languageCode: "cu",
          languageName: "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic"
        },{
          languageCode: "om",
          languageName: "Oromo"
        },{
          languageCode: "or",
          languageName: "Oriya"
        },{
          languageCode: "os",
          languageName: "Ossetian, Ossetic"
        },{
          languageCode: "pa",
          languageName: "Panjabi, Punjabi"
        },{
          languageCode: "pi",
          languageName: "Pāli"
        },{
          languageCode: "fa",
          languageName: "Persian"
        },{
          languageCode: "pl",
          languageName: "Polish"
        },{
          languageCode: "ps",
          languageName: "Pashto, Pushto"
        },{
          languageCode: "pt",
          languageName: "Portuguese"
        },{
          languageCode: "qu",
          languageName: "Quechua"
        },{
          languageCode: "rm",
          languageName: "Romansh"
        },{
          languageCode: "rn",
          languageName: "Kirundi"
        },{
          languageCode: "ro",
          languageName: "Romanian, Moldavian, Moldovan"
        },{
          languageCode: "ru",
          languageName: "Russian"
        },{
          languageCode: "sa",
          languageName: "Sanskrit (Saṁskṛta)"
        },{
          languageCode: "sc",
          languageName: "Sardinian"
        },{
          languageCode: "sd",
          languageName: "Sindhi"
        },{
          languageCode: "se",
          languageName: "Northern Sami"
        },{
          languageCode: "sm",
          languageName: "Samoan"
        },{
          languageCode: "sg",
          languageName: "Sango"
        },{
          languageCode: "sr",
          languageName: "Serbian"
        },{
          languageCode: "gd",
          languageName: "Scottish Gaelic; Gaelic"
        },{
          languageCode: "sn",
          languageName: "Shona"
        },{
          languageCode: "si",
          languageName: "Sinhala, Sinhalese"
        },{
          languageCode: "sk",
          languageName: "Slovak"
        },{
          languageCode: "sl",
          languageName: "Slovene"
        },{
          languageCode: "so",
          languageName: "Somali"
        },{
          languageCode: "st",
          languageName: "Southern Sotho"
        },{
          languageCode: "es",
          languageName: "Spanish; Castilian"
        },{
          languageCode: "su",
          languageName: "Sundanese"
        },{
          languageCode: "sw",
          languageName: "Swahili"
        },{
          languageCode: "ss",
          languageName: "Swati"
        },{
          languageCode: "sv",
          languageName: "Swedish"
        },{
          languageCode: "ta",
          languageName: "Tamil"
        },{
          languageCode: "te",
          languageName: "Telugu"
        },{
          languageCode: "tg",
          languageName: "Tajik"
        },{
          languageCode: "th",
          languageName: "Thai"
        },{
          languageCode: "ti",
          languageName: "Tigrinya"
        },{
          languageCode: "bo",
          languageName: "Tibetan Standard, Tibetan, Central"
        },{
          languageCode: "tk",
          languageName: "Turkmen"
        },{
          languageCode: "tl",
          languageName: "Tagalog"
        },{
          languageCode: "tn",
          languageName: "Tswana"
        },{
          languageCode: "to",
          languageName: "Tonga (Tonga Islands)"
        },{
          languageCode: "tr",
          languageName: "Turkish"
        },{
          languageCode: "ts",
          languageName: "Tsonga"
        },{
          languageCode: "tt",
          languageName: "Tatar"
        },{
          languageCode: "tw",
          languageName: "Twi"
        },{
          languageCode: "ty",
          languageName: "Tahitian"
        },{
          languageCode: "ug",
          languageName: "Uighur, Uyghur"
        },{
          languageCode: "uk",
          languageName: "Ukrainian"
        },{
          languageCode: "ur",
          languageName: "Urdu"
        },{
          languageCode: "uz",
          languageName: "Uzbek"
        },{
          languageCode: "ve",
          languageName: "Venda"
        },{
          languageCode: "vi",
          languageName: "Vietnamese"
        },{
          languageCode: "vo",
          languageName: "Volapük"
        },{
          languageCode: "wa",
          languageName: "Walloon"
        },{
          languageCode: "cy",
          languageName: "Welsh"
        },{
          languageCode: "wo",
          languageName: "Wolof"
        },{
          languageCode: "fy",
          languageName: "Western Frisian"
        },{
          languageCode: "xh",
          languageName: "Xhosa"
        },{
          languageCode: "yi",
          languageName: "Yiddish"
        },{
          languageCode: "yo",
          languageName: "Yoruba"
        },{
          languageCode: "za",
          languageName: "Zhuang, Chuang"
        }
      ];

      var roles = [
        {
          roleName: "firm"
        },
        {
          roleName: "super"
        },
        {
          roleName: "client"
        },
        {
          roleName: "register"
        },
        {
          roleName: "consultant"
        }
      ];

      var users = [
        {
          firstName: "Gustavo",
          lastName: "Laguna",
          email: "glaguna@gmail.com",
          password: "$2a$10$2KloOSgrA5C2UM8M9yHca.4kfzXvVIqurTofdTj6J3M4WfHnjzvHC"
        }
      ];

      var add = {
        createdAt: queryInterface.sequelize.fn('NOW'),
        updatedAt: queryInterface.sequelize.fn('NOW')
      };

      function addTimeStamps(elems) {
        _.map(elems, function(language) {
          _.extend(language, add);
        });
      }

      addTimeStamps(languages);
      addTimeStamps(roles);
      addTimeStamps(users);

      return Promise.all([
        queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('Languages', languages)),
        queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('Users', users)),
        queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('Roles', roles))
      ]);
    });
  },

  down: function migrationDown(queryInterface) {
    return Promise.all([
      queryInterface.dropTable('Users'),
      queryInterface.dropTable('Languages')
    ]);
  }
};
