'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.changeColumn('Invitations','referalBy',{
      type: Sequelize.STRING,
      allowNull: true
    }).then(function() {
      return queryInterface.changeColumn('Invitations','referalEmail',{
        allowNull: true,
        type: Sequelize.STRING
      });
    });
  },

  down: function() {
  }
};
