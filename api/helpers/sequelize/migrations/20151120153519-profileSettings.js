'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('ProfileSettings', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      aboutMe: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      publishPress: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      profileLink:  {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING
      },
      serviceDescription:  {
        type: Sequelize.TEXT
      },
      ProfileId: {
        type: Sequelize.INTEGER,
        references: { model: "Profiles", key: "id" },
        unique: true,
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });

  },
  down: function(queryInterface) {
    return queryInterface.dropTable('ProfileSettings');
  }
};
