'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.removeColumn('Profiles', 'travel').then(function() {
      return queryInterface.addColumn(
        'Profiles',
        'travel',
        {
        type: Sequelize.ENUM('Local or Remote Only', '25%', '50%', '75%', 'Relocate'),
        defaultValue: 'Local or Remote Only',
        allowNull: true
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
