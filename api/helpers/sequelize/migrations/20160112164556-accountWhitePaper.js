'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('WhitePapers', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      requireContactInfo: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      FileId: {
        allowNull: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Files", key: "id" }
      }
    }).then(function createAccountWhitePaper() {
      return queryInterface.createTable('AccountWhitePapers', {
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        AccountId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Accounts", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        WhitePaperId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "WhitePapers", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('AccountWhitePapers')
      .then(function deleteAccountWhitePaper() {
        return queryInterface.dropTable('WhitePapers');
      });

  }
};
