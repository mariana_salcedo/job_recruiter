'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.removeColumn('References','email')
      .then(function() {
        return queryInterface.addColumn('References','email',{
          allowNull: false,
          type: Sequelize.STRING
        });
      })
      .then(function changeCommunicationMethodRequired() {
        return queryInterface.changeColumn('References','communitacionMethod',{
          allowNull: true,
          type: Sequelize.ENUM("Email", "Phone", "No preference"),
          defaultValue: "Email"
        });
      })
      .then(function() {
        return queryInterface.addColumn('References','expectationAccomplished',{
          type: Sequelize.BOOLEAN,
          allowNull: true
        });
      })
      .then(function() {
        return queryInterface.addColumn(
          'References',
          'consultantEffective',
          {
            type: Sequelize.BOOLEAN,
            allowNull: true
          });
      })
      .then(function() {
        return queryInterface.addColumn(
          'References',
          'hireAgain',
          {
            type: Sequelize.BOOLEAN,
            allowNull: true
          });
      })
      .then(function() {
        return queryInterface.addColumn(
          'References',
          'questions',
          {
            type: Sequelize.TEXT,
            allowNull: true
          });
      });
  },

  down: function(queryInterface, Sequelize) {
  return queryInterface.changeColumn('References','communitacionMethod',{
      allowNull: false,
      type: Sequelize.ENUM("Email", "Phone", "No preference")
    })
    .then(function changeReferenceEmailRequired() {
      return queryInterface.removeColumn('References','email')
        .then(function(){
          return queryInterface.addColumn('References','email',{
            allowNull: true,
            type: Sequelize.STRING
          });
        })
    })
    .then(function() {
      return queryInterface.removeColumn('References', 'expectationAccomplished');
    })
    .then(function() {
      return queryInterface.removeColumn('References', 'consultantEffective');
    })
    .then(function() {
      return queryInterface.removeColumn('References', 'hireAgain');
    })
    .then(function() {
      return queryInterface.removeColumn('References', 'questions');
    });
  }
};
