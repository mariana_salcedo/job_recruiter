'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('AccountWebsiteSettings', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      serviceHeader: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: 'Services'
      },
      experienceHeader: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: 'Experience'
      },
      pressHeader: {
        allowNull: false,
        defaultValue: 'Press',
        type: Sequelize.STRING
      },
      contactPreference: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: 'Contact'
      },
      AccountId: {
        allowNull: false,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Accounts", key: "id" }
      }
    });
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('AccountWebsiteSettings');
  }
};
