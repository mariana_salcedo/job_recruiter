'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn('Rfps', 'location', {
      allowNull: false,
      type: Sequelize.STRING
    });
  },

  down: function(queryInterface) {
    return queryInterface.addColumn('Rfps', 'location');
  }
};
