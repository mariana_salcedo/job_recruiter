'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('Projects', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      budget: {
        type: Sequelize.FLOAT
      },
      startDate: {
        type: Sequelize.DATE
      },
      duration: {
        type: Sequelize.INTEGER
      },
      ProposalId: {
        type: Sequelize.INTEGER,
        references: { model: "Proposals", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict',
        allowNull: true
      },
      AccountId: {
        allowNull: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Accounts", key: "id" }
      },
      RfpId: {
        allowNull: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Rfps", key: "id" }
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('Projects');
  }
};
