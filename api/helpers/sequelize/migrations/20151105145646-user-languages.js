'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('UserLanguages', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      UserId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "Users", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      },
      LanguageId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "Languages", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('UserLanguages');
  }
};
