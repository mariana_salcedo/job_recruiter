'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Rfps', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      summary: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      minPrice: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      maxPrice: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      duration: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      private: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      status: {
        allowNull: false,
        defaultValue: 'Created',
        type: Sequelize.ENUM('Pending Payment','Published','Created','Disable','Awarded')
      },
      scopeDescription: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      deliverables: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      tools: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      requirements: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      paymentTerms: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      contactPerson: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      other: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      AccountId: {
        type: Sequelize.INTEGER,
        references: { model: "Accounts", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict',
        allowNull: false
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('Rfps');

  }
};
