'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('Countries', 'order', {
      type: Sequelize.INTEGER,
      defaultValue: 5,
      allowNull: false
    }).then(function(){
      return Promise.all([
        queryInterface.sequelize.query(queryInterface.QueryGenerator.updateQuery('Countries', {order:1}, {id:231})),
        queryInterface.sequelize.query(queryInterface.QueryGenerator.updateQuery('Countries', {order:2}, {id:38})),
        queryInterface.sequelize.query(queryInterface.QueryGenerator.updateQuery('Countries', {order:3}, {id:230})),
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
