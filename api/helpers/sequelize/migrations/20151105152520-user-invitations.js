'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('Invitations', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      firstName:  {
        allowNull: false,
        type: Sequelize.STRING
      },
      lastName:  {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      accessToken: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      refreshToken: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      referalBy:  {
        allowNull: true,
        type: Sequelize.STRING
      },
      referalEmail: {
        allowNull: true,
        type: Sequelize.STRING
      },
      message: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      lastRemind: {
        type: Sequelize.DATE
      },
      reminds: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      type: {
        allowNull: false,
        type: Sequelize.ENUM('Client', 'Consultant')
      },
      status: {
        type: Sequelize.ENUM('Unread', 'Read', 'Deleted', 'Accepted'),
        defaultValue: 'Unread'
      },
      UserId: {
        type: Sequelize.INTEGER,
        references: { model: "Users", key: "id" },
        onUpdate: 'cascade',
        allowNull: false,
        onDelete: 'restrict'
      }});
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('Invitations');
  }
};
