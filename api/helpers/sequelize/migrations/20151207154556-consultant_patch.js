'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('Profiles', 'status').then(function(){
      return queryInterface.addColumn('Profiles', 'status', {
        type: Sequelize.ENUM('Accepted', 'Active', 'Deleted', 'Pending'),
        defaultValue: 'Accepted',
        allowNull: false
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
