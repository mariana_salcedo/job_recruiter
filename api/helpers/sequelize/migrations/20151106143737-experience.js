'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Experiences', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      experienceCompany: {
        allowNull: false,
        type: Sequelize.STRING
      },
      experienceTitle: {
        allowNull: false,
        type: Sequelize.STRING
      },
      experienceDescription: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      experienceShow: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      experienceStart: {
        allowNull: false,
        type: Sequelize.DATE
      },
      experienceEnd: Sequelize.DATE,
      ProfileId: {
        type: Sequelize.INTEGER,
        references: { model: "Profiles", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('Experiences');

  }
};
