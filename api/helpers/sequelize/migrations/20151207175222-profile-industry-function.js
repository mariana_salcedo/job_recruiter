'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'Profiles',
      'primaryFunction',
      Sequelize.STRING);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('Profiles', 'primaryFunction');
  }
};
