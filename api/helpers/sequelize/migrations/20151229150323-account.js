'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Prospects')
      .then(function createClientTable() {
        return queryInterface.createTable('Accounts', {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
          },
          createdAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          updatedAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          deletedAt: {
            allowNull: true,
            type: Sequelize.DATE
          },
          firstName: {
            allowNull: false,
            type: Sequelize.STRING
          },
          lastName: {
            allowNull: false,
            type: Sequelize.STRING
          },
          companyName: {
            allowNull: true,
            type: Sequelize.STRING
          },
          website: {
            allowNull: false,
            type: Sequelize.STRING
          },
          phoneNumber: {
            allowNull: true,
            type: Sequelize.STRING
          },
          message: {
            allowNull: true,
            type: Sequelize.STRING
          },
          email: {
            unique: true,
            allowNull: false,
            type: Sequelize.STRING

          },
          type: {
            allowNull: false,
            type: Sequelize.ENUM('Client','Firm')
          },
          status: {
            allowNull: false,
            type: Sequelize.ENUM('Prospect','Approved','Suspended','Rejected')
          },
          FileId: {
            type: Sequelize.INTEGER,
            references: { model: "Files", key: "id" },
            onUpdate: 'cascade',
            onDelete: 'restrict',
            allowNull: true,
            defaultValue: 1
          }
        });
      });
  },

  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Accounts')
      .then(function createProspectTable() {
        return queryInterface.createTable('Prospects', {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
          },
          createdAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          updatedAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          deletedAt: {
            allowNull: true,
            type: Sequelize.DATE
          },
          firstName: {
            type: Sequelize.STRING,
            allowNull: false
          },
          lastName: {
            type: Sequelize.STRING,
            allowNull: false
          },
          companyName: {
            type: Sequelize.STRING,
            allowNull: true
          },
          email: {
            type: Sequelize.STRING,
            allowNull: false
          },
          phoneNumber: {
            type: Sequelize.STRING,
            allowNull: true
          },
          message: {
            type: Sequelize.STRING,
            allowNull: true
          }
        });
      });
  }
};
