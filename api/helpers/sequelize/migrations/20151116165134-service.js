'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Services', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      serviceName: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING
      },
      serviceStatus: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      serviceDescription: Sequelize.TEXT
    }).then(function createProfileServicesTable() {
      return queryInterface.createTable('ProfileServices', {
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        ProfileId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Profiles", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        ServiceId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Services", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('ProfileServices').then(function deleteSkill() {
      return queryInterface.dropTable('Services');
    });
  }
};
