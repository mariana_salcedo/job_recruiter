'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('AccountCaseStudies', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      AccountId: {
        allowNull: false,
        primaryKey: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        unique: 'compositeIndex',
        references: { model: "Accounts", key: "id" }
      },
      CaseStudyId: {
        allowNull: false,
        primaryKey: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        unique: 'compositeIndex',
        references: { model: "CaseStudies", key: "id" }
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('AccountCaseStudies');
  }
};
