'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('Files', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      s3Url: {
        type: Sequelize.STRING
      },
      cloudinaryUrl: {
        type: Sequelize.STRING
      },
      type: {
        allowNull: false,
        type: Sequelize.ENUM('Image', 'File')
      },
      extension: {
        allowNull: false,
        type: Sequelize.STRING
      },
      size: {
        type: Sequelize.INTEGER
      }
    }).then(function InsertDefaultImage() {
      var x = [{
          id: 1,
          createdAt: queryInterface.sequelize.fn('NOW'),
          updatedAt: queryInterface.sequelize.fn('NOW'),
          type: "Image",
          extension: "jpg",
          cloudinaryUrl: "v1448378299/l38pezyytzzkzjeszd6c.jpg"
        }];
      return queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('Files', x));
    })
    .then(function createPofileTable() {
      return queryInterface.createTable('Profiles', {
        id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        avaliability: {
          allowNull: false,
          type: Sequelize.STRING
        },
        travel: {
          type: Sequelize.ENUM('Realocate', 'NOP', 'Short Trips')
        },
        skype: {
          type: Sequelize.STRING
        },
        linkedin: {
          type: Sequelize.STRING
        },
        twitter: {
          type: Sequelize.STRING
        },
        resume: {
          type: Sequelize.STRING
        },
        status: {
          type: Sequelize.ENUM('Accepted', 'Active', 'Deleted', 'Pending'),
          defaultValue: 'Accepted',
          allowNull: false
        },
        companyName: {
          type: Sequelize.STRING
        },
        FileId: {
          type: Sequelize.INTEGER,
          references: { model: "Files", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict',
          allowNull: true,
          defaultValue: 1
        },
        UserId: {
          type: Sequelize.INTEGER,
          references: { model: "Users", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      });
    });
  },

  down: function migrationDown(queryInterface) {
    return Promise.all([
      queryInterface.dropTable('ProfileFunctions'),
      queryInterface.dropTable('Profiles'),
      queryInterface.dropTable('Files')
    ]);
  }
};