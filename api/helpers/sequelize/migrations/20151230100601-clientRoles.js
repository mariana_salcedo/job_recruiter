'use strict';

module.exports = {
  up: function(queryInterface) {
    var roles = [
      {
        roleName: "client admin",
        createdAt: queryInterface.sequelize.fn('NOW'),
        updatedAt: queryInterface.sequelize.fn('NOW')
      }
    ];
    return queryInterface.sequelize.query(queryInterface.QueryGenerator.bulkInsertQuery('Roles', roles));
  },

  down: function() {

  }
};
