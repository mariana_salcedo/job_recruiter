'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'References',
      'timeToCall',
      {
        type: Sequelize.ENUM("Morning", "Afternoon", "Evening"),
        allowNull: true
      }
    )
    .then(function() {
      return queryInterface.addColumn(
        'References',
        'timezone',
        {
          type: Sequelize.TEXT,
          allowNull: true
        });
    });
  },

  down: function(queryInterface) {
    return queryInterface.removeColumn(
      'References',
      'timeToCall'
    )
    .then(function() {
      return queryInterface.addColumn(
        'References',
        'timezone'
      );
    });
  }
};
