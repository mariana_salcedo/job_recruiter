'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('Proposals', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      amount: {
        type: Sequelize.FLOAT
      },
      FileId: {
        type: Sequelize.INTEGER,
        references: { model: "Files", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict',
        allowNull: true
      },
      RfpInterestedId: {
        type: Sequelize.INTEGER,
        references: { model: "RfpInteresteds", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict',
        allowNull: false
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('Proposals');
  }
};
