'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('CompanyHighlight', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      AccountId: {
        allowNull: false,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Accounts", key: "id" }
      }
    });
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('CompanyHighlight');
  }
};
