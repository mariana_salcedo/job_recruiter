'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'ApplicationNotes',
      'UserId',
      {
        type: Sequelize.INTEGER,
        references: { model: "Users", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    );
  },

  down: function(queryInterface) {
    return queryInterface.removeColumn('ApplicationNotes', 'UserId');
  }
};
