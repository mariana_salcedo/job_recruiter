'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('AccountUserProfiles', {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      AccountId: {
        allowNull: false,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        unique: 'compositeIndex',
        references: { model: "Accounts", key: "id" }
      },
      UserId: {
        allowNull: false,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        unique: 'compositeIndex',
        references: { model: "Users", key: "id" }
      },
      ProfileId: {
        allowNull: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Profiles", key: "id" }
      },
      FileId: {
        defaultValue: 1,
        allowNull: true,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "Files", key: "id" }
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('AccountUserProfiles');
  }
};
