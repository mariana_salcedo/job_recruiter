'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('AccountUserNotes', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      AccountUserProfileId: {
        allowNull: false,
        onUpdate: 'cascade',
        onDelete: 'restrict',
        type: Sequelize.INTEGER,
        references: { model: "AccountUserProfiles", key: "id" }
      }
    });
  },

  down: function(queryInterface) {
    return queryInterface.dropTable('AccountUserNotes');
  }
};
