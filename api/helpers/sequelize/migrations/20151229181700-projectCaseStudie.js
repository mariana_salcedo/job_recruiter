'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {

    return queryInterface.createTable('ProjectCaseStudies', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ProjectId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: 'compositeIndex',
        references: { model: "Projects", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      },
      CaseStudyId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: 'compositeIndex',
        references: { model: "Profiles", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    });
  },

  down: function migrationDown(queryInterface) {
    return queryInterface.dropTable('ProjectCaseStudies');
  }
};
