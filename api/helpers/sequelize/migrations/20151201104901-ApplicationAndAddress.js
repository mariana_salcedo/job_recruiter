'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return Promise.all([queryInterface.addColumn(
      'Applications',
      'writingSample',
      {
        type: Sequelize.TEXT
      }
    ),queryInterface.addColumn(
      'Addresses',
      'phoneNumberType',
      {
        allowNull: true,
        type: Sequelize.ENUM('Mobile', 'Home', 'Work')
      }
    )]) ;

  },
  down: function(queryInterface) {
    return Promise.all([
      queryInterface.removeColumn('Applications', 'writingSample'),
      queryInterface.removeColumn('Addresses', 'phoneNumberType')]);
  }
};
