'use strict';

module.exports = {
  up: function migrationUp(queryInterface, Sequelize) {
    return Promise.all([queryInterface.createTable('RfpIndustries', {
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      RfpId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "Rfps", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      },
      IndustryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: 'compositeIndex',
        references: { model: "Industries", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    }),
      queryInterface.createTable('RfpServices', {
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        deletedAt: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updatedBy: {
          allowNull: true,
          type: Sequelize.STRING
        },
        RfpId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Rfps", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        },
        ServiceId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          unique: 'compositeIndex',
          references: { model: "Services", key: "id" },
          onUpdate: 'cascade',
          onDelete: 'restrict'
        }
      })]);
  },

  down: function migrationDown(queryInterface) {
    return Promise.all([queryInterface.dropTable('RfpIndustries'),
                        queryInterface.dropTable('RfpServices')]);
  }
};
