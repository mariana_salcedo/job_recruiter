'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Applications', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.STRING
      },
      status: {
        allowNull: false,
        type: Sequelize.ENUM('Pending', 'Accepted', 'Rejected', 'Reviewed', 'New', 'Bench')
      },
      applicationDate: {
        allowNull: false,
        type: Sequelize.DATE
      },
      calificationDate: {
        type: Sequelize.DATE
      },
      websiteUrl: {
        type: Sequelize.STRING
      },
      referredBy: {
        type: Sequelize.STRING
      },
      UserId: {
        type: Sequelize.INTEGER,
        references: { model: "Users", key: "id" },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      }
    })
      .then(function createApplicationNotesTable() {
        return queryInterface.createTable('ApplicationNotes', {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
          },
          createdAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          updatedAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          deletedAt: {
            allowNull: true,
            type: Sequelize.DATE
          },
          updatedBy: {
            allowNull: true,
            type: Sequelize.STRING
          },
          note: {
            allowNull: true,
            type: Sequelize.TEXT
          },
          ApplicationId: {
            type: Sequelize.INTEGER,
            references: { model: "Applications", key: "id" },
            onUpdate: 'cascade',
            onDelete: 'restrict'
          }
        });
      })
      .then(function createApplicationReferenceTable() {
        return queryInterface.createTable('References', {
          id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
          },
          createdAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          updatedAt: {
            type: Sequelize.DATE,
            allowNull: false
          },
          deletedAt: {
            allowNull: true,
            type: Sequelize.DATE
          },
          updatedBy: {
            allowNull: true,
            type: Sequelize.STRING
          },
          firstName: {
            allowNull: false,
            type: Sequelize.STRING
          },
          lastName: {
            allowNull: false,
            type: Sequelize.STRING
          },
          phone: {
            type: Sequelize.STRING
          },
          email: {
            type: Sequelize.STRING
          },
          response: {
            type: Sequelize.TEXT
          },
          message: {
            type: Sequelize.TEXT
          },
          relation: {
            allowNull: false,
            type: Sequelize.ENUM("Client", "Manager", "Colleague")
          },
          communitacionMethod: {
            allowNull: false,
            type: Sequelize.ENUM("Email", "Phone", "No preference")
          },
          status: {
            allowNull: false,
            type: Sequelize.ENUM("Pending", "Read", "Responded")
          },
          ApplicationId: {
            type: Sequelize.INTEGER,
            references: { model: "Applications", key: "id" },
            onUpdate: 'cascade',
            onDelete: 'restrict'
          }
        });
      });
  },
  down: function(queryInterface) {
    return queryInterface.dropTable('ApplicationNotes')
      .then(function deleteReferenceTable() {
        return queryInterface.dropTable('References');
      }).then(function deleteApplicationTable() {
        return queryInterface.dropTable('Applications');
      });
  }
};
