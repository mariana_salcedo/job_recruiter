'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Suggestion', {
    content:  {
      allowNull: false,
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Suggestion.belongsTo(models.User);
        models.Suggestion.belongsToMany(models.User, {through: models.SuggestionLike});
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};