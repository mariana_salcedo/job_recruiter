'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Address', {
    addressLineOne: {
      allowNull: false,
      type: DataTypes.STRING
    },
    addressLineTwo: {
      allowNull: true,
      type: DataTypes.STRING
    },
    addressLineThree: {
      allowNull: true,
      type: DataTypes.STRING
    },
    zipCode: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    phoneNumber: {
      allowNull: true,
      type: DataTypes.STRING
    },
    phoneNumberType: {
      allowNull: true,
      type: DataTypes.ENUM('Mobile', 'Home', 'Work')
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Address.belongsTo(models.City);
        models.Address.belongsToMany(models.User, {through: models.UserAddress});
        models.Address.belongsToMany(models.Account, {through: models.AccountAddresses});
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};