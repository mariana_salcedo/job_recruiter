'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ApplicationNote', {
    note: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ApplicationNote.belongsTo(models.User);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};