'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Skill', {
    skillName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    skillStatus: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    skillDescription: {
      type: DataTypes.STRING
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Skill.belongsToMany(models.Profile, { through: models.ProfileSkill });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};