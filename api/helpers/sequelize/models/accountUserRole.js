'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountUserRole', {
    AccountUserProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    RoleId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountUserRole.belongsTo(models.Role);
        models.AccountUserRole.belongsTo(models.AccountUserProfile);

      }
    },
    timestamps: true
  });
  return model;
};