'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Invitation', {
    firstName:  {
      allowNull: false,
      type: DataTypes.STRING
    },
    lastName:  {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    referalBy:  {
      allowNull: true,
      type: DataTypes.STRING
    },
    referalEmail: {
      allowNull: true,
      type: DataTypes.STRING
    },
    message: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    accessToken: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    refreshToken: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    lastRemind: {
      type: DataTypes.DATE
    },
    reminds: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM('Client', 'Consultant')
    },
    status: {
      type: DataTypes.ENUM('Unread', 'Read', 'Deleted', 'Accepted'),
      defaultValue: 'Unread'
    }
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};