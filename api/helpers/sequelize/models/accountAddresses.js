'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountAddresses', {
    AccountId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AddressId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountAddresses.belongsTo(models.Account);
        models.AccountAddresses.belongsTo(models.Address);
      }},
    timestamps: true,
    paranoid: true
  });
  return model;
};