'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Message', {
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};