'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProfileIndustries', {
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    IndustryId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProfileIndustries.belongsTo(models.Profile);
        models.ProfileIndustries.belongsTo(models.Industry);
      }
    },
    timestamps: true
  });
  return model;
};