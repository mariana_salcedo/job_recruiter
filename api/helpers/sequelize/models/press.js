'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Press', {
    pressName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    pressFileUrl: {
      allowNull: false,
      type: DataTypes.STRING
    },
    pressDate: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Press.belongsTo(models.File);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};