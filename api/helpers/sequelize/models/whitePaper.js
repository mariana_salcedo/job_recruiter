'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('WhitePaper', {
    title: {
      allowNull: false,
      type: DataTypes.STRING
    },
    description: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    requireContactInfo: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.WhitePaper.belongsTo(models.File);
        models.WhitePaper.belongsToMany(models.Profile, { through: 'UserWhitePapers' });
        models.WhitePaper.belongsToMany(models.Industry, { through: 'WhitePaperIndustries' });
        models.WhitePaper.belongsToMany(models.Account, { through: models.AccountWhitePapers });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};