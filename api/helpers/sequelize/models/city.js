'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('City', {
    cityName: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.City.hasMany(models.Address);
        models.City.belongsTo(models.State);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};