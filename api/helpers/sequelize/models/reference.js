'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Reference', {
    firstName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    lastName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    response: {
      type: DataTypes.TEXT
    },
    questions: {
      type: DataTypes.TEXT
    },
    message: {
      type: DataTypes.TEXT
    },
    expectationAccomplished: {
      type: DataTypes.BOOLEAN
    },
    consultantEffective: {
      type: DataTypes.BOOLEAN
    },
    hireAgain: {
      type: DataTypes.BOOLEAN
    },
    relation: {
      allowNull: false,
      type: DataTypes.ENUM("Client", "Manager", "Colleague")
    },
    timeToCall: {
      allowNull: true,
      type: DataTypes.ENUM("Morning", "Afternoon", "Evening")
    },
    timezone: {
      allowNull: true,
      type: DataTypes.STRING
    },
    communitacionMethod: {
      defaultValue: "Email",
      allowNull: true,
      type: DataTypes.ENUM("Email", "Phone", "No preference")
    },
    status: {
      allowNull: false,
      type: DataTypes.ENUM("Pending", "Read", "Responded")
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Reference.belongsTo(models.Application);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};