'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('CompanyHighlight', {
    name: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods:  {
      associate: function(models) {
        models.CompanyHighlight.belongsTo(models.Account);
      }},
    timestamps: true,
    paranoid: true
  });
  return model;
};