'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProfileServices', {
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    ServiceId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProfileServices.belongsTo(models.Profile);
        models.ProfileServices.belongsTo(models.Service);
      }
    },
    timestamps: true
  });
  return model;
};