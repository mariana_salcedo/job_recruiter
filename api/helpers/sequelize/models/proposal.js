'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Proposal', {
    description: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    amount: {
      type: DataTypes.FLOAT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Proposal.hasOne(models.Project);
        models.Proposal.belongsTo(models.File);
        models.Proposal.belongsTo(models.RfpInterested);

      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};