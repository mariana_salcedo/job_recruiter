'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Education', {
    university: {
      allowNull: false,
      type: DataTypes.STRING
    },
    degreeDesignation: {
      allowNull: false,
      type: DataTypes.STRING
    },
    degreeName: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};