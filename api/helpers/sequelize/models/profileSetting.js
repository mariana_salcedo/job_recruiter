'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProfileSetting', {
    aboutMe: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    publishPress: {
      allowNull: false,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    },
    profileLink:  {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING
    },
    serviceDescription:  {
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProfileSetting.belongsTo(models.Profile);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};