'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Task', {
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};