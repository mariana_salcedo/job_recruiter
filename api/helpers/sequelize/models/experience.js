'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Experience', {
    experienceCompany: {
      allowNull: false,
      type: DataTypes.STRING
    },
    experienceTitle: {
      allowNull: false,
      type: DataTypes.STRING
    },
    experienceDescription: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    experienceShow: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    experienceStart: {
      allowNull: false,
      type: DataTypes.DATE
    },
    experienceEnd: DataTypes.DATE
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};