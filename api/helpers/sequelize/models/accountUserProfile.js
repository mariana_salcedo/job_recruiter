'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountUserProfile', {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    UserId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AccountId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    ProfileId: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountUserProfile.belongsTo(models.File);
        models.AccountUserProfile.belongsTo(models.User);
        models.AccountUserProfile.belongsTo(models.Account);
        models.AccountUserProfile.belongsToMany(models.Role, { through: models.AccountUserRole });
        models.AccountUserProfile.belongsToMany(models.Profile, { through: models.ConsultantFavorites });

        // Firm relations
        models.AccountUserProfile.belongsTo(models.Profile);
        models.AccountUserProfile.hasMany(models.AccountUserNote);

      }
    },
    timestamps: true
  });
  return model;
};