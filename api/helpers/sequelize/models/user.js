'use strict';
var Chance = require('chance');
var chance = new Chance();

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('User', {
    firstName:  {
      allowNull: false,
      type: DataTypes.STRING
    },
    lastName:  {
      allowNull: false,
      type: DataTypes.STRING
    },
    email:  {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING
    },
    password:  {
      allowNull: false,
      type: DataTypes.STRING
    },
    welcomeMessage: {
      allowNull: true,
      type: DataTypes.BOOLEAN
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.User.hasOne(models.Settings);
        models.User.hasOne(models.Application);
        models.User.hasMany(models.Profile);
        models.User.hasMany(models.Suggestion);
        models.User.hasMany(models.Invitation);
        models.User.hasMany(models.ApplicationNote);
        models.User.belongsToMany(models.Role, { through: models.UserRole });
        models.User.belongsToMany(models.Address, {through: models.UserAddress});
        models.User.belongsToMany(models.Rfp, {through: models.RfpInterested});
        models.User.belongsToMany(models.Language, { through: models.UserLanguage });
        models.User.belongsToMany(models.Suggestion, {through: models.SuggestionLike});
        models.User.belongsToMany(models.Account, {through: models.AccountUserProfile});
      },
      mockUser: function() {
        var userData = {
          firstName: chance.first(),
          lastName: chance.last(),
          email: chance.email(),
          password: chance.zip()
        };
        return userData;
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};