'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProjectCaseStudies', {
    CaseStudyId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    ProjectId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProjectCaseStudies.belongsTo(models.Project);
        models.ProjectCaseStudies.belongsTo(models.CaseStudy);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};