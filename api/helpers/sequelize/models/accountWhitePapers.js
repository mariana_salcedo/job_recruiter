'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountWhitePapers', {
    WhitePaperId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AccountId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountWhitePapers.belongsTo(models.Account);
        models.AccountWhitePapers.belongsTo(models.WhitePaper);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};