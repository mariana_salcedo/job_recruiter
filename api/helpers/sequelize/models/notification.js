'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Notification', {
    notificationContent: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    notificationDate: {
      allowNull: false,
      type: DataTypes.DATE
    },
    isReaded: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};