/**
 * avisare
 * Created by Andres Juarez on 26/10/15.
 * email ajuarez@teravisiontech.com
 */
'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountUserNote', {
    content: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountUserNote.belongsTo(models.AccountUserProfile);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};