'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Rfp', {
    title: {
      allowNull: false,
      type: DataTypes.STRING
    },
    summary: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    minPrice: {
      allowNull: true,
      type: DataTypes.FLOAT
    },
    maxPrice: {
      allowNull: true,
      type: DataTypes.FLOAT
    },
    duration: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    private: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    status: {
      allowNull: false,
      defaultValue: 'Created',
      type: DataTypes.ENUM('Pending Payment','Published','Created','Disable','Awarded')
    },
    location: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    scopeDescription: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    deliverables: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    tools: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    requirements: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    paymentTerms: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    contactPerson: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    other: {
      allowNull: true,
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Rfp.hasOne(models.Project);
        models.Rfp.belongsTo(models.Account);
        models.Rfp.belongsToMany(models.Profile, { through: models.RfpInterested });
        models.Rfp.belongsToMany(models.Industry, { through: models.RfpIndustries });
        models.Rfp.belongsToMany(models.Service, { through: models.RfpServices });
        models.Rfp.belongsToMany(models.File, { through: models.RfpFiles });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};