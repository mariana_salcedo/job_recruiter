'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountCaseStudies', {
    CaseStudyId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AccountId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountCaseStudies.belongsTo(models.Account);
        models.AccountCaseStudies.belongsTo(models.CaseStudy);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};