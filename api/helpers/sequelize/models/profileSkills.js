'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProfileSkill', {
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    SkillId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProfileSkill.belongsTo(models.Profile);
        models.ProfileSkill.belongsTo(models.Skill);
      }
    },
    timestamps: true
  });
  return model;
};