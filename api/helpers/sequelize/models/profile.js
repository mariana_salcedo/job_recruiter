'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Profile', {
    avaliability: {
      type: DataTypes.DATE
    },
    travel: {
      type: DataTypes.ENUM('Local or Remote Only', '25%', '50%', '75%', 'Relocate')
    },
    skype: {
      type: DataTypes.STRING
    },
    linkedin: {
      type: DataTypes.STRING
    },
    twitter: {
      type: DataTypes.STRING
    },
    resume: {
      type: DataTypes.STRING
    },
    primaryFunction: {
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      defaultValue: 'Accepted',
      type: DataTypes.ENUM('Accepted', 'Active', 'Deleted', 'Pending')
    },
    companyName:  {
      type: DataTypes.STRING
    },
    FileId: {
      allowNull: true,
      type: DataTypes.INTEGER
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Profile.hasMany(models.Press);
        models.Profile.belongsTo(models.File);
        models.Profile.belongsTo(models.User);
        models.Profile.hasMany(models.Education);
        models.Profile.hasMany(models.PastClient);
        models.Profile.hasMany(models.Experience);
        models.Profile.hasOne(models.ProfileSetting);
        models.Profile.hasMany(models.Certification);
        models.Profile.hasOne(models.AccountUserProfile);
        models.Profile.belongsToMany(models.Rfp, { through: models.RfpInterested });
        models.Profile.belongsToMany(models.Project, {through: models.ProjectMember });
        models.Profile.belongsToMany(models.Skill,  { through: models.ProfileSkill });
        models.Profile.belongsToMany(models.WhitePaper, { through: 'UserWhitePapers' });
        models.Profile.belongsToMany(models.Service, { through: models.ProfileServices });
        models.Profile.belongsToMany(models.CaseStudy, { through: models.UserCaseStudies });
        models.Profile.belongsToMany(models.Industry,  { through: models.ProfileIndustries });
        models.Profile.belongsToMany(models.IndustryFunction,  { through: models.ProfileFunctions });
        models.Profile.belongsToMany(models.AccountUserProfile, { through: models.ConsultantFavorites });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};