'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('File', {
    s3Url: {
      type: DataTypes.STRING
    },
    cloudinaryUrl: {
      type: DataTypes.STRING
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM('Image', 'File')
    },
    extension: {
      allowNull: false,
      type: DataTypes.STRING
    },
    size: {
      type: DataTypes.INTEGER
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.File.hasMany(models.Account);
        models.File.hasMany(models.PastClient);
        models.File.hasMany(models.Press);
        models.File.hasMany(models.Profile);
        models.File.hasMany(models.Proposal);
        models.File.hasMany(models.WhitePaper);
        models.File.hasMany(models.AccountUserProfile);
        models.File.belongsToMany(models.Rfp, { through: models.RfpFiles});
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};