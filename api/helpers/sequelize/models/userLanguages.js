'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('UserLanguage', {
    UserId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    LanguageId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.UserLanguage.belongsTo(models.User);
        models.UserLanguage.belongsTo(models.Language);
      }
    },
    timestamps: true
  });
  return model;
};