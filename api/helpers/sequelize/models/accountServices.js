'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountServices', {
    ServiceId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AccountId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountServices.belongsTo(models.Account);
        models.AccountServices.belongsTo(models.Service);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};