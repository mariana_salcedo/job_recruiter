'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Country', {
    countryName: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Country.hasMany(models.State);

      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};