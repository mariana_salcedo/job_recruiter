'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('UserAddress', {
    UserId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AddressId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.UserAddress.belongsTo(models.User);
        models.UserAddress.belongsTo(models.Address);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};