'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Certification', {
    certificationName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    certificationType: {
      allowNull: false,
      type: DataTypes.ENUM('Course', 'Master', 'Certificate')
    },
    certificationDate: {
      allowNull: false,
      type: DataTypes.DATE
    },
    issuer: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};