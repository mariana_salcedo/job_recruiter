/**
 * avisare
 * Created by Andres Juarez on 26/10/15.
 * email ajuarez@teravisiontech.com
 */
'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Industry', {
    industryName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    industryStatus: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Industry.hasMany(models.CaseStudy);
        models.Industry.belongsToMany(models.Rfp, { through: models.RfpIndustries });
        models.Industry.belongsToMany(models.Profile, { through: 'ProfileIndustries' });
        models.Industry.belongsToMany(models.Account, { through: models.AccountIndustries });
        models.Industry.belongsToMany(models.WhitePaper, { through: 'WhitePaperIndustries' });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};