'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Application', {
    status: {
      allowNull: false,
      type: DataTypes.ENUM('Pending', 'Accepted', 'Rejected', 'Reviewed', 'New', 'Bench')
    },
    applicationDate: {
      allowNull: false,
      type: DataTypes.DATE
    },
    calificationDate: {
      type: DataTypes.DATE
    },
    websiteUrl: {
      type: DataTypes.STRING
    },
    referredBy: {
      type: DataTypes.STRING
    },
    writingSample: {
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Application.belongsTo(models.User);
        models.Application.hasMany(models.Reference);
        models.Application.hasMany(models.ApplicationNote);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};