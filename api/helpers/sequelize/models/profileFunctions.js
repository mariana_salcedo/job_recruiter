'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProfileFunctions', {
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    IndustryFunctionId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProfileFunctions.belongsTo(models.Profile);
        models.ProfileFunctions.belongsTo(models.IndustryFunction);
      }
    },
    paranoid: false,
    timestamps: true
  });
  return model;
};