'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Review', {
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};