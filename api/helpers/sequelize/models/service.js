'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Service', {
    serviceName: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING
    },
    serviceStatus: {
      allowNull: false,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    },
    serviceDescription: DataTypes.TEXT
  }, {
    classMethods:  {
      associate: function(models) {
        models.Service.belongsToMany(models.Rfp, { through: models.RfpServices });
        models.Service.belongsToMany(models.Profile, { through: models.ProfileServices });

        // Firm Relations
        models.Service.belongsToMany(models.Account, { through: models.AccountServices});

      }
    }
  });
  return model;
};