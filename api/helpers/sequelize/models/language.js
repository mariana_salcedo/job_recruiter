'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Language', {
    languageCode: {
      allowNull: false,
      type: DataTypes.STRING
    },
    languageName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    languageStatus: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};