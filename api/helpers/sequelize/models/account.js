'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Account', {
    firstName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    lastName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    companyName: {
      allowNull: true,
      type: DataTypes.STRING
    },
    website: {
      allowNull: false,
      type: DataTypes.STRING
    },
    phoneNumber: {
      allowNull: true,
      type: DataTypes.STRING
    },
    message: {
      allowNull: true,
      type: DataTypes.STRING
    },
    email: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM('Client','Firm')
    },
    status: {
      allowNull: false,
      type: DataTypes.ENUM('Prospect','Approved','Suspended','Rejected')
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Account.hasMany(models.Rfp);
        models.Account.belongsTo(models.File);
        models.Account.hasMany(models.Project);
        models.Account.belongsToMany(models.User, {through: models.AccountUserProfile});
        models.Account.belongsToMany(models.Address, {through: models.AccountAddresses});
        models.Account.belongsToMany(models.Industry, {through: models.AccountIndustries});
        models.Account.belongsToMany(models.CaseStudy, {through: models.AccountCaseStudies});

        // Firm Relation
        models.Account.hasMany(models.CompanyHighlight);
        models.Account.hasOne(models.AccountWebsiteSetting);
        models.Account.belongsToMany(models.Service, {through: models.AccountServices});
        models.Account.belongsToMany(models.WhitePaper, {through: models.AccountWhitePapers});

      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};