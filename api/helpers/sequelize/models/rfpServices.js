'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('RfpServices', {
    RfpId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    ServiceId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.RfpServices.belongsTo(models.Rfp);
        models.RfpServices.belongsTo(models.Service);
      }
    },
    timestamps: true
  });
  return model;
};