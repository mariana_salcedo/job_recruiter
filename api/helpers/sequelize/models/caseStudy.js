'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('CaseStudy', {
    caseStudyTitle: {
      allowNull: false,
      type: DataTypes.STRING
    },
    caseStudyProblem: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    caseStudySolution: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    caseStudyResult: {
      allowNull: false,
      type: DataTypes.TEXT
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.CaseStudy.belongsTo(models.Industry);
        models.CaseStudy.belongsToMany(models.Profile, { through: models.UserCaseStudies });
        models.CaseStudy.belongsToMany(models.Project, { through: models.ProjectCaseStudies });
        models.CaseStudy.belongsToMany(models.Account, { through: models.AccountCaseStudies });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};