'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('UserCaseStudies', {
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    CaseStudyId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.UserCaseStudies.belongsTo(models.Profile);
        models.UserCaseStudies.belongsTo(models.CaseStudy);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};