'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('RfpIndustries', {
    RfpId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    IndustryId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.RfpIndustries.belongsTo(models.Rfp);
        models.RfpIndustries.belongsTo(models.Industry);
      }
    },
    timestamps: true
  });
  return model;
};