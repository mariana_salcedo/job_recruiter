'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Project', {
    name: {
      type: DataTypes.STRING
    },
    budget: {
      type: DataTypes.FLOAT
    },
    startDate: {
      type: DataTypes.DATE
    },
    duration: {
      type: DataTypes.INTEGER
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Project.belongsTo(models.Rfp);
        models.Project.belongsTo(models.Account);
        models.Project.belongsTo(models.Proposal);
        models.Project.belongsToMany(models.Profile, { through: models.ProjectMember });
        models.Project.belongsToMany(models.CaseStudy, { through: models.ProjectCaseStudies });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};