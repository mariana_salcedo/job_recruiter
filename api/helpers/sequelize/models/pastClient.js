'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('PastClient', {
    pastClientName: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.PastClient.belongsTo(models.File);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};