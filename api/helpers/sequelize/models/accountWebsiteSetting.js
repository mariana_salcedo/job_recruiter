'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountWebsiteSetting', {
    serviceHeader: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'Services'
    },
    experienceHeader: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'Experience'
    },
    pressHeader: {
      allowNull: false,
      defaultValue: 'Press',
      type: DataTypes.STRING
    },
    contactPreference: {
      allowNull: false,
      type: DataTypes.STRING,
      defaultValue: 'Contact'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountWebsiteSetting.belongsTo(models.Account);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};