'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('State', {
    stateName: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.State.hasMany(models.City);
        models.State.belongsTo(models.Country);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};