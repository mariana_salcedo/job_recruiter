'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Settings', {
    allowNotifications: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    allowMessages: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};