'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('AccountIndustries', {
    IndustryId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AccountId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.AccountIndustries.belongsTo(models.Account);
        models.AccountIndustries.belongsTo(models.Industry);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};