'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Activity', {
  }, {
    classMethods: {},
    timestamps: true,
    paranoid: true
  });
  return model;
};