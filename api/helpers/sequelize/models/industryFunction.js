'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('IndustryFunction', {
    functionName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    functionDescription: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    functionStatus: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.IndustryFunction.belongsToMany(models.Profile, { through: 'ProfileFunctions' });
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};