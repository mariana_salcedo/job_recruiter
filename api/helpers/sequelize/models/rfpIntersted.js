'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('RfpInterested', {
    id: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex',
      primaryKey: true,
      autoIncrement: true
    },
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    RfpId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    status: {
      allowNull: false,
      type: DataTypes.ENUM('Shortlisted', 'Awarded','Rejected')
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.RfpInterested.hasOne(models.Proposal);
        models.RfpInterested.belongsTo(models.Rfp);
        models.RfpInterested.belongsTo(models.Profile);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};