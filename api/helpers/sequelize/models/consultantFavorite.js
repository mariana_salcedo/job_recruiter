'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ConsultantFavorites', {
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    AccountUserProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ConsultantFavorites.belongsTo(models.Profile);
        models.ConsultantFavorites.belongsTo(models.AccountUserProfile);
      }},
    timestamps: true,
    paranoid: true
  });
  return model;
};