'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ProjectMember', {
    id: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex',
      primaryKey: true,
      autoIncrement: true
    },
    ProfileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    ProjectId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.ProjectMember.belongsTo(models.Project);
        models.ProjectMember.belongsTo(models.Profile);
      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};