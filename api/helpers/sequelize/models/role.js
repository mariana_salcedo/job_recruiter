'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('Role', {
    roleName:  {
      allowNull: false,
      type: DataTypes.STRING
    },
    roleDescription: {
      allowNull: true,
      type: DataTypes.STRING
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.Role.belongsToMany(models.User, { through: models.UserRole });
        models.Role.belongsToMany(models.AccountUserProfile, { through: models.AccountUserRole });

      }
    },
    timestamps: true,
    paranoid: true
  });
  return model;
};