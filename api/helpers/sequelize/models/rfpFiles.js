'use strict';

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('RfpFiles', {
    RfpId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    FileId: {
      type: DataTypes.INTEGER,
      unique: 'compositeIndex'
    },
    type: {
      type: DataTypes.ENUM('Scope','Deliverable'),
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function(models) {
        models.RfpFiles.belongsTo(models.Rfp);
        models.RfpFiles.belongsTo(models.File);
      }
    },
    timestamps: true
  });
  return model;
};