'use strict';

var chance = new (require('chance'))();

var oauth = require('../../oauth/');
var mailer = require('../../mailer/');
var basePath = require('../../config/').basePath;

var models = require('../models.js');

var user = models("User");

var profile = models("Profile");

var CommonCRUD = require(__dirname + '/../commoms.js');

class Invitation extends CommonCRUD {
  constructor(modelName, orm) {
    super(modelName, orm);
  }

  create(data, options) {

    var password = chance.guid();

    return user.create({
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      password: password,
      roles: ['register ']
    }, options)
    .then(function addProfile(newUser) {
      return profile.create({
        UserId: newUser.id,
        status: 'Pending',
        avaliability: new Date()
      }, options).then(function returnUser() {
        return newUser;
      });
    })
    .then(function getToken(newUser) {
      return oauth.applicationToken({id: newUser.id, email: newUser.email});
    }).then(function createInvite(token) {
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers

      data.accessToken = token.access_token;

      return [data, options];
    })
    .bind(this)
    .spread(super.create)
    .then(function sendEmail(invite) {
      return mailer.sendMail(data.email, "You have been invited ", "invite", {
        fullName: data.firstName + " " +  data.lastName,
        referalName: data.referalBy,
        accessToken: encodeURIComponent(data.accessToken),
        message: data.message,
        basePath: basePath
      }).then(function createResponse() {
        return {
          id: invite.id,
          email: data.email,
          success: true
        };
      });
    });
  }
}

module.exports = Invitation;