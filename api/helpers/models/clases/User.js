'use strict';

var _ = require('lodash');
var bcrypt = require('bcrypt');
var promise = require('bluebird');

var models = require('../models.js');

var role = models("Role");

promise.promisifyAll(bcrypt);

var CommonCRUD = require(__dirname + '/../commoms.js');

class User extends CommonCRUD {
  constructor(modelName, orm) {
    super(modelName, orm);
  }

  create(data, options) {

    // Extract user roles
    var roles = data.roles;
    delete data.roles;

    // Encrypt password
    return bcrypt.hashAsync(data.password, 10)
    .bind(this)
    .then(function(hash) {
      data.password = hash;
      return [data, options];
    }).spread(super.create)

    // Link roles if they exist
    .then(function createRoles(newUser) {

      // Check if the users where send
      if (roles && roles.length > 0) {

        // Find the roles by name
        return role.findAll({
          where: {
            roleName: {
              $or: roles
            }
          }
        }, options)
        .then(function addRoles(newRoles) {

          // Add roles to the user
          return newUser.addRoles(newRoles, options)
          .then(function filterData(results) {
            newUser.dataValues.roles = _.map(results[0], function addRoleName(data, index) {
              data.dataValues.roleName = newRoles[index].get("roleName");
              return data;
            });
            return newUser;
          });
        });
      } else {
        return newUser;
      }
    });
  }
}

module.exports = User;