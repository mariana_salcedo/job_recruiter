'use strict';

class CommonCRUD {
  constructor(modelName, orm) {
    this.modelName = modelName;
    this.model = orm.models[modelName];
  }

  create(data, options) {

    return this.model.create(data, options);
  }

  delete(data, options) {

    return this.model.destroy(data, options);
  }

  bulkCreate(data, options) {

    return this.model.bulkCreate(data, options);
  }

  get(id, attributes) {

    return this.model
      .findOne({
        where: {
          id: id
        },
        attributes: attributes
      }).bind(this).then(this.checkQueryResponse);
  }

  find(data) {

    return this.model
      .findOne(data).then(function(data) {

        return data;
      });
  }

  findAll(data) {
    return this.model
      .findAll(data).then(function(data) {

        return data;
      });
  }

  findAllPaginated(data) {
    return this.model
      .findAndCountAll(data).then(function(data) {
        return data;
      });
  }

  update(id, data, options) {

    return this.get(id).bind(this)
      .then(this.checkQueryResponse)
      .then(function methodName(result) {
        return result.update(data, options).then(function(result) {
          return result;
        });
      });
  }

  build(values, options) {
    return this.model.build(values, options);
  }

  softDelete(id) {
    return this.get(id).then(function methodName(result) {
      return result.destroy({paranoid: true}).bind(this).then(this.checkQueryResponse).then(function(result) {return result;});
    });
  }

  softDeleteManyToMany(ids) {
    return this.model.findOne(ids).then(function methodName(result) {
      return result.destroy({paranoid: true}).bind(this).then(this.checkQueryResponse).then(function(result) {return result;});
    });
  }

  checkQueryResponse(data) {

    if (data === null) {

      this.error(this.modelName + " not found.", 404);
    } else {

      return data;
    }
  }

  error(message, code) {

    var error = new Error(message);

    if (!!code) {
      error.responseCode = 404;
    }

    throw error;
  }
}

module.exports = CommonCRUD;
