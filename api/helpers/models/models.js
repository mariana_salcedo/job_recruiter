'use strict';

var debug = require('debug')('custom-models');

var sequelize = require('../sequelize');
var Commons = require('./commoms.js');

module.exports = modelFactory;

function requestClass(name) {
  var rute = './clases/' + name + ".js";
  debug("Request %s class on rute %s", name, rute);
  try {
    return require(rute);
  }catch (err) {
    debug("Request of class %s fail", name);
    return null;
  }
}

function modelFactory(className) {
  var Class = requestClass(className) || Commons;
  return new Class(className, sequelize);
}