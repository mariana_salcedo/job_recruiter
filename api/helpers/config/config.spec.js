// jshint mocha:true, unused:false
'use strict';

var should = require('should');
var configLocation = __dirname + '/config.js';

var currentEnv = process.env.NODE_ENV;

describe('Configuration Module', function() {

  afterEach(function() {
    process.env.NODE_ENV = currentEnv;
    delete require.cache[require.resolve(configLocation)];
  });

  it('Should always return an object', function() {
    var config = require(configLocation);
    config.should.be.ok().and.be.an.Object().and.not.empty();
  });

  it('Should return an object base on the NODE_ENV environment variable', function() {

    process.env.NODE_ENV = currentEnv;

    var config = require(configLocation);
    config.should.be.ok().and.be.an.Object().and.not.empty();
  });

  it('Should throw error with an invalid NODE_ENV value', function() {

    process.env.NODE_ENV = "unicorn";
    (function() {
      var config = require(configLocation);
      config.should.be.ok().and.be.an.Object().and.not.empty();
    }).should.throw();

  });

  it('Should contain the following attributes mysql, redis, mailgun, cloudinary, aws', function() {

    var config = require(configLocation);
    config.should.be.ok().and.be.an.Object();
    config.should.have.properties([
      "mysql",
      "redis",
      "mailgun",
      "cloudinary",
      "aws"
    ]);

  });

  describe('Redis', function() {

    it('Should always return an object', function() {
      var config = require(configLocation).redis;
      config.should.be.ok().and.be.an.Object().and.not.empty();
    });

    it('Should contain the following attributes encryptionKey, devRequest, appRequest', function() {

      var config = require(configLocation).redis;
      config.should.be.ok().and.be.an.Object();
      config.should.have.properties([
        "encryptionKey",
        "devRequest",
        "appRequest"
      ]);

    });

  });

  describe('Mysql', function() {

    it('Should always return an object', function() {
      var config = require(configLocation).mysql;
      config.should.be.ok().and.be.an.Object().and.not.empty();
    });

    it('Should contain the following attributes database, username, password, dialect, host, port', function() {

      var config = require(configLocation).mysql;
      config.should.be.ok().and.be.an.Object();
      config.should.have.properties([
        "database",
        "username",
        "password",
        "dialect",
        "host",
        "port"
      ]);

    });

  });

  describe('Mailgun', function() {

    it('Should always return an object', function() {
      var config = require(configLocation).mailgun;
      config.should.be.ok().and.be.an.Object().and.not.empty();
    });

    it('Should contain the following attributes apiKey, domain', function() {

      var config = require(configLocation).mailgun;
      config.should.be.ok().and.be.an.Object();
      config.should.have.properties([
        "apiKey",
        "domain"
      ]);

    });

  });

  describe('Cloudinary', function() {

    it('Should always return an object', function() {
      var config = require(configLocation).cloudinary;
      config.should.be.ok().and.be.an.Object().and.not.empty();
    });

    it('Should contain the following attributes cloud_name, api_key, api_secret', function() {

      var config = require(configLocation).cloudinary;
      config.should.be.ok().and.be.an.Object();
      config.should.have.properties([
        "cloud_name",
        "api_key",
        "api_secret"
      ]);

    });

  });

  describe('AWS', function() {

    it('Should always return an object', function() {
      var config = require(configLocation).aws;
      config.should.be.ok().and.be.an.Object().and.not.empty();
    });

    describe('S3', function() {

      it('Should always return an object', function() {
        var config = require(configLocation).aws.s3;
        config.should.be.ok().and.be.an.Object().and.not.empty();
      });

      it('Should contain the following attributes user, accessKey, secretAccessKey, bucket', function() {

        var config = require(configLocation).aws.s3;
        config.should.be.ok().and.be.an.Object();
        config.should.have.properties([
          "user",
          "accessKey",
          "secretAccessKey",
          "bucket"
        ]);

      });

    });

  });

});