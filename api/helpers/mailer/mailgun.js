'use strict';

var Mailgun = require('mailgun-js');
var Bluebird = require('bluebird');
var request = require('request');
var nunjucks = require('nunjucks');
var debug = require('debug')('mailgun-js');

var config = require('../config/').mailgun;

var mailgun = new Mailgun({
  apiKey: config.apiKey,
  domain: config.domain
});

var validateOptions = {
  url: 'https://api.mailgun.net/v3/address/validate',
  method: 'GET',
  qs: {address: ''},
  encoding: 'ASCII',
  auth: {
    username: "api",
    password: config.publicKey,
  }
};

module.exports = {
  send: send,
  validate: validate,
  template: template,
  sendMail: sendMail
};

function template(name, data) {
  return nunjucks.render(__dirname + "/email/build/" + name + ".html", data);
}

function validate(email) {

  validateOptions.qs.address = email;

  debug("Validate email to %s", email);

  return new Bluebird(function validateEmailPromise(resolve, reject) {
    request(validateOptions, function validateEmail(err, result) {
      if (!!err) {

        debug("Error validating the email %s, %s ", email ,err.toString());
        reject(err);
        return;
      }

      debug("Success validating email to %s");
      resolve(JSON.parse(result.request.response.body));
    });
  });
}

function sendMail(to, subject, templateName, data, from) {
  return send(to, subject, template(templateName, data), from);
}

function send(to, subject, body, from) {

  debug("Send email to %s", to);

  from = from || config.from;

  var data = {
    to: config.redirect || to,
    html: body,
    from: from,
    subject: subject,
  };

  return new Bluebird(function sendEmailPromise(resolve, reject) {
    mailgun.messages().send(data, function(err, response) {

      if (!!err) {

        debug("Error sending email to %s, %s ", to ,err.toString());
        reject(err);
        return;
      }

      debug("Success sending email to %s", to);
      resolve(response);
    });
  });
}