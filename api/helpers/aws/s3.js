/**
 * Created by franzsilva on 26/10/15.
 */
"use strict";

var AWS = require('aws-sdk');
var promise = require('bluebird');
var debug = require('debug')("AWS-service");
var buildenv = process.env.BUILD_ENV || 'develop';
var env = process.env.NODE_ENV || 'develop';
var fs = require('fs');

var config = require('../config').aws.s3;

// Set environment variables for AWS
process.env.AWS_ACCESS_KEY_ID = config.accessKey;
process.env.AWS_SECRET_ACCESS_KEY = config.secretAccessKey;

debug("Instance S3");
// Add bucket
var s3 = new AWS.S3({
  params: { Bucket: config.bucket }
});

promise.promisifyAll(s3);

module.exports = {
  uploadFileToS3: uploadFileToS3,
  getBuckets: getBuckets,
  getEnviromentFile: getEnviromentFile,
};

function getBuckets() {

  debug("Request Buckets");

  return s3.listBucketsAsync();

}

function uploadFileToS3(name, file, mime, acl) {

  return s3.putObjectAsync({
    Bucket: 'avisare-' + env,
    ACL: acl,
    Key: name,
    Body: file,
    ContentType: mime
  });
}

function getEnviromentFile() {
  var params = {
    Bucket: 'avisare-' + buildenv,
    Key: 'config/' + buildenv + '.json'
  };
  return s3.getObjectAsync(params);
}

if (process.argv[2] === "getenviroment") {

  getEnviromentFile().then(function(data) {
    console.log('DATA', data);

    fs.writeFile("./env/" + buildenv + ".json", data.Body, function(err) {
      if (err) {
        return console.log(err);
      }

      console.log("The file was saved!");
    });

  });
}
