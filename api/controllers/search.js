'use strict';
var _ = require('lodash');
var moment = require('moment');

var profile = require('../helpers/models/')('Profile');
var photo = require('../helpers/models/')('File');
var address = require('../helpers/models/')('Address');
var city = require('../helpers/models/')('City');
var state = require('../helpers/models/')('State');
var settings = require('../helpers/models/')('ProfileSetting');
var service = require('../helpers/models/')('Service');
var skill = require('../helpers/models/')('Skill');
var language = require('../helpers/models/')('Language');
var user = require('../helpers/models/')('User');
var caseStudy = require('../helpers/models/')('CaseStudy');
var pastClient = require('../helpers/models/')('PastClient');
var experience = require('../helpers/models/')('Experience');
var education = require('../helpers/models/')('Education');
var certification = require('../helpers/models/')('Certification');
var industry = require('../helpers/models/')('Industry');
var industryFunction = require('../helpers/models/')('IndustryFunction');
var press = require('../helpers/models/')('Press');
var webshot = require('webshot');
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');

module.exports = {
  getSearchConsultants: getSearchConsultants,
  getAllConsultants: getAllConsultants

};

function getAllConsultants(req, res, next) {

  var queryOptions = {where: {status:{$ne:'Pending'}}, include: [
    service.model,
    {model: caseStudy.model, include: [
      industry.model
    ]},
    {model: pastClient.model, include: [
      photo.model
    ]},
    experience.model,
    education.model,
    skill.model,
    certification.model,
    {model: press.model, include: [
      photo.model
    ]},
    {model: user.model, include: [
      language.model,
      {model: address.model, include: [
        {model: city.model, include: [
          state.model
        ]}
      ]}

    ]},
    industryFunction.model,
    photo.model,
    setting.model

]};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  if (!!order) {
    queryOptions.order = 'LOWER(`'+ orderBy +'`) '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.$or = [
      {email: {$like: "%" + query + "%"}},
      {firstName: {$like: "%" + query + "%"}},
      {lastName: {$like: "%" + query + "%"}},
      {status: {$like: "%" + query + "%"}}
    ];
  }

  if (!!offset) {
    queryOptions.offset = offset;
  }

  if (!!limit) {
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)) {
    profile.findAllPaginated(queryOptions)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    profile.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }

}

function getSearchConsultants(req, res, next) {

  var caseStudyModel = {
    model: caseStudy.model,
    include: [industry.model], 
    required: true
  };
  var stateModel = {
    model: state.model,
    //include: [country.model]
  };

  var cityModel = {
    model: city.model,
    include:[stateModel]
  };

  var userTable = {
    model: user.model,
    where: {},
    include: [{
      model: address.model,
      include:[cityModel]
    }]
  };

  var certificationTable = {
    model: certification.model,
    required:true
  };
  
  var queryOptions = {
    where: {
      status: {
        $ne:'Pending'
      }
    }
  };

  queryOptions.include = [userTable, certificationTable, {model:settings.model, required:true}, caseStudyModel,{model:skill.model, required:true}];


  /*var queryOptions = {
    where: {},
    /*  status: { 
        $ne:'Pending'
      }
    },
    include: [
      service.model,
      {
        model: caseStudy.model, 
        include: [industry.model]
      },
      experience.model,
      skill.model,
      certification.model,
      {
        model: press.model, 
        include: [ photo.model ]
      },
      {
        model: user.model, 
        include: [language.model,
        {
          model: address.model, 
          include: [{
            model: city.model, 
            include: [state.model]
          }]
        }]
      },
      photo.model,
      settings.model
    ]
  };*/

  //var query = req.query.query;
  var keyword = req.query.keyword;
  var name = req.query.name;
  var location = req.query.location;
  var firm = req.query.firm;
  var avaliability = req.query.avaliability;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;
  var whereOptions = [];

  if (!!order) {
    queryOptions.order = 'LOWER(`'+ orderBy +'`) '+ order.toUpperCase();
  }

  if (!!name){
    whereOptions.push(
      {'$User.firstName$': {$like: "%"+name+"%"}},
      {'$User.lastName$': {$like: "%"+name+"%"}}
    );
  };
  if(!!keyword){
    whereOptions.push(
      {'$CaseStudies.caseStudyTitle$': {$like: "%"+keyword+"%"}},
      {'$Certifications.certificationName$': {$like: "%"+keyword+"%"}},
      {'$Skills.skillName$': {$like: "%"+keyword+"%"}},
      {'$ProfileSetting.aboutMe$': {$like: "%"+keyword+"%"}},
      {'$ProfileSetting.serviceDescription$': {$like: "%"+keyword+"%"}}
    );
  };
  if(!!location){
    whereOptions.push(
      {'$User.Addresses.City.cityName$': {$like: "%"+location+"%"}},
      {'$User.Addresses.City.State.stateName$': {$like: "%"+location+"%"}}
    );
  };
  if(!!firm){
    //companyName
    whereOptions.push({companyName : {$like: "%"+firm+"%"}});
  } 
  if(!!avaliability){
    if(avaliability == 1){
      //Now
      whereOptions.push({avaliability : {$lt: moment() }});

    }else if(avaliability == 2){
      //This Month
      whereOptions.push({avaliability : {$lt: moment().endOf('month') }});
    
    };
  } 
  
  queryOptions.where.$or = whereOptions;

  //console.log("queryOptions", queryOptions.where.$or);


  if (!!offset) {
    queryOptions.offset = offset;
  }

  if (!!limit) {
    queryOptions.limit = limit;
  }
  if ((!!offset) || (!!limit)) {
    profile.findAllPaginated(queryOptions)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
  
  } else {
    profile.findAll(queryOptions)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
  }

};
