'use strict';

var bluebird = require('bluebird');
var _ = require('lodash');

var commons = require('../helpers/commons/');
var oauth = require('../helpers/oauth/');
var mailer = require('../helpers/mailer/');
var basePath = require('../helpers/config/').basePath;

var invitation = require('../helpers/models/')('Invitation');
var sequelize = require('../helpers/sequelize/');
var errorHandler = require('../helpers/error-handler/');
var user = require('../helpers/models/')('User');

module.exports = {
  getInvites: getInvites,
  remindEmail: remindEmail,
  updateInvite: updateInvite,
  deleteInvite: deleteInvite,
  createInvite: createInvite,
  createInvites: createInvites,
  validateEmail: validateEmail,
  getAllInvites: getAllInvites,
  myInvite: myInvite
};

function myInvite(req, res, next) {
  var data = req.headers.authorization;
  var token = data.replace("Bearer ","");

  invitation.find({
    where: {
      accessToken: token
    }
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function createInvites(req, res, next) {

  var invites = commons.pickParams(req);
  var UserId = req.token.attributes.id;

  var promise = bluebird.resolve(invites);

  if (!commons.isSuper(req.token)) {
    promise = promise.then(function getUserName(invites) {
      return user.find({
        where: {
          id: UserId
        }
      }).then(function setReferralBy(userData) {

        var fullName = userData.firstName + ' ' + userData.lastName;

        return bluebird.map(invites, function setReferralBy(invite) {
          invite.referalBy = fullName;

          return invite;
        });
      });
    });
  }

  promise.map(function sendInvites(invite) {

    invite.UserId = UserId;
    return sequelize.transaction({
      autocommit: false
    }).then(function(t) {

      var options = {
        transaction: t
      };

      return invitation.create(invite, options)
        .then(function commit(result) {
          t.commit();
          return result;
        })
        .catch(function rollback(err) {
          t.rollback();
          return {
            email: invite.email,
            success: false,
            message: err.message,
            error: err
          };
        });
    });
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteInvite(req, res, next) {

  var InvitationId = req.token.attributes.id;

  invitation.softDelete(InvitationId)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function updateInvite(req, res, next) {

  var invite = req.body;
  var InvitationId = req.token.attributes.id;

  invitation.update(InvitationId, invite)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getAllInvites(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  if (!!order) {
    queryOptions.order = orderBy +' '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.$or = [
      {email : {$like: "%"+query+"%"}},
      {firstName : {$like: "%"+query+"%"}},
      {lastName : {$like: "%"+query+"%"}},
      {status : {$like: "%"+query+"%"}}
    ];
  }

  if(!!offset){
    queryOptions.offset = offset;
  }

  if(!!limit){
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)){
    invitation.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    invitation.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function getInvites(req, res, next) {

  var UserId = req.token.attributes.id;
  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  if (!!order) {
    queryOptions.order = orderBy +' '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.$or = [
      {email : {$like: "%"+query+"%"}},
      {firstName : {$like: "%"+query+"%"}},
      {lastName : {$like: "%"+query+"%"}},
      {status : {$like: "%"+query+"%"}}
    ];
  }

  if(!!offset){
    queryOptions.offset = offset;
  }

  if(!!limit){
    queryOptions.limit = limit;
  }

  queryOptions.where.UserId = UserId;

  if ((!!offset) || (!!limit)){
    invitation.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    invitation.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function remindEmail(req, res, next) {

  var id = req.swagger.params.id.value;

  invitation.get(id)
    .then(function secureAccessToken(invite) {

      if (!invite.accessToken) {
        return user.find({
          where: {
            email: invite.email
          }
        }).then(function methodName(userData) {
          return oauth.applicationToken({id: userData.id, email: userData.email});
        })
        .then(function setToken(token) {
          invite.accessToken = token.access_token;
          return invite;
        });
      }
      return invite;
    })
    .then(function sendEmail(invite) {
      var data = {
        fullName: invite.firstName + " " +  invite.lastName,
        referalName: invite.referalBy,
        accessToken: encodeURIComponent(invite.accessToken),
        remind: true,
        message: invite.message,
        basePath: basePath
      };
      return mailer.sendMail(invite.email, "Reminder – Your Avisare Invitation Awaits", "invite", data)
      .then(function updateInvite() {
        invite.reminds = invite.reminds + 1;
        invite.lastRemind = sequelize.fn('NOW');
        return invite.save();
      });
    }).then(function sendResponse(invite) {
      return {
        reminds: invite.reminds,
        lastRemind: invite.updatedAt
      };
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function validateEmail(req, res, next) {

  var email = req.swagger.params.email.value;

  mailer.validate(email).
    then(function checkEmail(response) {
      return user.find({ where: { email: email }})
      .then(function(user) {
        response.isTaked = !!user;
        return response;
      });
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function createInvite(req, res, next) {

  var invite = req.body;
  var UserId = req.token.attributes.id;
  var promise = bluebird.resolve(invite);
  
  invite.UserId = UserId;

  if (!commons.isSuper(req.token)) {
    promise = promise.then(function getUserName(invite) {
      return user.find({
        where: {
          id: UserId
        }
      }).then(function setReferralBy(userData) {
        invite.referalBy = userData.firstName + ' ' + userData.lastName;
        return invite;
      });
    });
  }

  promise.then(function createInvite(invite) {

    sequelize.transaction({
      autocommit: false
    }).then(function(t) {

      var options = {
        transaction: t
      };

      return invitation.create(invite, options)
        .then(function commit(result) {
          t.commit();
          res.send(result);
        })
        .catch(function rollback(err) {
          t.rollback();
          errorHandler(res, next)(err);
        });
    });
  });
}