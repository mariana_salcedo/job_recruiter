'use strict';

var bcrypt = require('bcrypt');
var promise = require('bluebird');
var chance = new (require('chance'))();
var _ = require('lodash');
var basePath = require('../helpers/config/').basePath;
var mailer = require('../helpers/mailer/');

var commons = require('../helpers/commons/');
var oauth = require('../helpers/oauth/');

var user = require('../helpers/models/')('User');
var sequelize = require('../helpers/sequelize/');
var role = require('../helpers/models/')('Role');
var profile = require('../helpers/models/')('Profile');
var profileSetting = require('../helpers/models/')('ProfileSetting');
var address = require('../helpers/models/')('Address');
var errorHandler = require('../helpers/error-handler/');
var language = require('../helpers/models/')('Language');
var userAddress = require('../helpers/models/')('UserAddress');
var languageModel = require('../helpers/models/')('UserLanguage');
var account = require('../helpers/models/')('Account');
var image = require('../helpers/models/')('File');

promise.promisifyAll(bcrypt);

module.exports = {
  createUser: createUser,
  getMe: getMe,
  getUser: getUser,
  getUsers: getUsers,
  updateMe: updateMe,
  addAddress: addAddress,
  getProfiles: getProfiles,
  addLanguage: addLanguage,
  getLanguages: getLanguages,
  getAddresses: getAddresses,
  resetPassword: resetPassword,
  updateAddress: updateAddress,
  deleteAddress: deleteAddress,
  dismissMessage: dismissMessage,
  changePassword: changePassword,
  deleteLanguages: deleteLanguages,
  requestResetPass: requestResetPass,
  updateApplication: updateApplication,
  createApplicationUser: createApplicationUser
};

function resetPassword(req, res, next)  {
  var data = req.swagger.params.password.value;
  var UserId = req.token.attributes.id;
  console.log(req.token);

  return bcrypt.hashAsync(data.password, 10)
    .then(function saveNewPassword(hash) {
      return user.update(UserId, { password: hash });
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function requestResetPass(req, res, next)  {
  var email = req.swagger.params.email.value;
  user.find({ where: {
      email: email
    },
    include: {
      model: role.model,
      require: true
    }})
    .then(function getResetToken(u) {
      if (!!u && !(_.isEmpty(u.Roles))) {
        var data = { email: email, id: u.id };
        return oauth.resetPasswordToken(data)
          .then(function(token) {
            u.token = token;
            return Promise.resolve(u);
          })
          .then(function sendResetEmail(u) {
            var emailData = {
              firstName: u.firstName,
              basePath: basePath,
              accessToken: encodeURIComponent(u.token.access_token)
            };
            return mailer.sendMail(u.email, "Reset password request", "resetPassword", emailData,"support@avisare.com");
          })
          .then(res.send.bind(res))
          .catch(errorHandler(res, next));
      }else {
        res.sendStatus(404);
      }
    });

}

function updateApplication(req, res, next) {

  var application = req.body;
  var applicationProfile = application.profile || {};

  delete application.profile;

  user.update(application.id, application)
  .then(function addProfile(newUser) {

    return profile.update(applicationProfile.id, applicationProfile)
    .then(function getProfile(newProfile) {
      newUser.dataValues.profile = newProfile;

      return newUser;
    });
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function createApplicationUser(req, res, next) {

  var password = chance.guid();
  var application = req.body;
  var applicationProfile = application.profile || {};

  application.password = password;
  application.roles = [
    'register'
  ];

  delete application.profile;

  sequelize.transaction({
    autocommit: false
  }).then(function(t) {
    var transaction = {transaction: t};
    return user.create(application, transaction)
    .then(function addProfile(newUser) {

      _.assign(applicationProfile, {
        UserId: newUser.id,
        status: 'Pending'
      });
      return profile.create(applicationProfile, transaction)
      .then(function getProfile(newProfile) {
        return oauth.applicationToken({id: newUser.id, email: newUser.email})
        .then(function sendApplication(token) {
          newUser.dataValues.token = token;
          newUser.dataValues.profile = newProfile;

          return newUser;
        });
      });
    })
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}

function changePassword(req, res, next)  {
  var data = commons.pickParams(req);
  var UserId = req.token.attributes.id;

  user.get(UserId).then(function comparePasswords(u) {
    return bcrypt.compareAsync(data.oldPassword, u.password);
  })
  .then(function(check) {
    if (check) {
      return bcrypt.hashAsync(data.newPassword, 10)
        .then(function saveNewPassword(hash) {
          return user.update(UserId, { password: hash });
        })
        .then(res.send.bind(res))
        .catch(errorHandler(res, next));

    }else {
      res.sendStatus(403);
    }
  });

}

function updateMe(req, res, next) {
  var UserId = req.token.attributes.id;
  var data = _.pick(req.body, ["firstName", "lastName"]);

  user.update(UserId, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getUsers(req, res, next) {

  var rol = {
    model: role.model
  };
  var query = {
    include: [profile, rol]
  };
  var roles = req.swagger.params.roles.value;

  if (!!roles) {
    rol.where = {
      roleName: {
        $or: roles
      }
    };

    role.required = true;
  }

  user.findAll(query)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function dismissMessage(req, res, next) {

  var token = req.token.attributes;
  var id = token.id;
  var field = req.swagger.params.field.value;
  var data = {};
  data[field] = true;
  user.update(id, data, {})
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getProfiles(req, res, next) {

  var user = req.token.attributes;

  profile.findAll({ where: {UserId: user.id}})
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function deleteAddress(req, res, next) {

  var id = req.swagger.params.id.value;
  var user = req.token.attributes;

  userAddress.softDeleteManyToMany({
    where: {
      UserId: user.id,
      AddressId: id
    }
  })
  .then(address.softDelete(id))
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));

}

function updateAddress(req, res, next) {
  var data = req.body;
  var id = req.swagger.params.id.value;

  address.update(id, data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function addAddress(req, res, next) {

  var data = req.token.attributes;
  var newAddress = req.body;

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    address.create(newAddress, options).then(function createUserAddressBind(result) {
      return userAddress.create({
        UserId: data.id,
        AddressId: result.id
      }, options);
    })
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}

function getAddresses(req, res, next) {
  var data = req.token.attributes;

  userAddress.findAll({
    where: {
      UserId: data.id
    },
    include: [ address.model ]
  }).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getLanguages(req, res, next) {

  var data = req.token.attributes;

  languageModel.findAll({
    where: {
      UserId: data.id
    },
    include: [ language.model ]
  }).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function addLanguage(req, res, next) {

  var data = req.token.attributes;
  var languages = req.body;

  languageModel.bulkCreate(_.map(languages, function addUserId(languaje) {
    return {
      UserId: data.id,
      LanguageId: languaje.id
    };
  })).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteLanguages(req, res, next) {

  var data = req.token.attributes;
  var languages = req.swagger.params.languages.value;
  return Promise.all(_.map(languages, function deleteLanguages(language) {
    return languageModel.delete({
      where: {
        UserId: data.id,
        LanguageId: language
      }});
  })).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function createUser(req, res, next) {

  var data = req.body;

  sequelize.transaction({
    autocommit: false
  }).then(function(t) {
    var transaction = {transaction: t};
    return user.create(data, transaction)
    .then(function addProfile(newUser) {
      return profile.create({
        UserId: newUser.id,
        status: 'Active',
        avaliability: new Date(),
        companyName: ' ',
        resume: ' '
      }, transaction)
      .then(function returnUser(profileSet) {
        return profileSetting.create({
        ProfileId: profileSet.id,
        profileLink: newUser.firstName + newUser.lastName,
        aboutMe: ' '
      }, transaction)
        .then(function() {
          return newUser;
        });
      });
    })
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}

function getMe(req, res, next) {

  var data = req.token.attributes;
  var include = req.swagger.params.include.value;

  var options = {
    where: {
      id: data.id
    },
    exclude: ['password']
  };

  if (!!include) {
    options.include = [{
      model: profile.model,
      include: [image.model]
    },
    {
      model: role.model
    },
    {
      model: account.model
    }
    ];
  }

  user.find(options)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getUser(req, res, next) {

  var id = req.swagger.params.id.value;

  user.get(id, {
    exclude: ['password']
  }).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}
