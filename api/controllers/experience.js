'use strict';

var experience = require('../helpers/models/')('Experience');
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');

module.exports = {
  updateExperience: updateExperience,
  deleteExperience: deleteExperience,
  addExperiences: addExperiences,
  getExperiences: getExperiences,
  getExperience: getExperience

};

function getExperiences(req, res, next) {

  var id = req.swagger.params.id.value;

  experience.findAll({
    where: {
      ProfileId: id
    }
  })
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function addExperiences(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);

  data.ProfileId = id;
    if (!data.experienceEnd){
    data.experienceEnd = null;
  }

  if (!data.experienceEnd){
    data.experienceEnd = null;
  }

  experience.create(data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function deleteExperience(req, res, next) {

  var id = req.swagger.params.id.value;

  experience.softDelete(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getExperience(req, res, next) {

  var id = req.swagger.params.id.value;

  experience.get(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function updateExperience(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  if (!data.experienceEnd){
    data.experienceEnd = null;
  }

  experience.update(id, data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));

}