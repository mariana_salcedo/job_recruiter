'use strict';

var bluebird = require('bluebird');
var mailer = require('../helpers/mailer/');
var sequelize = require('../helpers/sequelize/');
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');

module.exports = {
  newContact: newContact
};

function newContact(req, res, next) {
  var data = commons.pickParams(req);

  return mailer.sendMail("avisare@avisare.com", "Hey! We have a new contact request!", "contact", data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}