'use strict';

var press = require('../helpers/models/')('Press');
var image = require('../helpers/models/')('File');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getImage: getImage,
  addPress: addPress,
  getPresses: getPresses,
  getPress: getPress,
  updatePress: updatePress,
  deletePress: deletePress
};

function getImage(req, res, next) {
  var id = req.swagger.params.id.value;

  press.get(id).then(function(p) {
    return image.get(p.FileId);
  })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function addPress(req, res, next) {
  var id = req.swagger.params.id.value;
  var data = req.body;

  data.ProfileId = id;

  press.create(data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function updatePress(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  press.update(id, data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function deletePress(req, res, next) {

  var id = req.swagger.params.id.value;

  press.softDelete(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getPresses(req, res, next) {
  var id = req.swagger.params.id.value;

  return press.findAll({
    where: {
      ProfileId: id
    },
    include: [
      {
        model: image.model
      }
    ]
  })
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getPress(req, res, next) {
  var id = req.swagger.params.id.value;

  return press.get(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}
