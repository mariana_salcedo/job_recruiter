'use strict';

var industryFunction = require('../helpers/models/')('IndustryFunction');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  createIndustryFunction: createIndustryFunction,
  getIndustryFunctions: getIndustryFunctions,
  updateIndustryFunction: updateIndustryFunction,
  deleteIndustryFunction: deleteIndustryFunction
};

function getIndustryFunctions(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  if (!!order) {
    queryOptions.order = orderBy +' '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.functionName = {
      $like: query + "%"
    };
  }

  if(!!offset){
    queryOptions.offset = offset;
  }

  if(!!limit){
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)){
    industryFunction.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    industryFunction.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function createIndustryFunction(req, res, next){

  var data = req.body;

  industryFunction.create(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function updateIndustryFunction(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  industryFunction.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteIndustryFunction(req, res, next) {

  var id = req.swagger.params.id.value;
  industryFunction.softDelete(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}