'use strict';

var country = require('../helpers/models/')('Country');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getCountries: getCountries
};

function getCountries(req, res, next) {

  var queryOptions = {};
  var query = req.query.query;
  var order = req.query.order;

  queryOptions.order = '`order`'

  if (!!order) {
    queryOptions.order = 'countryName ' + order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where = {
      countryName: {
        $like: query + "%"
      }
    };
  }

  country.findAll(queryOptions).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}