'use strict';

var certification = require('../helpers/models/')('Certification');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getCertification: getCertification,
  getCertifications: getCertifications,
  addCertification: addCertification,
  updateCertification: updateCertification,
  deleteCertification: deleteCertification
};

function updateCertification(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  certification.update(id, data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));

}

function deleteCertification(req, res, next) {

  var id = req.swagger.params.id.value;

  certification.softDelete(id)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function addCertification(req, res, next) {

  var id = req.swagger.params.id.value;
  var cert = req.body;

  cert.ProfileId = id;

  certification.create(cert)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getCertifications(req, res, next) {

  var id = req.swagger.params.id.value;

  certification.findAll({
    where: {
      ProfileId: id
    }
  })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getCertification(req, res, next) {

  var id = req.swagger.params.id.value;

  certification.get(id)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}
