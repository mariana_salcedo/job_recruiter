'use strict';

var pastClient = require('../helpers/models/')('PastClient');
var image = require('../helpers/models/')('File');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getImage: getImage,
  addPastClient: addPastClient,
  getPastClients: getPastClients,
  getPastClient: getPastClient,
  updatePastClient: updatePastClient,
  deletePastClient: deletePastClient

};

function getImage(req, res, next) {
  var id = req.swagger.params.id.value;

  pastClient.get(id).then(function(p) {
    return image.get(p.FileId);
  })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function addPastClient(req, res, next) {
  var id = req.swagger.params.id.value;
  var data = req.body;

  data.ProfileId = id;

  pastClient.create(data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function updatePastClient(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  pastClient.update(id, data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function deletePastClient(req, res, next) {

  var id = req.swagger.params.id.value;

  pastClient.softDelete(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getPastClients(req, res, next) {
  var id = req.swagger.params.id.value;

  return pastClient.findAll({
    where: {
      ProfileId: id
    },
    include: [image.model]
  })
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getPastClient(req, res, next) {
  var id = req.swagger.params.id.value;

  return pastClient.get(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}