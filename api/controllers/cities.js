'use strict';

var city = require('../helpers/models/')('City');
var state = require('../helpers/models/')('State');
var country = require('../helpers/models/')('Country');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getCities: getCities,
  getCity: getCity
};

function getCity(req, res, next) {

  var queryOptions = {where: {
    id: req.swagger.params.id.value
  }};

  var include = req.swagger.params.include.value;

  if (include === true) {
    queryOptions.include = [
      {
        model: state.model,
        include: [country.model]
      }
    ];
  }

  city.find(queryOptions)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getCities(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var state = req.query.state;

  if (!!order) {
    queryOptions.order = 'cityName ' + order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.cityName = {
      $like: query + "%"
    };
  }

  if (!!state) {
    queryOptions.where.StateId = state;
  }

  city.findAll(queryOptions)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}