'use strict';

var file = require('../helpers/models/')('File');
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');
var s3 = require('../helpers/aws/');
var env = process.env.NODE_ENV || 'develop';
var awsBaseUrl = 'https://s3.amazonaws.com/avisare-' + env;

module.exports = {
  addImage: addImage,
  uploadFile: uploadFile
};

function addImage(req, res, next) {
  var data = commons.pickParams(req);
  if (!!data.s3Url || !!data.cloudinaryUrl) {
    file.create(data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }else {
    res.sendStatus(400);
  }
}

function uploadFile(req, res, next) {
  var data = {};
  data.type = req.body.type;
  data.extension = req.files.fileObject.extension;
  data.size = req.files.fileObject.size;

  var body = req.files.fileObject.buffer;
  var name = new Date().getTime() + req.body.name;
  var mime = req.files.fileObject.mimetype;

  s3.uploadFileToS3(name, body, mime ,'public-read')
    .then(function() {
      data.s3Url = awsBaseUrl + '/' + name;
      return file.create(data);
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

