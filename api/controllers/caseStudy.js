'use strict';

var sequelize = require('../helpers/sequelize/');

var caseStudy = require('../helpers/models/')('CaseStudy');
var errorHandler = require('../helpers/error-handler/');
var userCaseStudy = require('../helpers/models/')('UserCaseStudies');

module.exports = {
  getCaseStudies: getCaseStudies,
  addCaseStudy: addCaseStudy,
  getCaseStudy: getCaseStudy,
  updateCaseStudy: updateCaseStudy,
  deleteCaseStudy: deleteCaseStudy
};

function addCaseStudy(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = req.body;

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    return caseStudy.create(data, options)
      .then(function createUserCaseStudy(cs) {
        return userCaseStudy.create({
          ProfileId: id,
          CaseStudyId: cs.id
        }, options).then(function returnCaseStudy() {
          return cs;
        });
      })
      .then(function commit(result) {
        t.commit();
        res.send(result);
      })
      .catch(function rollback(err) {
        t.rollback();
        errorHandler(res, next)(err);
      });
  });

}

function getCaseStudies(req, res, next) {

  var id = req.swagger.params.id.value;

  userCaseStudy.findAll({
    where: {
      ProfileId: id
    }, include: [
      {
        model: caseStudy.model
      }
    ]
  })
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getCaseStudy(req, res, next) {

  var id = req.swagger.params.id.value;

  caseStudy.get(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function updateCaseStudy(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  caseStudy.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteCaseStudy(req, res, next) {

  var id = req.swagger.params.id.value;

  userCaseStudy.findAll({
    where: {
      CaseStudyId: id
    }})
  .then(function(ucs) {
    return userCaseStudy.softDeleteManyToMany({
      where: {
        ProfileId: ucs[0].ProfileId,
        CaseStudyId: id
      }});
  })
  .then(caseStudy.softDelete(id))
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}