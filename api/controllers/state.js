'use strict';

var state = require('../helpers/models/')('State');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getStates: getStates
};

function getStates(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var country = req.query.country;

  if (!!order) {
    queryOptions.order = 'stateName ' + order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.stateName = {
      $like: query + "%"
    };
  }

  if (!!country) {
    queryOptions.where.CountryId = country;
  }

  state.findAll(queryOptions).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}