'use strict';

var prospect = require('../helpers/models/')('Account');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  getProspects: getProspects
};

function getProspects(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;
  queryOptions.where.type = 'Client';
  queryOptions.where.status = 'Prospect';

  if (!!order) {
    queryOptions.order = orderBy + ' ' + order.toUpperCase();
  }
  if (!!query) {
    queryOptions.where = {
      firstName: {
        $like: query + "%"
      }
    };
  }

  if (!!offset) {
    queryOptions.offset = offset;
  }

  if (!!limit) {
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)) {
    prospect.findAllPaginated(queryOptions, true).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    prospect.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

