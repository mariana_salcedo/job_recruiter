'use strict';

var _ = require('lodash');
var chance = new (require('chance'))();

var mailer = require('../helpers/mailer/');
var commons = require('../helpers/commons/');
var sequelize = require('../helpers/sequelize/');
var basePath = require('../helpers/config/').basePath;
var oauth = require('../helpers/oauth/');
var errorHandler = require('../helpers/error-handler/');

var role = require('../helpers/models/')('Role');
var file = require('../helpers/models/')('File');
var account = require('../helpers/models/')('Account');
// var address = require('../helpers/models/')('Address');
var user = require('../helpers/models/')('User');
var userAccount = require('../helpers/models/')('AccountUserProfile');
var userAccountRole = require('../helpers/models/')('AccountUserRole');

module.exports = {
  getAccount: getAccount,
  createAccount: createAccount,
  deleteAccount: deleteAccount,
  updateAccount: updateAccount,
  resolveClient: resolveClient,
  getAllAccounts: getAllAccounts,
  getUserAccount: getUserAccount,
  getAllUsersAccount: getAllUsersAccount
};

function getAllUsersAccount(req, res, next){
  var id = req.swagger.params.id.value;
  userAccount.findAll({
      where: {
        AccountId: id
      },
      include: [ user.model, file.model]
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getUserAccount(req, res, next) {
  var id = req.swagger.params.id.value;

  var accountModel = {
    model: account.model,
    include: file.model
  };
  userAccount.find({
    where: {
      UserId: id
    },
    include: [ accountModel, user.model, file.model]
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function resolveClient(req, res, next) {
  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {
    var options = { transaction: t };

    return account.get(id, options)
      .then(function createUserClient(clientResult) {
        if (data.response === 'Approved' && clientResult.status === 'Prospect') {
          var newClient = {};
          newClient.lastName = clientResult.dataValues.lastName;
          newClient.email = clientResult.dataValues.email;
          newClient.firstName = clientResult.dataValues.firstName;
          newClient.password = chance.guid();
          newClient.roles = ['client'];

          return user.create(newClient, options)
            .then(function getRole(newUserCli) {
              return [newUserCli, role.find({ where: { roleName: 'client admin' }}, options)];
            })
            .spread(function createUserClientBind(newUserCli, clientRole) {
              return userAccount.create({
                UserId: newUserCli.id,
                AccountId: clientResult.id
              },options)
                .then(function createAccountUserRole(userAccount) {
                  return userAccountRole.create({
                    AccountUserProfileId: userAccount.id,
                    RoleId: clientRole.id
                  },options);
                })
                .then(function updateClient() {
                  return account.update(id, { status: data.response },options);
                })
                .then(function sendEmail() {
                  return oauth.resetPasswordToken(newUserCli)
                    .then(function(token) {
                      var emailData = {
                        firstName: clientResult.firstName + " " + clientResult.lastName,
                        companyName: clientResult.companyName,
                        basePath: basePath,
                        accessToken:  encodeURIComponent(token.access_token)
                      };
                      return mailer.sendMail(newUserCli.email, 'Welcome to Avisare', 'clientApproved', emailData, 'client@avisare.com');
                    });
                });
            });

        }else if (data.response === 'Rejected' && clientResult.status === 'Prospect') {
          // TODO find a better solution for this
          return account.update(id, { status: data.response },options);

        }else {
          t.commit();
          res.sendStatus(409);
        }
      })
      .then(function commit(result) {
        t.commit();
        res.send(result);
      })
      .catch(function rollback(err) {
        t.rollback();
        errorHandler(res, next)(err);
      });
  });
}

function getAccount(req, res, next) {

  var id = req.swagger.params.id.value;

  account.findAll({
    where: {
      id: id
    },
    include: [ file.model ]
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getAllAccounts(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  queryOptions.include = [ file.model ];
  if (!!order) {
    queryOptions.order = orderBy + ' ' + order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.companyName  = {
      $like: query + "%"
    };
  }

  if (!!offset) {
    queryOptions.offset = offset;
  }

  if (!!limit) {
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)) {
    account.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    account.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function updateAccount(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  account.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteAccount(req, res, next) {

  var id = req.swagger.params.id.value;

  account.softDelete(id)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function createAccount(req, res, next) {

  var data = commons.pickParams(req);

  return Promise.resolve()
    .then(function() {
      switch (data.type) {
        case 'Client' : {
          return createClient(data);
        }
        case 'Firm' : {
          break;
        }
        default: {
          break;
        }
      }
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function createClient(data) {

  data.status = 'Prospect';

  var adminEmail = {
    firstName: data.firstName,
    companyName: data.companyName,
    companyWebsite: data.website
  };
  var userEmail = {
    firstName: data.firstName
  };

  return account.create(data)
    .then(function sendClientRegistrationEmails() {
      return Promise.all([mailer.sendMail('client@avisare.com', 'New Client Registration', 'clientRegistrationToAdmin', adminEmail),
        mailer.sendMail(data.email, 'Avisare Client Registration', 'clientRegistrationToClient', userEmail,'client@avisare.com')]);
    });
}