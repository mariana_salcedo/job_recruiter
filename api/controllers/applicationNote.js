'use strict';

var note = require('../helpers/models/')('ApplicationNote');
var user = require('../helpers/models/')('User');
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');

var _ = require('lodash');

module.exports = {
  getNote: getNote,
  getNotes: getNotes,
  addNote: addNote,
  updateNote: updateNote,
  deleteNote: deleteNote
};

function updateNote(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  note.update(id, data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));

}

function deleteNote(req, res, next) {

  var id = req.swagger.params.id.value;

  note.softDelete(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function addNote(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);
  var userId =  req.token.attributes.id;

  _.extend(data, {
    ApplicationId: id,
    UserId: userId
  });

  note.create(data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getNotes(req, res, next) {

  var id = req.swagger.params.id.value;

  note.findAll({
    where: {
      ApplicationId: id
    },
    include: [ user.model ]
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getNote(req, res, next) {

  var id = req.swagger.params.id.value;

  note.find({
    where: {
      id: id
    },
    include: [ {
      model: user.model,
      exclude: ['password']
    } ]})
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

