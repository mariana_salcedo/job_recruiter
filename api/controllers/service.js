'use strict';

var _ = require('lodash');
var bluebird = require('bluebird');

var commons = require('../helpers/commons/');
var service = require('../helpers/models/')('Service');
var errorHandler = require('../helpers/error-handler/');
var serviceProfile = require('../helpers/models/')('ProfileServices');
var profile = require('../helpers/models/')('Profile');

module.exports = {
  getServices: getServices,
  addServices: addServices,
  createService: createService,
  updateService: updateService,
  deleteService: deleteService,
  getAllServices: getAllServices,
  getService: getService,
  deleteProfileServices: deleteProfileServices
};

function addServices(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);

  var currentProfile = profile.build({
    id: id
  });

  bluebird.map(data, function(s) {
    if (s.id) {
      return service.build({
        id: s.id
      });
    }else {
      return service.create(s);
    }
  })
    .then(function addServices(services) {
      return currentProfile
        .addServices(services)
        .then(function response() {
          return services;
        });
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getServices(req, res, next) {

  var id = req.swagger.params.id.value;

  serviceProfile.findAll({
    where: {
      ProfileId: id
    }, include: [
      {
        model: service.model
      }
    ]
  })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function deleteProfileServices(req, res, next) {
  var id = req.swagger.params.id.value;
  var services = req.swagger.params.services.value;

  var currentProfile = profile.build({
    id: id
  });

  var servicesToDelete = _.map(services, function(s) {
    return service.build({
      id: s
    });
  });

  currentProfile
    .removeService(servicesToDelete)
    .then(function methodName(affectedRows) {
      if (affectedRows === 0) {
        res.sendStatus(404);
      } else {
        res.send(services);
      }
    })
    .catch(errorHandler(res, next));

}

function getAllServices(req, res, next) {

  var queryOptions = {};
  var query = req.query.query;
  var order = req.query.order;

  if (!!order) {
    queryOptions.order = 'serviceName ' + order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where = {
      serviceName: {
        $like: query + "%"
      }
    };
  }

  service.findAll(queryOptions).then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getService(req, res, next) {

  var id = req.swagger.params.id.value;

  service.get(id).then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function updateService(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  service.update(id, data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function deleteService(req, res, next) {

  var id = req.swagger.params.id.value;

  service.softDelete(id)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function createService(req, res, next) {

  var data = commons.pickParams(req);

  data.UserId = req.token.attributes.id;

  service.create(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}
