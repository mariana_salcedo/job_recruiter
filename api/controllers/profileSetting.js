'use strict';
var profile = require('../helpers/models/')('Profile');
var user = require('../helpers/models/')('User');
var setting = require('../helpers/models/')('ProfileSetting');
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');

module.exports = {
  addProfileSetting: addProfileSetting,
  getProfileSetting: getProfileSetting,
  updateProfileSetting: updateProfileSetting,
  deleteProfileSetting: deleteProfileSetting,
  validateUrl: validateUrl
};

function validateUrl(req, res, next) {
  var link = req.swagger.params.link.value;
  var profileId = req.swagger.params.id.value;

  return setting.find({ where: {
    profileLink: link,
    ProfileId: {
      $ne: profileId
    }
  }})
    .then(function(setting) {
      var response = {};
      response.isFree = !(!!setting);
      return response;
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function validateUrlUtil(profileUrl, profileId) {
  var link = profileUrl;
  var profileId = profileId;

  return setting.find({ where: {
    profileLink: link,
    ProfileId: {
      $ne: profileId
    } 
  }})
    .then(function(setting) {
      var response = {};
      response.isFree = !(!!setting);
      return response.isFree;
    })
    .catch(function(error) {
      return error;
    });
}

function addProfileSetting(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);
  data.ProfileId = id;

  user.find({
    where: {
      id: req.token.attributes.id
    }
  }).then(function(userInfo) {  
    
    data.profileLink = userInfo.firstName + userInfo.lastName;
    return setting.create(data)
      .then(res.send.bind(res));

  })
  .catch(errorHandler(res, next));
}

function getProfileSetting(req, res, next) {

  var id = req.swagger.params.id.value;

  setting.find({ where: {
    ProfileId: id
  }, include: [profile.model]})
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function updateProfileSetting(req, res, next) {
  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);

  setting.find({where: { ProfileId: id}})
  .then(function updateProfileSettings(s) {
    return setting.update(s.id, data);
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteProfileSetting(req, res, next) {

  var id = req.swagger.params.id.value;

  setting.find({where: { ProfileId: id}})
    .then(function deleteProfileSettings(s) {
      return setting.softDelete(s.id);
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}