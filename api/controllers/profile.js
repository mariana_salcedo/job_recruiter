'use strict';

var sequelize = require('../helpers/sequelize/');

var profile = require('../helpers/models/')('Profile');
var userAddress = require('../helpers/models/')('UserAddress');
var press = require('../helpers/models/')('Press');
var experience = require('../helpers/models/')('Experience');
var education = require('../helpers/models/')('Education');
var industryFunction = require('../helpers/models/')('IndustryFunction');
var profileIndustries = require('../helpers/models/')('ProfileIndustries');
var profileFunctions = require('../helpers/models/')('ProfileFunctions');
var pastClient = require('../helpers/models/')('PastClient');
var caseStudy = require('../helpers/models/')('UserCaseStudies');
var service = require('../helpers/models/')('ProfileServices');
var settings = require('../helpers/models/')('ProfileSetting');
var industry = require('../helpers/models/')('Industry');
var image = require('../helpers/models/')('File');
var errorHandler = require('../helpers/error-handler/');
// var commons = require('../helpers/commons/');

module.exports = {
  createProfile: createProfile,
  updateProfile: updateProfile,
  getProfile: getProfile,
  getIndustryFunctions: getIndustryFunctions,
  addIndustryFunctions: addIndustryFunctions,
  getIndustries: getIndustries,
  addIndustry: addIndustry,
  removeIndustry: removeIndustry,
  updateIndustry: updateIndustry,
  getCompletion: getCompletion,
  removeFunction: removeFunction,
  getImage: getImage

};

function removeIndustry(req, res, next) {

  var id = req.swagger.params.id.value;
  var industry = req.swagger.params.industry.value;

  profileIndustries.delete({
    where: {
      ProfileId: id,
      IndustryId: industry
    }
  }, {paranoid: true})
  .then(function setData() {
    return {
      message: "Deleted"
    };
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function removeFunction(req, res, next) {

  var id = req.swagger.params.id.value;
  var profileFunction = req.swagger.params.functionId.value;

  profileFunctions.delete({
    where: {
      ProfileId: id,
      IndustryFunctionId: profileFunction
    }
  })
  .then(function setData() {
    return {
      message: "Deleted"
    };
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getImage(req, res, next) {
  var id = req.swagger.params.id.value;

  profile.get(id).then(function(p) {
    return image.get(p.FileId);
  })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getCompletion(req, res, next) {
  var id = req.swagger.params.id.value;
  var total = { percentage: 0 };

  profile.find({
    where: {
      id: id
    },
    include: [pastClient.model, experience.model, education.model, press.model]
  }).then(function(p) {
    if (!!p.resume) {
      total.percentage = total.percentage + 10;
    }
    if (p.Education.length !== 0) {
      total.percentage = total.percentage + 10;
    }
    if (p.Experiences.length !== 0) {
      total.percentage = total.percentage + 10;
    }
    if (!!p.FileId) {
      total.percentage = total.percentage + 12;
    }
    if (p.PastClients.length !== 0) {
      total.percentage = total.percentage + 5;
    }
    if (p.Presses.length !== 0) {
      total.percentage = total.percentage + 5;
    }
    return Promise.resolve(p);

  })
    .then(function(p) {
      return Promise.all([
        service.findAll({
          where: {
            ProfileId: p.id
          }}),
        caseStudy.findAll({
          where: {
            ProfileId: p.id
          }
        }),
        userAddress.findAll({
          where: {
            UserId: p.UserId
          }
        }),
        settings.find({
          where: {
            ProfileId: id
          }
        }), Promise.resolve(p)]);
    })
    .spread(function(services, caseStudy, userAddress, settings, p) {
      if (settings && !!settings.aboutMe) {
        total.percentage = total.percentage + 10;
      }
      if (settings && !!settings.serviceDescription) {
        total.percentage = total.percentage + 10;
      }
      if (services.length !== 0) {
        total.percentage = total.percentage + 8;
      }
      if (caseStudy.length !== 0) {
        total.percentage = total.percentage + 10;
      }
      if (userAddress.length !== 0 && !!p.linkedin && !!p.twitter && !!p.skype) {
        total.percentage = total.percentage + 10;
      }
      return total;
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function addIndustry(req, res, next) {

  var data = {
    ProfileId: req.swagger.params.id.value,
    IndustryId: req.body.IndustryId
  };

  profileIndustries.create(data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getIndustries(req, res, next) {

  var id = req.swagger.params.id.value;

  profileIndustries.findAll({
      where: {
        ProfileId: id
      }, include: [
        {
          model: industry.model
        }
      ]
    })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function updateIndustry(req, res, next) {

  var data = {
    ProfileId: req.swagger.params.id.value,
    IndustryId: req.body.IndustryId
  };

  profileIndustries.delete({where: {ProfileId: req.swagger.params.id.value}}).then(
    function() {
      return profileIndustries.create(data);
    }).then(res.send.bind(res)).catch(errorHandler(res, next));

}

function addIndustryFunctions(req, res, next) {

  var data = {
    ProfileId: req.swagger.params.id.value,
    IndustryFunctionId: req.body.industryFunctionId
  };

  profileFunctions.create(data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getIndustryFunctions(req, res, next) {

  var queryOptions = {where: {},
      include: [
        {
          model: industryFunction.model
        }
      ]
  };
  queryOptions.where.ProfileId = req.swagger.params.id.value;

  profileFunctions.findAll(queryOptions).then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getProfile(req, res, next) {

  var id = req.swagger.params.id.value;

  profile.get(id).then(function(p) {
    return image.get(p.FileId).then(function(i) {
      p.dataValues.imagePath = i.cloudinaryUrl;
      return p;
    });
  })
  .then(res.send.bind(res))

  .catch(errorHandler(res, next));
}

function updateProfile(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  profile.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function createProfile(req, res, next) {

  var data = req.body;

  data.UserId = req.token.attributes.id;
  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    profile.create(data, options)
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}