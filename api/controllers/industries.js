'use strict';

var industry = require('../helpers/models/')('Industry');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  createIndustry: createIndustry,
  getIndustries: getIndustries,
  updateIndustry: updateIndustry,
  deleteIndustry: deleteIndustry,
  getIndustry: getIndustry
};

function getIndustries(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;


  if (!!order) {
    queryOptions.order = orderBy +' '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.industryName = {
      $like: query + "%"
    };
  }

  if(!!offset){
    queryOptions.offset = offset;
  }

  if(!!limit){
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)){
    industry.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    industry.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function createIndustry(req, res, next) {

  var data = req.body;
  data.UserId = req.token.attributes.id;

  industry.create(data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getIndustry(req, res, next) {

  var id = req.swagger.params.id.value;

  industry.get(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function updateIndustry(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  industry.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteIndustry(req, res, next) {

  var id = req.swagger.params.id.value;
  industry.softDelete(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}
