'use strict';

var sequelize = require('../helpers/sequelize/');

var _ = require('lodash');
var commons = require('../helpers/commons/');
var errorHandler = require('../helpers/error-handler/');

var reference = require('../helpers/models/')('Reference');
var application = require('../helpers/models/')('Application');
var user = require('../helpers/models/')('User');

module.exports = {
  getReference: getReference,
  getReferences: getReferences,
  getReferencesByApplication: getReferencesByApplication,
  addReferences: addReferences,
  updateReference: updateReference,
  deleteReference: deleteReference
};

function updateReference(req, res, next) {

  var data = commons.pickParams(req);

  var id = req.swagger.params.id.value;

  var responded = req.swagger.params.responded.value;

  if (responded === true) {
    data.status = 'Responded';
  }

  return reference.update(id, data)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));

}

function deleteReference(req, res, next) {

  var id = req.swagger.params.id.value;

  reference.softDelete(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function addReferences(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = commons.pickParams(req);
  var newRef = _.extend(data, {ApplicationId: id});

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    return reference.create(newRef, options)
      .then(function commit(result) {
        t.commit();
        res.send(result);
      })
      .catch(function rollback(err) {
        t.rollback();
        errorHandler(res, next)(err);
      });
  });
}

function getReferencesByApplication(req, res, next) {

  var id = req.swagger.params.id.value;

  reference.findAll({
    where: {
      ApplicationId: id
    }
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getReferences(req, res, next) {

  reference.findAll()
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getReference(req, res, next) {

  var id = req.swagger.params.id.value;
  reference.find({
    where: {
      id: id
    }
  })
  .then(function getReference(referral) {
    if (referral.status === 'Pending') {
      referral.status = 'Read';
      return referral.save();
    } else {
      return referral;
    }
  })
  .then(function sendReference() {
    return reference.find({
      where: {
        id: id
      },
      include: [{
        model: application.model,
        include: [{
          model: user.model,
          attributes: { exclude: ['password'] }
        }]
      }]
    });
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

