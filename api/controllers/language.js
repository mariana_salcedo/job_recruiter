'use strict';

var language = require('../helpers/models/')('Language');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  createLanguage: createLanguage,
  getLanguages: getLanguages,
  updateLanguage: updateLanguage,
  deleteLanguage: deleteLanguage
};

function getLanguages(req, res, next) {

  var queryOptions = {};
  var query = req.query.query;
  var order = req.query.order;

  if (!!order) {
    queryOptions.order = 'languageName ' + order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where = {
      languageName: {
        $like: query + "%"
      }
    };
  }

  language.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function createLanguage(req, res, next){

  var data = req.body;
  data.UserId = req.token.attributes.id;

  language.create(data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function updateLanguage(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  language.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteLanguage(req, res, next) {

  var id = req.swagger.params.id.value;
  language.softDelete(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}