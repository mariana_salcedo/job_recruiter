'use strict';
var _ = require('lodash');
var mailer = require('../helpers/mailer/');
var commons = require('../helpers/commons/');
var sequelize = require('../helpers/sequelize/');
var errorHandler = require('../helpers/error-handler/');

var rfp = require('../helpers/models/')('Rfp');
var user = require('../helpers/models/')('User');
var file = require('../helpers/models/')('File');
var service = require('../helpers/models/')('Service');
var profile = require('../helpers/models/')('Profile');
var account = require('../helpers/models/')('Account');
var industry = require('../helpers/models/')('Industry');
var rfpServices = require('../helpers/models/')('RfpServices');
var rfpIndustries = require('../helpers/models/')('RfpIndustries');
var rfpInterested = require('../helpers/models/')('RfpInterested');

module.exports = {
  getRfp: getRfp,
  getRfps: getRfps,
  editRfp: editRfp,
  createRfp: createRfp,
  getClientRfp: getClientRfp
};

function getRfps(req, res, next) {
  var queryOptions = {};

  queryOptions.include = [
    industry.model,
    service.model,
    account.model,
    // profile.model,
    file.model
  ];
  return rfp.findAll(queryOptions)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getClientRfp(req, res, next) {
  var id = req.swagger.params.id.value;
  var queryOptions = {};

  queryOptions.include = [
    industry.model,
    service.model
  ];

  queryOptions.where = {AccountId: id};

  rfp.findAll(queryOptions)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getRfp(req, res, next) {
  var id = req.swagger.params.id.value;
  var queryOptions = {};

  queryOptions.where = {
    id: id
  };
  queryOptions.include = [
    industry.model,
    service.model,
    account.model,
    file.model,
    {
      model: profile.model,
      include: user.model
    }
  ];
  return rfp.find(queryOptions)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function createRfp(req, res, next) {

  var data = commons.pickParams(req);
  var accountId = data.AccountId;

  // TODO Validate Required fields
  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {
    var options = {
      transaction: t
    };
    return rfp.create(data, options)
    .then(function addConsultants(newRfp) {
      return Promise.all([
        addIndustries(data.industries, newRfp, options),
        addServices(data.services, newRfp, options)
      ])
        .then(function(){
          if (!data.private) {
            return newRfp;
          } else if (data.private && !(!!data.consultants)) {
            t.rollback();
            res.sendStatus(401);
          } else if (data.private && !!data.consultants) {
            return addConsultantsToRfp(data.consultants, newRfp, options)
              .then(function sendEmails() {
                return Promise.all(_.map(data.consultants, function getProfile(consultant) {
                  return profile.find({
                      where: {
                        id: consultant
                      },
                      include: user.model
                    })
                    .then(function(consultantData) {
                      var emailData = {};
                      return mailer.sendMail(consultantData.User.email,
                        'You have been selected to participate ' + data.title + '',
                        'consultantRfpShortlisted',
                        emailData,
                        'client@avisare.com');
                    });
                }));
              });
          } else {
            return newRfp;
          }
        }
      );
    })
    .then(function getUser() {
      return account.get(accountId);
    })
    .then(function sendInternalEmail(result) {
      return Promise.all([
        mailer.sendMail(
          "client@avisare.com", //admin mail!!!
          "New RFP",
          "newRfpToAdmin",
          {name: result.firstName+' '+result.lastName, companyName: result.companyName}),
        mailer.sendMail(
          result.email, // Client Email!!!
          "RFP Posting Successful",
          "newRfpToOwner",
          {firstName: result.firstName}, "client@avisare.com")
      ]);
    })
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}

function editRfp(req, res, next) {

  var data = commons.pickParams(req);
  var id = req.swagger.params.id.value;

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    return rfp.update(id, data, options)
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });

}

function addConsultantsToRfp(consultants, rfp, transaction) {
  return rfpInterested.bulkCreate(_.map(consultants, function addUserId(consultant) {
    return {
      RfpId: rfp.id,
      ProfileId: consultant,
      status: 'Shortlisted'
    };
  }), transaction);
}

function addIndustries(industries, rfp, transaction){
  return rfpIndustries.bulkCreate(_.map(industries, function(industries){
    return {
      RfpId:      rfp.id,
      IndustryId: industries
    }
  }), transaction);
}

function addServices(services, rfp, transaction){
  return rfpServices.bulkCreate(_.map(services, function(services){
    return {
      RfpId:      rfp.id,
      ServiceId:  services
    }
  }), transaction);
}