'use strict';

var Sequelize = require('sequelize');

var basePath = require('../helpers/config/').basePath;
var errorHandler = require('../helpers/error-handler/');
var commons = require('../helpers/commons/');
var mailer = require('../helpers/mailer/');
var sequelize = require('../helpers/sequelize/');
var oauth = require('../helpers/oauth/');
var promise = require('bluebird');

var application = require('../helpers/models/')('Application');
var applicationNotes = require('../helpers/models/')('ApplicationNote');
var user = require('../helpers/models/')('User');
var profile = require('../helpers/models/')('Profile');
var reference = require('../helpers/models/')('Reference');
var userRole = require('../helpers/models/')('UserRole');
var role = require('../helpers/models/')('Role');
var caseStudy = require('../helpers/models/')('CaseStudy');
var profileSettings = require('../helpers/models/')('ProfileSetting');
var profileIndustries = require('../helpers/models/')('Industry');
var profileFunctions = require('../helpers/models/')('IndustryFunction');
var address = require('../helpers/models/')('Address');
var city = require('../helpers/models/')('City');
var country = require('../helpers/models/')('Country');
var state = require('../helpers/models/')('State');

var _ = require('lodash');

module.exports = {
  resendEmail: resendEmail,
  getApplication: getApplication,
  addApplication: addApplication,
  getApplications: getApplications,
  addMyApplication: addMyApplication,
  getMyApplication: getMyApplication,
  updateApplication: updateApplication,
  deleteApplication: deleteApplication,
  resolveApplication: resolveApplication,
  completeApplication: completeApplication,
  updateMyApplication: updateMyApplication,
  getApplicationDetail: getApplicationDetail
};

function sendReferenceEmail(ref, user) {
  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers

  return oauth.referenceToken(ref)
    .then(function(token) {
      var data = {
        firstName: ref.firstName,
        consultantFirstName: user.firstName,
        consultantLastName: user.lastName,
        basePath: basePath,
        id: ref.id,
        accessToken: encodeURIComponent(token.access_token)
      };
      return mailer.sendMail(ref.email, "Referral", "reference", data);
    })
    .then(function() {
      return Promise.resolve(ref);
    });
}

function resendEmail(req, res, next) {

  var id = req.swagger.params.id.value;

  application.find({
    where: {
      id: id
    }
  })
  .then(function sendApplicationEmail(app) {
    return sendEmail(app);
  })
  .then(function sendResponse() {
    return {success: true};
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function sendEmail(application) {

  return user.find({
    where: {
      id: application.UserId
    }
  })
  .then(function getToken(user) {
    // jscs:disable requireCamelCaseOrUpperCaseIdentifiers

    return oauth.applicationToken({id: user.id, email: user.email})
    .then(function sendMail(token) {
      var appData = {
        firstName: user.firstName,
        lastName: user.lastName,
        basePath: basePath,
        accessToken: encodeURIComponent(token.access_token)
      };
      return mailer.sendMail(user.email, "Your Avisare Application has been saved", "applicationSaved", appData);
    });
  });
}

function updateMyApplication(req, res, next) {

  var applicationData = commons.pickParams(req);

  application.update(req.body.id, applicationData, {
    where: {
      UserId: req.token.attributes.id
    }
  })
  .then(function(newApplication) {
    return sendEmail(newApplication)
    .then(function sendApplication() {
      return newApplication;
    });
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function completeApplication(req, res, next) {

  var id = req.token.attributes.id;

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    application.find({
      where: {
        UserId: id
      }
    }, options).then(function updateApplication(application) {
      return application.update({status: 'New'}, options);
    })
    .then(function findUser(applicationData) {
      return user.find({
        where: {id: id},
        include: [{
          model: address.model,
          include: [{
            model: city.model,
            include: [ state.model ]
          }]
        }, {
          model: profile.model,
          include: [
            profileIndustries.model,
            profileFunctions.model
          ]
        }]
      }, options)
      .then(function getReferrals(userData) {
        return reference.findAll({
          where: {
            ApplicationId: applicationData.id
          }
        }, options).map(function sendEmail(ref) {
          return sendReferenceEmail(ref, userData);
        })
        .then(function sendUserData() {
          return userData;
        });
      });
    })
    .then(function sendMails(userData) {

      return oauth.applicationToken({id: userData.id, email: userData.email})
      .then(function sendEmails(token) {
        var emailPromises = [];

        var internalData = {
          firstName: userData.firstName,
          lastName: userData.lastName,
          city:  userData.Addresses[0].City.cityName,
          state: userData.Addresses[0].City.State.stateName,
          primaryIndustry: !!userData.Profiles[0].Industries[0] ? userData.Profiles[0].Industries[0].industryName : "",
          primaryFunction: !!userData.Profiles[0].IndustryFunctions[0] ? userData.Profiles[0].IndustryFunctions[0].functionName : ""
        };
        emailPromises.push(mailer.sendMail(
          "invite@avisare.com",
          "We have a new application!",
          "newApplicationInternal",
          internalData,
          "invite@avisare.com")
        );

        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        var appData = {
          firstName: userData.firstName,
          lastName: userData.lastName,
          basePath: basePath,
          accessToken: encodeURIComponent(token.access_token)
        };

        emailPromises.push(mailer.sendMail(userData.email, "Your Avisare Application has been received", "application", appData));

        promise.all(emailPromises);
      });
    })
    .then(function sendResponse() {
      return {
        success: true
      };
    })
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}

function addMyApplication(req, res, next) {

  var newApplication = req.body;

  newApplication.status = 'Pending';
  newApplication.applicationDate = Sequelize.fn('NOW');

  newApplication.UserId = req.token.attributes.id;

  return sequelize.transaction({
    autocommit: false
  }).then(function(t) {

    var options = {
      transaction: t
    };
    return application.create(newApplication, options)
    .then(function commit(result) {
      t.commit();
      res.send(result);
    })
    .catch(function rollback(err) {
      t.rollback();
      errorHandler(res, next)(err);
    });
  });
}

function getMyApplication(req, res, next) {

  var id = req.token.attributes.id;

  application.find({
    where: {
      UserId: id
    }
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function resolveApplication(req, res, next) {
  if (!!req.token) {
    var emailData = {};
    var data = commons.pickParams(req);
    var id = req.swagger.params.id.value;

    application.get(id).
      then(function updateApplication(app1) {
        if (app1.status === 'Pending' || app1.status === 'New') {
          return application.update(id, { status: data.response})
            .then(function getUser(app) {
              return user.get(app.UserId);
            })
            .then(function sendEmail(u) {
              var subject;
              var template;
              _.extend(emailData, { email: u.email,
                firstName: u.firstName,
                lastName: u.lastName,
                password: u.password,
                basePath: basePath
              });

              return userRole.delete({ where: { UserId: u.id }})
                .then(function() {
                  if (data.response === 'Accepted') {
                    template = 'applicationApproved';
                    subject = 'Welcome to Avisare';
                    return role.find({ where: { roleName: 'consultant'}})
                      .then(function(r) {
                        return userRole.create({
                          UserId: u.id,
                          RoleId: r.id
                        });
                      })
                      .then(function updateProfile(){
                        return profile.find({where:{UserId: u.id}}).then(function(profileObj){
                          return profileObj.update({status:'Accepted'});
                        });
                      })
                      .then(function() {
                        return oauth.resetPasswordToken(u)
                          .then(function(token) {
                            _.extend(emailData, {
                              accessToken:  encodeURIComponent(token.access_token)
                            });
                            return mailer.sendMail(u.email, subject, template, emailData);
                          });
                      });
                  }else {
                    template = 'applicationRejected';
                    subject = 'Your Application for Avisare';
                    return profile.find({where:{UserId: u.id }})
                      .then(function(profileObj){
                        return profileObj.update({status:'Deleted'});
                      })
                      .then(function sentRejectEmail(){
                        return mailer.sendMail(u.email, subject, template, emailData);
                      });

                  }
                });
            });
        }else {
          res.sendStatus(409);
        }
      })
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }else {
    res.sendStatus(401);
  }

}

function deleteApplication(req, res, next) {

  var id = req.swagger.params.id.value;

  application.softDelete(id)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function updateApplication(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  application.update(id, data)
    .then(function(response){
      switch (data.status){
        case 'Bench':
        case 'Accepted':profile.find({where:{UserId:response.UserId}}).then(function(profileObj){
                          profileObj.update({status:'Accepted'}).then(res.send.bind(res));
                        });
                        break;
        case 'Rejected':profile.find({where:{UserId:response.UserId}}).then(function(profileObj){
                          profileObj.update({status:'Deleted'}).then(res.send.bind(res));
                        });
                        break;
        case 'Reviewed':
        case 'New':
        case 'Pending': profile.find({where:{UserId:response.UserId}}).then(function(profileObj){
                          profileObj.update({status:'Pending'}).then(res.send.bind(res));
                        });
                        break;
      }
    })
    .catch(errorHandler(res, next));


}
// TODO check if this is used.
function addApplication(req, res, next) {

  var newApplication = commons.pickParams(req);

  application.create(newApplication)
    .then(function getApplication(app) {
      return application.find({
        where: {
          id: app.id
        },
        include: [ {
        model: user.model,
        include: [{
          model: address.model,
          include: [{
            model: city.model,
            include: [ state.model ]
          }]
        },
        {
          model: profile.model,
          include: [ profileIndustries.model, profileFunctions.model ]
        }]
      }]
      });
    })
    .then(function sendApplicationEmails(appUser) {
      var internalData = {
        firstName: appUser.User.firstName,
        lastName: appUser.User.lastName,
        city:  appUser.User.Addresses[0].City.cityName,
        state: appUser.User.Addresses[0].City.State.stateName,
        primaryIndustry: !!appUser.User.Profiles[0].Industries[0] ? appUser.User.Profiles[0].Industries[0].industryName : "",
        primaryFunction: !!appUser.User.Profiles[0].IndustryFunctions[0] ? appUser.User.Profiles[0].IndustryFunctions[0].functionName : ""
      };
      return mailer.sendMail("invite@avisare.com", "Application - We have a new application!", "newApplicationInternal", internalData)
        .then(function getAppToken() {
          return oauth.applicationToken({id: appUser.User.id, email: appUser.User.email});
        })
        .then(function sendUserApplicationEmail() {
          var appData = {
            firstName: appUser.User.firstName,
            lastName: appUser.User.lastName
          };
          return mailer.sendMail(appUser.User.email, "Your Avisare Application has been received", "application", appData);
        });
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function getApplications(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  var caseStudyModel = {
    model: caseStudy.model,
    include: [profileIndustries.model]
  };

  var stateModel = {
    model: state.model,
    include: [country.model]
  };

  var cityModel = {
    model: city.model,
    include:[stateModel]
  };

  var userTable = {
    model: user.model,
    attributes: ['firstName', 'lastName', 'email'],
    where: {},
    /*include: [{
      model: profile.model,
      include: [caseStudyModel, profileSettings.model]
    },{
      model: address.model,
      include:[cityModel]
    }]*/
  };

  var notesTable = {
    model: applicationNotes.model,
    include: [user.model]
  };

  queryOptions.include = [userTable, notesTable/*, reference.model*/];

  if (!!order) {
    if (orderBy.indexOf('.') == -1){
      queryOptions.order = [[orderBy, order.toUpperCase()]];
    } else {
      queryOptions.order = 'LOWER(`'+ orderBy +'`) '+ order.toUpperCase();
    }
  } else {
    queryOptions.order = [['applicationDate', 'DESC']];
  }

  if (!!query) {
    queryOptions.where.$or = [
      {referredBy : {$like: "%"+query+"%"}},
      {status : {$like: "%"+query+"%"}},
      {'$User.firstName$': {$like: "%"+query+"%"}},
      {'$User.lastName$': {$like: "%"+query+"%"}},
      {'$User.email$': {$like: "%"+query+"%"}}
    ];
  }

  if (!!offset) {
    queryOptions.offset = offset;
  }

  if (!!limit) {
    queryOptions.limit = limit;
  }
  queryOptions.attributes = ['id', 'applicationDate', 'status', 'referredBy'];
  if ((!!offset) || (!!limit)) {
    application.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    application.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function getApplication(req, res, next) {

  var id = req.swagger.params.id.value;

  application.findAll({
    where: {
      UserId: id
    },
    include: [ {
      model: user.model,
      include: [{
        model: profile.model,
        include: [ caseStudy.model ]
      }]
    }]
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function getApplicationDetail(req, res, next) {

  var id = req.swagger.params.id.value;

  var queryOptions = {where: {id:id}};

  var caseStudyModel = {
    model: caseStudy.model,
    include: [profileIndustries.model]
  };

  var stateModel = {
    model: state.model,
    include: [country.model]
  };

  var cityModel = {
    model: city.model,
    include:[stateModel]
  };

  var userTable = {
    model: user.model,
    where: {},
    include: [{
     model: profile.model,
     include: [caseStudyModel, profileSettings.model]
     },{
     model: address.model,
     include:[cityModel]
     }]
  };

  var notesTable = {
    model: applicationNotes.model,
    include: [user.model]
  };

  queryOptions.include = [userTable, notesTable, reference.model];

  application.find(queryOptions).then(res.send.bind(res))
    .catch(errorHandler(res, next));
}
