'use strict';
var _ = require('lodash');

var profile = require('../helpers/models/')('Profile');
var photo = require('../helpers/models/')('File');
var address = require('../helpers/models/')('Address');
var city = require('../helpers/models/')('City');
var state = require('../helpers/models/')('State');
var setting = require('../helpers/models/')('ProfileSetting');
var service = require('../helpers/models/')('Service');
var skill = require('../helpers/models/')('Skill');
var language = require('../helpers/models/')('Language');
var user = require('../helpers/models/')('User');
var caseStudy = require('../helpers/models/')('CaseStudy');
var pastClient = require('../helpers/models/')('PastClient');
var experience = require('../helpers/models/')('Experience');
var education = require('../helpers/models/')('Education');
var certification = require('../helpers/models/')('Certification');
var industry = require('../helpers/models/')('Industry');
var industryFunction = require('../helpers/models/')('IndustryFunction');
var press = require('../helpers/models/')('Press');
var webshot = require('webshot');
var errorHandler = require('../helpers/error-handler/');
// var commons = require('../helpers/commons/');

module.exports = {
  getWebSite: getWebSite,
  getScreen: getScreen,
  getAllConsultants: getAllConsultants,
  getConsultantDetails: getConsultantDetails

};

function CustomFloor(value) {
  return Math.floor(value / 3) * 3;
}

function getWebSite(req, res, next) {
  var web = req.params.profilePage;
  var webProfile = {};

  return setting.find({ where: { profileLink: web }, include: [{
    model: profile.model,
    include: [
      service.model,
      {model: caseStudy.model, include: [
        industry.model
      ]},
      {model: pastClient.model, include: [
        photo.model
      ]},
      experience.model,
      education.model,
      certification.model,
      {model: press.model, include: [
        photo.model
      ]},
      {model: user.model, include: [
        {model: address.model, include: [
          {model: city.model, include: [
            state.model
          ]}
        ]},
        language.model
      ]},
      photo.model
    ]
  }]})
    .then(function(settingProfile) {
      if (!settingProfile) {
        res.send("404");
      }else {
        /** Logic to return only services tags multiples of 3 **/
          var newServices = _.take(settingProfile.Profile.Services, CustomFloor(settingProfile.Profile.Services.length));
          settingProfile.Profile.Services = newServices;
        /** ******************************** **/
        res.render('consultant/site', {pageValues: settingProfile});
      }

    })
    .catch(console.log);
};

function validateUrl(req, res, next) {
  var link = req.swagger.params.link.value;

  return setting.find({ where: { profileLink: link }})
    .then(function(setting) {
      var response = {};
      response.isFree = !(!!setting);
      return response;
    })
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
};

function getAllConsultants(req, res, next) {

  var stateModel = {
    model: state.model,
    attributes: ['stateName'],
    required: false
  };

  var cityModel = {
    model: city.model,
    include:[stateModel],
    attributes: ['cityName'],
    required: false
  };

  var addressModel = {
    model: address.model,
    include:[cityModel],
    attributes: ['CityId'],
    required: false
  };

  var userModel = {
    model: user.model,
    attributes: ['firstName', 'lastName', 'email'],
    include: [addressModel],
    required: true
  };

  var photoModel = {
    model: photo.model,
    required: false
  };

  var industryFunctionModel = {
    model: industryFunction.model,
    required: false
  };

  var settingsModel = {
    model: setting.model,
    attributes:['profileLink']
  };

  var queryOptions = {
    where: {status:{$ne:'Pending'}},
    include: [
      userModel,
      photoModel,
      settingsModel,
      industryFunctionModel
    ]
  };
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  if (!!order) {
    queryOptions.order = 'LOWER(`'+ orderBy +'`) '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.$or = [
      {status : {$like: "%"+query+"%"}},
      {'$User.firstName$': {$like: "%"+query+"%"}},
      {'$User.lastName$': {$like: "%"+query+"%"}},
      {'$User.email$': {$like: "%"+query+"%"}}
    ];
  }

  if (!!offset) {
    queryOptions.offset = offset;
  }

  if (!!limit) {
    queryOptions.limit = limit;
  }
  queryOptions.attributes = ['id','status','UserId','FileId', 'avaliability'];

  if ((!!offset) || (!!limit)) {
    profile.findAllPaginated(queryOptions)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    profile.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
};

function getConsultantDetails(req, res, next){

  var id = req.swagger.params.id.value;

  var stateModel = {
    model: state.model,
    attributes: ['stateName']
  };

  var cityModel = {
    model: city.model,
    include:[stateModel],
    attributes: ['cityName']
  };

  var addressModel = {
    model: address.model,
    include:[cityModel],
    attributes: ['CityId']
  };

  var userModel = {
    model: user.model,
    attributes: ['firstName', 'lastName', 'email'],
    include: [addressModel, language.model]
  };

  var caseStudyModel = {
    model: caseStudy.model,
    include: [industry.model]
  };

  var queryOptions = {
    where: {status:{$ne:'Pending'}, id:id},
    include: [
      userModel,
      photo.model,
      service.model,
      caseStudyModel,
      experience.model,
      education.model,
      skill.model,
      certification.model,
      industryFunction.model,
      setting.model
    ]};

  queryOptions.attributes = ['id','status','UserId','FileId', 'avaliability'];

  profile.find(queryOptions).then(res.send.bind(res))
    .catch(errorHandler(res, next));
};

function getScreen(req, res, next) {
  var web = req.params.profilePage;
  var endurl = req.headers.host + '/consultant/' + web; 
  webshot('http://' + endurl, {shotSize: 'all'}, function(err, renderStream) {
    renderStream.pipe(res);
  });

};