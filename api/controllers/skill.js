'use strict';

var _ = require('lodash');
var bluebird = require('bluebird');

var commons = require('../helpers/commons/');
var skill = require('../helpers/models/')('Skill');
var profile = require('../helpers/models/')('Profile');
var errorHandler = require('../helpers/error-handler/');
var skillProfile = require('../helpers/models/')('ProfileSkill');

module.exports = {
  getSkill: getSkill,
  getSkills: getSkills,
  addSkills: addSkills,
  createSkill: createSkill,
  updateSkill: updateSkill,
  deleteSkill: deleteSkill,
  getAllSkills: getAllSkills,
  deleteSkills: deleteSkills
};

function deleteSkills(req, res, next) {

  var id = req.swagger.params.id.value;
  var skills = req.swagger.params.skills.value;

  var skillsToDelete = _.map(skills, function(s) {
    return skill.build({
      id: s
    });
  });

  var currentProfile = profile.build({
    id: id
  });

  currentProfile
  .removeSkill(skillsToDelete)
  .then(function methodName(affectedRows) {
      if (affectedRows === 0) {
        res.sendStatus(404);
      } else {
        res.send(skills);
      }
    })
  .catch(errorHandler(res, next));

}

function addSkills(req, res, next) {

  var id = req.swagger.params.id.value;
  var skills = req.body;

  var currentProfile = profile.build({
    id: id
  });

  bluebird.map(skills, function(s) {
    if (s.id) {
      return skill.build({
        id: s.id
      });
    }else {
      return skill.create(s);
    }
  })
  .then(function addSkills(skills) {
    return currentProfile
    .addSkills(skills)
    .then(function response() {
      return skills;
    });
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));

}

function getSkills(req, res, next) {

  var id = req.swagger.params.id.value;

  skillProfile.findAll({
    where: {
      ProfileId: id
    }, include: [
      {
        model: skill.model
      }
    ]
  })
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}
function getAllSkills(req, res, next) {

  var queryOptions = {where: {}};
  var query = req.query.query;
  var order = req.query.order;
  var orderBy = req.query.orderBy;
  var offset = req.query.offset;
  var limit = req.query.limit;

  if (!!order) {
    queryOptions.order = orderBy +' '+ order.toUpperCase();
  }

  if (!!query) {
    queryOptions.where.industryName = {
      $like: query + "%"
    };
  }

  if(!!offset){
    queryOptions.offset = offset;
  }

  if(!!limit){
    queryOptions.limit = limit;
  }

  if ((!!offset) || (!!limit)){
    skill.findAllPaginated(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  } else {
    skill.findAll(queryOptions).then(res.send.bind(res))
      .catch(errorHandler(res, next));
  }
}

function getSkill(req, res, next) {

  var id = req.swagger.params.id.value;

  skill.get(id).then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function updateSkill(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  skill.update(id, data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}

function deleteSkill(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  skill.softDelete(id)
    .then(res.send.bind(res))
    .catch(errorHandler(res, next));
}

function createSkill(req, res, next) {

  var data = commons.pickParams(req);

  data.UserId = req.token.attributes.id;

  skill.create(data)
  .then(res.send.bind(res))
  .catch(errorHandler(res, next));
}
