'use strict';

var education = require('../helpers/models/')('Education');
var errorHandler = require('../helpers/error-handler/');

module.exports = {
  addEducations: addEducations,
  getEducations: getEducations,
  updateEducation: updateEducation,
  deleteEducation: deleteEducation
};

function updateEducation(req, res, next) {

  var data = req.body;
  var id = req.swagger.params.id.value;

  data.UserId = req.token.attributes.id;

  education.update(id, data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));

}

function deleteEducation(req, res, next) {

  var id = req.swagger.params.id.value;

  education.softDelete(id)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function getEducations(req, res, next) {

  var id = req.swagger.params.id.value;

  education.findAll({
    where: {
      ProfileId: id
    }
  })
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}

function addEducations(req, res, next) {

  var id = req.swagger.params.id.value;
  var data = req.body;

  data.ProfileId = id;

  education.create(data)
      .then(res.send.bind(res))
      .catch(errorHandler(res, next));
}
