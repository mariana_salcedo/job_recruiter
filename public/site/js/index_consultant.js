$(document).ready(function() {  
  /* highlight the top nav as scrolling occurs */
  $('body').scrollspy({ target: '#nav-bar' })

  /* smooth scrolling for scroll to top */
  $('#toTop').click(function(){
    $('body,html').animate({scrollTop:0},1000);
  })

  /* smooth scrolling for nav sections */
  $('#nav-bar .navbar-nav li>a').click(function(){
    var link = $(this).attr('href');
    var posi = $(link).offset().top;
    //console.log(link+'-'+(posi-91));
    if(link=='#about'){
      $('body,html').animate({scrollTop:0},700);
    }else{
      $('body,html').animate({scrollTop:posi},700);
    }
  });

  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 210,
    itemMargin: 10,
    minItems: 1
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() > 220) {
        $('#toTop').fadeIn(500);
    } else {
        $('#toTop').fadeOut(500);
    }
  });

  var cantItems= $('.box-slider').length;
  var width = $(window).width();
  //console.log('ancho: '+width+'cant: '+cantItems);
  if (width>=767 && cantItems<=2) {
    $(".flexslider .slides").css('width', 'inherit');      
  }else if(width<568 && cantItems>=2){
    $(".flexslider .slides").css('width', '1000%');
  }else if(width<=667 && cantItems>2){
    //console.log('ancho: '+width+'cant: '+cantItems);
    $(".flexslider .slides").css('width', '1000%');
  }else if(width>767 && width<=1000 && cantItems>=4){
    $(".flexslider .slides").css('width', '1000%');
  }else if(width>1000 && cantItems>=6){
    $(".flexslider .slides").css('width', '1000%');
  }else{
    $(".flexslider .slides").css('width', 'inherit');
  };
/****************************************************************/
  $(window).resize(function() {
    var width = $(window).width();
    
    if (width>=767 && cantItems<=2) {
      $(".flexslider .slides").css('width', 'inherit');
    }else if(width<568 && cantItems>=2){
      $(".flexslider .slides").css('width', '1000%');
    }else if(width<=667 && cantItems>2){
      //console.log('ancho: '+width+'cant: '+cantItems);
      $(".flexslider .slides").css('width', '1000%');
    }else if(width>767 && width<=1000 && cantItems>=4){
      $(".flexslider .slides").css('width', '1000%');
    }else if(width>1000 && cantItems>=6){
      $(".flexslider .slides").css('width', '1000%');
    }else{
      $(".flexslider .slides").css('width', 'inherit');
    }
  });
/*********************************************************************/

});//END DOCUMENT READY
/////////////
function DetailCase(detail) {
  var DetailCase= detail;
  //console.log(DetailCase);

  $('.flexslider').addClass('hide');
  $('#case-'+DetailCase).toggleClass('hide');
}

function CloseCase(detail) {
  var DetailCase= detail;
  $('#case-'+DetailCase).addClass('hide').focus();
  $('.flexslider').removeClass('hide');
}
/////////////