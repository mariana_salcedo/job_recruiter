$(window).load(function() {
  var maskList = $.masksSort($.masksLoad("/public/site/js/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
  var maskOpts = {
    inputmask: {
      definitions: {
        '#': {
          validator: "[0-9]",
          cardinality: 1
        }
      },
      // clearIncomplete: true,
      showMaskOnHover: false,
      autoUnmask: false
    },
    match: /[0-9]/,
    replace: '#',
    list: maskList,
    listKey: "mask"

  };

  $('#phoneNumber').inputmasks(maskOpts);
  $('.flexslider').flexslider({
    animation: "fade",
    prevText: "",
    nextText: "",
    slideshowSpeed: 4000,
    directionNav: true,
    start: function() {
      var classe = $('.flex-active-slide').data('type');
      $('.cont-' + classe).css('display', 'block');
    },
    after: function() {

      var classe = $('.flex-active-slide').data('type');
      console.log('paso' + classe);
      $('.cont div').css('display', 'none');
      $('.cont-' + classe).fadeToggle('slow');
    }
  });
  $.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
      if (o[this.name] !== undefined) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  $("#clientform").submit(function(event) {
    var data = JSON.stringify($('#clientform').serializeObject());
    console.log("data", data);
    $.ajax({
      url: '/api/prospects',
      data: data,
      contentType: "application/json",
      error: function() {
        bootbox.alert("Unable to send request, please try again!");
      },
      dataType: 'json',
      success: function(data) {
        bootbox.alert("Thank you for you interest on the platform. We will get back to you as soon as possible");
        document.getElementById("clientform").reset();
      },
      type: 'POST'
    });

    event.preventDefault();

  });

  $("#contactus").submit(function(event) {
    var data = JSON.stringify($('#contactus').serializeObject());
    //console.log("data", data);
    $.ajax({
      url: '/api/contact',
      data: data,
      contentType: "application/json",
      error: function() {
        bootbox.alert("Unable to send request, please try again!");
      },
      dataType: 'json',
      success: function(data) {
        bootbox.alert("Thank you for you interest on the platform. We will get back to you as soon as possible");
        document.getElementById("contactus").reset();
      },
      type: 'POST'
    });
    event.preventDefault();
  });



  $("#loginform").submit(function(event) {
    var data = JSON.stringify($('#loginform').serializeObject());
    $.ajax({
      url: '/api/login',
      data: data,
      contentType: "application/json",
      error: function() {
        bootbox.alert("Invalid Credentials");
      },
      dataType: 'json',
      success: function(data) {

        localStorage.setItem('token', data.access_token);
        document.location.href = '/app';
      },
      type: 'POST'
    });

    event.preventDefault();

  });

  /*CHANGE HEADER CLASS WHEN ACTION SCROLL*/
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 200) {
      $(".navbar-default").addClass("header-back");
    } else {
      $(".navbar-default").removeClass("header-back");
    }
  });

  var checkTokenDisable = function() {
    if (localStorage.getItem('token')) {
      console.log("Im here");
      $("#loginButton").hide();
      $("#joinButton").hide();
      $("#dashboardButton").show();
      $("#logoutButton").show();
    }else{
            $("#loginButton").show();
      $("#joinButton").show();
      $("#dashboardButton").hide();
      $("#logoutButton").hide();
    }
  };
  checkTokenDisable();

  $("#logoutButton").click(function() {
    console.log("Hey");
    localStorage.removeItem('token');
    checkTokenDisable();
  });
  /*END CHANGE HEADER CLASS WHEN ACTION SCROLL*/
});