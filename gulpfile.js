// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

var gulp = require('gulp');

// Gulp Less Tasks
var less = require('gulp-less');
var LessPluginCleanCSS = require('less-plugin-clean-css');
var cleancss = new LessPluginCleanCSS({ advanced: true });
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var flatten = require('gulp-flatten');
var runSequence = require('run-sequence');

gulp.task('less', function() {
  gulp.src('./app/styles/*.less')
    .pipe(less())
    .pipe(gulp.dest('./public/styles/build'))
    .pipe(connect.reload());
});

gulp.task('build-dev', function() {
  runSequence('less','angular-templates-build','compile-js');
});

gulp.task('angular-templates-build', function() {
  gulp.src('./app/client/**/*.html')
  .pipe(flatten())
    .pipe(gulp.dest('./public/templates'));
});

gulp.task('html', function() {
  gulp.src('./app/*.html')
    .pipe(connect.reload());
});

gulp.task('js', function() {
  gulp.src('./app/*.html')
    .pipe(connect.reload());
});

gulp.task('compile-js', function() {

  gulp.src(['./app/client/**/*.module.js', './app/client/**/*.js', '!./app/client/application/site/*.js',
            '!./app/client/reset/site/*.js', '!./app/client/referral/site/*.js'])
    .pipe(concat('main.js'))
    .pipe(ngAnnotate({
      add: true,
      single_quotes: true
    }))
    .pipe(gulp.dest('./public'));

  gulp.src([
    './app/client/reset/**/*.module.js',
    './app/client/widgets/**/*.module.js',
    './app/client/auth/**/*.module.js',
    './app/client/core/**/*.module.js',

    './app/client/reset/**/*.js',
    './app/client/widgets/**/*.js',
    './app/client/auth/**/*.js',
    './app/client/core/**/*.js'
  ])
    .pipe(concat('main-reset-web.js'))
    .pipe(ngAnnotate({
      add: true,
      single_quotes: true
    }))
    .pipe(gulp.dest('./public'));

  gulp.src([
    './app/client/referral/**/*.module.js',
    './app/client/widgets/**/*.module.js',
    './app/client/auth/**/*.module.js',
    './app/client/core/**/*.module.js',

    './app/client/referral/**/*.js',
    './app/client/widgets/**/*.js',
    './app/client/auth/**/*.js',
    './app/client/core/**/*.js'
  ])
    .pipe(concat('main-referral-web.js'))
    .pipe(ngAnnotate({
      add: true,
      single_quotes: true
    }))
    .pipe(gulp.dest('./public'));

  gulp.src([
    './app/client/application/**/*.module.js',
    './app/client/referral/**/*.module.js',
    './app/client/clients/**/*.module.js',
    './app/client/widgets/**/*.module.js',
    './app/client/address/**/*.module.js',
    './app/client/auth/**/*.module.js',
    './app/client/core/**/*.module.js',

    './app/client/application/**/*.js',
    './app/client/referral/**/*.js',
    './app/client/clients/**/*.js',
    './app/client/widgets/**/*.js',
    './app/client/address/**/*.js',
    './app/client/auth/**/*.js',
    './app/client/core/**/*.js'
  ])
    .pipe(concat('main-application-web.js'))
    .pipe(ngAnnotate({
      add: true,
      single_quotes: true
    }))
    .pipe(gulp.dest('./public'));

});

gulp.task('compile-js-prod', function() {
  gulp.src(['./app/client/**/*.module.js', './app/client/**/*.js'])
    .pipe(concat('main.js'))
    .pipe(ngAnnotate({
      add: true,
      single_quotes: true
    }))
    .pipe(uglify({mangle: true}))
    .pipe(gulp.dest('./public'));
});

gulp.task('less-prod', function() {
  gulp.src('./app/styles/*.less')
    .pipe(less({
      plugins: [cleancss]
    }))
    .pipe(gulp.dest('./public/styles/build'));
});

// Gulp Watch Livereload

var connect = require('gulp-connect');
var open = require('gulp-open');

gulp.task('server', function() {
  connect.server({
    root: ['app', './'],
    port: 8080,
    livereload: true
  });
  gulp.src('')
  .pipe(open({
    uri: 'http://127.0.0.1:8080'
  }));
});

gulp.task('watch', ["server"], function() {
  gulp.watch(['./app/styles/*.less'], ['less']);
  gulp.watch(['./app/client/**/*.html'], ['html', 'angular-templates-build']);
  gulp.watch(['./app/client/**/*.js'], ['js', 'compile-js']);
});

// Gulp Emails Live Reload
gulp.task('watch-mails', function() {

  connect.server({
    root: ['./api/helpers/mailer/email/', './public/'],
    port: 10020,
    livereload: true
  });

  gulp.src('')
  .pipe(open({
    uri: 'http://127.0.0.1:10020/build/'
  }));

  gulp.watch(['./api/helpers/mailer/email/templates/**/*.html','./api/helpers/mailer/email/templates/style/*.css'], ['generate-emails']);
});

var nunjucksRender = require('gulp-nunjucks-render');
var inlinesource = require('gulp-inline-source');
var inlinecss = require('gulp-inline-css');

var email = {
  location: './api/helpers/mailer/email/templates/',
  build: './api/helpers/mailer/email/build/'
};

gulp.task('generate-emails', function() {

  nunjucksRender.nunjucks.configure([email.location], {watch: false});

  var options = {
    compress: false
  };

  return gulp.src(email.location + '*.html')
    .pipe(nunjucksRender())
    .pipe(inlinesource(options))
    .pipe(inlinecss({
      applyStyleTags: true,
      removeStyleTags: true,
      removeLinkTags: true,
      preserveMediaQueries: true,
      applyTableAttributes: true
    }))
    .pipe(gulp.dest(email.build))
    .pipe(connect.reload());
});

// Gulp Test Mocha

var mocha = require('gulp-mocha');

gulp.task('test', function() {
  return gulp.src(['./api/**/*.spec.js'], {read: false})
    .pipe(mocha());
});
