![](http://www.avisare.com/images/logo.png)

# Avisare v1 Repo

Develop
[![Build Status](http://74.208.205.2:8181/app/rest/builds/buildType:id:Avisare_Development/statusIcon)]
(http://74.208.205.2:8181/viewType.html?buildTypeId=Avisare_Development&guest=1)
Staging
[![Build Status](http://74.208.205.2:8181/app/rest/builds/buildType:id:Avisare_DeployStaging/statusIcon)]
(http://74.208.205.2:8181/viewType.html?buildTypeId=Avisare_DeployStaging&guest=1)
# README(Work in Progess)
# Prerequisites

#
##### The project needs to have the following programs installed to run the project
 - node.js 4.x
 - [swagger-node](https://github.com/swagger-api/swagger-node) as global
 - [gulpjs](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#1-install-gulp-globally) as global
 - [mocha](https://mochajs.org/#installation) as global
 - [bower](http://bower.io/#install-bower) as global

### Develop
 - jshint
 - jscs
 - gulp

# Get Started

### Clone Repo
```bash
    git clone git@github.com:TeravisionTechnologies/avisare.git
```
### Install Dependencies
```bash
    cd avisare
    npm install
```
### Run Swagger
```bash
   swagger project start -m
```
### Access Angular App
	Open browser http://localhost:10010/


### Access Base api url
	Open browser http://localhost:10010/api

### To review api documentation
```bash
   swagger project edit
```
