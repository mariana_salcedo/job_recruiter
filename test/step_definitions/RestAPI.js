var request = require('supertest');
var should = require('should');

module.exports = function() {
  var self = this;

  this.Given('I have a valid user', function() {

    return self.createValidUser();
  });

  this.Given(/^I have the following payload:$/, function(json, callback) {

    self.setPayload(JSON.parse(json));
    callback();
  });

  this.Given('I have a valid user login', function() {

    return self.createValidUser()
      .then(function(data) {

        var mockUser = data.request._data;
        var params = {
          email: mockUser.email,
          password: mockUser.password
        };

        return self.setPayload(params)
          .callEndpoint("login", "post")
          .then(function(res) {
            self.accessToken = res.body.access_token;
          });
      });
  });

  this.When('I $method "$endpoint"', function(method, endpoint) {

    return self.callEndpoint(endpoint, method.toLowerCase());
  });

  this.Then('I should get $property "$value" in header', function(property, value, callback) {

    self.request.res.headers.should.have.property(property.toLowerCase()).match(RegExp(value));
    callback();
  });

  this.Then('I should get "$value" in response', function(value, callback) {

    self.request.res.body.should.equal(value);
    callback();
  });

  this.Then('I should get an "$type" in the response', function(type, callback) {

    self.request.res.body.should.be.type(type);
    callback();
  });

  this.Then('The object should have "$name" as attribute', function(name, callback) {
    var keys = name.split(/\,\ ?/);
    self.request.res.body.should.have.properties(keys);
    callback();
  });

  this.Then('I should get $code as response code', function(code, callback) {

    self.request.res.statusCode.should.be.exactly(parseInt(code));
    callback();
  });
};