Feature: Create a profile
  In order to create a profile
  As a valid log user

  Scenario: Create a profile
    Given I have a valid user login
    And I have the following payload:
    """
    {
      "aboutMe": "Soy un programador y tal",
      "avaliability": "Always",
      "travel": "NOP",
      "skype": "gustavo.gelf",
      "linkedin": "gustavo.gelf",
      "twitter":  "gustavo.gelf",
      "resume": "Programo y tal",
      "publishPress": false,
      "status": "Enable",
      "companyName":  "CalorVision"
    }
    """
    Then I POST "profiles"
    And I should get 200 as response code
    And I should get an "object" in the response
    And The object should have "id" as attribute

  Scenario: Create a without data
    Given I have a valid user login
    And I have the following payload:
    """
    {
    }
    """
    Then I POST "profiles"
    And I should get 400 as response code

  Scenario: Create a without access token
    Given I have the following payload:
    """
    {
      "aboutMe": "Soy un programador y tal",
      "avaliability": "Always",
      "travel": "NOP",
      "skype": "gustavo.gelf",
      "linkedin": "gustavo.gelf",
      "twitter":  "gustavo.gelf",
      "resume": "Programo y tal",
      "publishPress": false,
      "status": "Enable",
      "companyName":  "CalorVision"
    }
    """
    Then I POST "profiles"
    And I should get 401 as response code
