'use strict';

var request = require('supertest');
var promise = require('bluebird');
var debug = require('debug')('cucumber');
var should = require('should');
var api = require("../../app");
var sequelize = require("../../api/helpers/sequelize");

promise.promisifyAll(request);

module.exports = function() {

  api
    .bind(this)
    .then(function apiDone(api) {

      debug("API ready");
      this.api = api;
    });

  this.done = api;
  this.request = request(api);
  this.sequelize = sequelize;

  this.headers = {};

  this.addHeader = function addHeader(header, value) {
    this.headers[header] = value;

    return this;
  };

  this.setPayload = function setPayload(data) {
    this.payload = data;

    return this;
  };

  this.createValidUser = function createValidUser() {

    var mockUser = this.sequelize.models.User.mockUser();

    mockUser.roles = ['super'];

    return this.setPayload(mockUser)
      .callEndpoint("users", "post");
  };

  this.callEndpoint = function callEndpoint(endpoint, method)   {
    this.request = request(this.api)[method]('/api/' + endpoint)
      .type('application/json');

    if (!!this.payload) {
      this.request.send(this.payload);
    }

    if (!!this.headers) {

      var headers = this.headers;

      for (var index in headers) {
        this.request.set(index, headers[index]);
      }

    }

    if (!!this.accessToken) {
      this.request.set("Authorization", "Bearer " + this.accessToken);
    }

    delete this.payload;
    delete this.headers;

    return this.request.endAsync();
  };

  this.accessToken = null;

};