'use strict';

var debug = require('debug')('cucumber');
var sequelizeSetup = require('sequelize-test-setup');
var sequelize = require("../../api/helpers/sequelize");
var api = require("../../app");

debug("Hook requested");

module.exports = function() {

  var self = this;
  this.Before(function(scenario, callback) {

    debug("Run before hook");

    api.bind(this).then(function apiDone() {

      delete self.accessToken;
      var options = { raw: true };
      return self.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', options)
        .then(function() {
          var tablesArray = [
            'UserRoles',
            'Profiles',
            'UserLanguages',
            'Invitations',
            'Users'
          ];
          return tablesArray;
        })
        .each(function(tableName) {
          debug("Delete From table %s", tableName);
          return sequelize.query('DELETE FROM `avisare`.`' + tableName + '`', options);
          // sequelize.query('ALTER TABLE  `avisare`.`' + tableName + '` AUTO_INCREMENT = 1', options);
        })
        .then(function() {
          debug("All tables truncate");
          return sequelize.query('SET FOREIGN_KEY_CHECKS = 1', options);
        }).then(function() {
          callback();
        });
    }).catch(callback);
  });

  this.After(function(scenario, callback) {

    debug("Run After hook");

    delete self.accessToken;
    callback();
  });
};