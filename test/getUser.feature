Feature: Get user info

  Scenario: Get access token
    Given I have a valid user login
    Then I should get 200 as response code
    And I should get an "object" in the response
    And The object should have "access_token" as attribute

  Scenario: Get api/me endpoint
    Given I have a valid user login
    Then I GET "users/me"
    And I should get 200 as response code
    And I should get an "object" in the response
    And The object should have "id, firstName, lastName, email" as attribute
