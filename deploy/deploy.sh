#!/bin/bash


ssh fsilva@74.208.205.2 'sudo docker rm -f avisare-app ; sudo docker run -d -p 8080:10010 --link avisare-redis:redis --link avisare-mysql:mysql -e DATABASE_USER=root -e DATABASE_PASS=root -e DATABASE_HOST=mysql -e REDIS_HOST=redis --name avisare-app avisaredev/avisare-app'
