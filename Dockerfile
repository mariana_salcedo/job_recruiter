FROM node:4.2.1
MAINTAINER Mariana Salcedo "msalcedo@teravisiontech.com"

WORKDIR /avisare
ADD . /avisare
RUN npm install -g sequelize
RUN npm install -g sequelize-cli
RUN npm install -g bower gulp-cli mysql && npm install && bower install --allow-root
RUN gulp build-dev
RUN gulp generate-emails
RUN chmod -R 777 /avisare

CMD [ "node" ]
ENTRYPOINT ["node","app.js"]

# replace this with your application's default port
EXPOSE 10010